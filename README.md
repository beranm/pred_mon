# Predictive monitoring

This repository servers as storage space for the whole work.

Basic topics covered in this work:

- collecting data from different monitoring servers (only Zabbix currently stable)
- creating simple root cause analysis based on:
	- time
	- correlation
	- clusters
	- dynamic thresholds
- estimating future of monitored items based on the following algorithm:
	- `LSTM` networks on `CUDA` (you need to use `CUDA` device or take immense amounts of time with `CPU` TensorFlow version)
	- Prophet from Facebook incubator (using `SARIMA` and `ARIMA`)
	
Folders list:

- `./text/` -- contains all of the publications from the work
  - `./text/main` -- contains `DP_Beranek_Martin_2017.pdf` which is the main text of the work 
- `./apps/` -- contains all the applications for the resulting application
- `./task/` -- contains only the written task from school's information system
- `./resources/` -- contains all the examples and programming articles I needed to deal with the task

## Application use

If you found yourself in the main directory of this repository, use:

```
cd apps;
```

### Testing

The whole application stack is quite complicated. If you need the app use for testing, use docker-compose:

Starting the app:

```
docker-compose -p predmon up -d;
```

Application will probably fail due to some configuration issues, that's why you need to change some small facts.

To just **test** the app in:
```
config.sh
```
paste somewhere:
```
tests="True";
```

Results will show up at: [http://localhost:8080/](http://localhost:8080/)

#### Collector settings

At `./credentials/zabbix.yml` you can set up the configuration to access zabbix server. Do not forget to add the correct user with proper permissions. Zabbix `API` permissions can be tricky.

At `./collectors/zabbix_collect/configure/hostnames.yml` you can specify with `Unix` filenames patterns showing what machines should be monitored and included in the analysis. Keep in mind that to monitor every machine (expression '`*`') in large topology could take forever to analyse. It‘s not recommended to do this. Application was succesfully tested for ten machines at small laptop (`Intel(R) Core(TM) i5-4200U CPU @ 1.60GHz` with `4 GB` of `RAM`).


#### Credentials

All other credentials related settings you can find in the `credentials` folder.

#### Configuration

At `configuration/basics.yml` you can find the following settings:

- `maximum_prediction_graphs`: how many `png` files of predictions should be kept in `./static/classify/graphs/`
- `maximum_hows_graphs`: how many png files of cause analysis should be kept in `./static/hows/graphs/`
- `prediction_alg`: which algorithm for prediction should be used, currently available `lstm`, `arima` and `Prophet`
- `predict_items_delay`: states how much collection times per item will the process wait until it starts predicting, for example if sampling of an item is half a minute and `predict_items_delay` is set to 4, prediction will happen on every second minute
- `collect_delay`: states the interval of how often is monitoring server being asked about data items, it does not mean it will collect the items in this interval, because data items have their own interval from the monitoring system 
- `disabled_features`: is a list of features which can be disabled, you can put there: correlations, out_of_border_items


### Application services

Each service states what inputs does it need and what are the ouputs. Also, readiness is provided from 1 (not ready) to 10 (would put it in production and trust it with my life). For further reading open `./apps/docker-compose.yml`.

#### collect

Collects data from monitoring servers into local database. Outputs data into *rabbitmq* and *postgres*.

Ready 6/10

#### predict

Get data from `collect` and create prediction based on the algorithm from `configuration/basics.yml`. Result doesn‘t provide any information on whether anything in the future will fail, that's why the result is sent to `classify`.

Ready 4/10

#### classify

Gets data from `predict` sent from predictions and classify data for bad states. Also the graph is constructed into `static/classify/graphs`.

Ready 5/10

#### hows

Create root cause analysis from data saved in *postgres* and *rabbitmq*. The result is provided in `static/hows/graphs`.

Ready 5/10

#### frontend


Creates frontend application on [http://localhost:8080/](http://localhost:8080/). The app simply reads what is in static folders for predictions and root cause analysis and displays it in browser.

Ready 7/10

#### API

Creates API on 5000 port. It just makes a lists of predictions and cause analysis. Access to pictures have to be done via frontend, `API` is not ready at all.

Ready 2/10

#### Cleaner

Reads directories:

- "static/classify/graphs"
- "static/hows/graphs"

and removes old data based on counters given in `configuration/basics.yml`.

Ready 9/10


## Git Blame & contact and affiliation


To spare you from writting command `git blame` all the errors can be sent to martin.beranek112@gmail.com. You can also reach me at the company *Lukapo*.

Whole work was tested on live data from company *Lukapo* and their customers.

You can also find this work at *CTU* library in Prague.


