\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{makeidx}
\usepackage{url}
\usepackage{tikz}
\usepackage{float}
\usepackage{pdfpages}
\usepackage{amsfonts}
\usepackage{mdwlist}
\usepackage{xcolor}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listingsutf8}
\usepackage{cite}
\usepackage{mdframed}

\begin{document}
\title{Forecasting on time series from system resources \\ MI-IKM Report}
\author{Martin Beránek}
\maketitle

%\tableofcontents
%\listoffigures
%\listoftables

\section{Introduction}

Nowadays there is not a single system with some commercial value that wouldn't be monitored -- there is always some health check. Beyond simple ping, control covers many other resources. For example \texttt{CPU} load, memory usage or disk \texttt{I/O} operations. Some of the monitoring systems also store the data to create graphs. Based on the data further analysis can be done. The majority of monitoring services (Icinga, Cacti, \dots) does not provide forecasting for data. Zabbix can use module \cite{zabbix} which can give basic models for forecast future based on past observations (linear, polynomial and exponential regressional models). None of the monitoring systems mentioned earlier use complex models like \texttt{ARIMA} or Neural Networks with Long Short-term Memory (\texttt{LSTM} Neural Networks), the reason is long computational time.

Forecast itself can help avoid outages of systems. With fitting statistical or machine learning models for some crucial resources, system administrator can know in advance what are the possible future problems. For example, when an incoming bandwidth of data to server exceeds certain value, a server can become unreachable.

Furthermore, there are still some attributes of forecasting that need to be satisfied. If forecast takes unreasonably long time, future can be far gone, when data are ready. Another problem may be selecting which data should be analyzed. Monitoring systems cover almost every resource of the client server ranging from incoming \texttt{HTTP} requests till hard drive \texttt{I/O} count. If a forecasting system needs to cover every input data time series, there can be hardly any assumption about data. Data could vary in trends, mean, maximum, minimum and various other features. Without any expert knowledge, forecast needs to be as general as possible.

\emph{Data} section of this paper is about data retrieval from monitoring systems. There is no standard representation of the data. Therefore, implementation of a collector which can do some transformation was created. For the purpose of showing how difficult forecast can be, one particular time series was selected. It has all the unwanted properties which could be found (no stationarity and a lot a peaks). Second section \emph{ARIMA} apply \texttt{ARIMA} model on one selected time series. Further analysis is done, especially on deciding the right order of model. Third section \emph{Reccurent Neural Long Short-term Memory Network} deals with the machine learning methods for forecasting.
 
\section{Data}

Data from the monitoring systems needs to be accessed via a fast and reliable way. Creating models from old data could lead to the invalid conclusion. For some systems, access to a database is performed directly via \texttt{REST API}. All the monitoring used in this paper has some \texttt{REST API}. The problem is with systems which do not by default keep any memory and only work with the current state of resources. That is, for example, \texttt{Icinga} (after basic installation) which asks client machines about their states. With collected output, logic comes. It decides on contacting the administrator to change any unwanted state. \texttt{Icinga} uses \texttt{Nagios} commands on client devices. On the other hand \texttt{Zabbix} keeps data about the past and uses moving average to compress data. Moving average can lose some features of time series. Both of these approaches are not usable for further analyses.

Therefore new layer was suggested, its main role is to collect data. That is why it uses collectors. These collectors are different for each monitoring system. The reason behind that is absence of normalization over \texttt{REST} interfaces. Usually, there is none Atom Syndication Format or \texttt{HATEOS}. Each of the collectors is going to obtain data from monitoring software's \texttt{REST API} making a layer between data used for time series analysis and data in different formats. Collectors are going to save the data to a shared database (figure \ref{data_collection}).

\begin{figure}[H]
  \centering
    \includegraphics[width=0.8\textwidth]{diagrams/collection.png}
  \caption{Data collection infrastructure}
  \label{data_collection}
\end{figure}

The database has a pretty simple layout, there is no need for complicated relationships. Table for records contains only five columns, only the first normal form is enforced. Following list explains the columns:

\begin{description}
    \item[id] -- is running counter for later use in \texttt{ORM}, there is no need for it in any analysis
    \item[time\_execution] -- contain Unix timestamp which is used in \texttt{Zabbix} and also in \texttt{Icinga} to cover time when the value was obtained
    \item[resource] -- contain name of monitored resource, for example \texttt{CPU load} or memory usage
    \item[hostname] -- of a machine which is monitored
    \item[result] -- is a value of \textbf{resource} taken at the \textbf{time\_execution} from machine with given \textbf{hostname}
    \item[state] -- is a status of \textbf{resource}, for example when monitoring suggests that resource is in \texttt{warning} state, non zero value is stored
\end{description}

Columns \texttt{hostname} and \texttt{resource} is already named in monitoring system. Collector only retrieves their names and saves them to records table. There is no need for user explicit knowledge.

Column \textbf{hostname} and \textbf{resource} uses bitmap index to speed up querying. Both columns have low cardinality. \textbf{Time\_execution} has significant meaning in \texttt{TimescaleDB} \cite{timescaledb}. The database can be partitioned not by the size of a table but by timestamp. This partitioning is called chunking. Fast access to the date is provided by a hypertable, which has an overview of all the chunks in the table. \texttt{TimescaleDB} is based on \texttt{PostgreSQL}. Developers can use normal \texttt{SQL} to obtain data. Much needed difference in speed can be seen in benchmarks provided from Timescale (figure \ref{bench}).

\begin{figure}[H]
  \centering
    \includegraphics[width=0.8\textwidth]{graphs/timescale_bench.png}
  \caption{\texttt{TimescaleDB} benchmark on 16\,GB of input data \cite{timescaledb}}
  \label{bench}
\end{figure}

% info gain and stuff

Furthermore, with collection of large set of time series comes a question of which of them are necessary to analyze. For example, collecting number of logged in users has much smaller meaning than counting incoming packets. Basic preprocessing should mark time series which do not give any information. The first idea would be to use entropy. But this is usually not the case since time series data are not discrete. Using a continuous version of counting entropy, which is called \emph{Limiting density of discrete point} \cite{ldodp}, is not reasonable ether. Data do not need to follow any continuous function. As a result, some other features need to be used. One of the fastest ways seems to be simply to check standard deviation. If the deviation is zero, there is no need to use the series in forecasts. Time series model, in this case, should be constant for all its values through the time. The reasonable forecast would be just to prolong constant values.

Another way to select correct data would be to validate them among each other. As it was mentioned earlier, we can use entropy. For selecting useful time series it would be in counting information gain \cite{kono}. The problem with continuous data stays, therefore it would make such a measure unusable. Also, there can be hardly any assumptions about the data correlation in forecasting since general model does not know any context. The safest approach would be not to compare data series between each other at all.

Another attribute in selecting right time series to analyze would be to ask a human expert to mark what he is interested in specifically. For the purpose of this paper, only data from one resource are used. It is the incoming network traffic on Ethernet device of message queue server. Data are taken from Zabbix which saved data of resource every minute for five days. There is a space in data at the beginning, meaning that the state of the resource hasn't changed in some time. No data were received during that time.

\begin{figure}[H]
  \centering
    \includegraphics[width=1\textwidth]{graphs/eth0.png}
  \caption{Incoming network traffic of ethernet device of message queue server in bits per second}
  \label{ethload}
\end{figure}

Zabbix collected $3780$ pieces of data with the standard deviation of $22674710$ and mean of $5010694$. Data spans from 2017-05-25 21:52:21 to 2017-05-30 17:49:38. Minimum value is $30776$ (30 [Kbits]) with maximum $298492700$ (284 [Mbps]). By the simple look at the data, hardly any assumption can be made. There are a lot of peaks and no visible stationarity by far.

\section{ARIMA}

% cite [BOX1] Box G E P, Jenkins G M (1968). Some recent advances in forecasting and control. Applied Statistics, 17(2), 91-109 

ARIMA is an acronym for Autoregressive Integrated Moving Average model. It got its fame from it’s application in predicting stock market \cite{box}. There are two reasons why to use ARIMA. First is to understand better underlying signal of data (if there is any) and second to create a forecast based on already known data. As the acronym suggests, it is constructed from a combination of three base models. 

\subsection{AR}

Autoregressive part implies that there is some autoregression in the signal which could be used for further forecast. It's defined as follows:

\begin{equation}
x_t = \alpha_1X_{t-1} + \dots + \alpha_pX_{t-p} + z_t
\end{equation}

Where $\alpha$ are autocorrelation coefficients at lags 1, 2, \dots, $p$ and $z_t$ is residual error term. A good model covers all the data and leaves $z_t = 0$. It is trying to fit data with a function using previous $x$. The function is adapted via optimizing coefficients to fit input data with the smallest error. In this case the residual sum of squares divided by the number of degrees of freedom was used (as it is usually default option). Input parameter of $p$ states what order AR model is.

\subsection{MA}

Moving average can help with univariate time series. Furthermore smooth data, which have peaks with no informational value (for example current load of \texttt{CPU}). It is defined below:

\begin{equation}
x_t = \sum^{q}_{i=0}\beta_ix_{t-i}
\end{equation}

Where $\beta_i$ are the weights applied to prior data. The $\beta$ coefficients cover weighted average from current and immediate values. Moving average targets trend of immediate data to create a forecast.

\subsection{Differencing}

Both AR and MA models have the problem with stationarity. If the model is not stationary, there is hardly any way how to fit these models (models are finite). Time series have to keep variance, autocorrelation and mean constant over time. This suggests removal of trends in data, therefore integration was introduced to make dataset stationary.

For our input dataset, we can create a test telling us something more about stationarity. \emph{Augmented Dickey--Fuller test} (named after David Dickey and Wayne Fuller \cite{dftest}) has null hypothesis whether a unit root is presented in an autoregressive model or not. The alternative hypothesis states stationarity. We can run the algorithm on the dataset:

\begin{figure}
    \begin{verbatim}
ADF Statistic: -4.297952
p-value: 0.000448
        5%: -2.862
        1%: -3.432
        10%: -2.567
    \end{verbatim}
    \label{adft}
\end{figure}

For rejection of null hypothesis, the p-value is less than or equal to a specific level. P-value $0.000448$ is much bigger than any level of $5\%$, $1\%$ or $10\%$. Hypothesis couldn't be rejected. Dataset could not be labelled as stationary, therefore differencing is going to be used by ARIMA model. Whole model can be described as follows:

\begin{equation}
(1 - \phi_1B-\dots-\phi_pB^p) \cdot (1-B)^dy_t = c + (1 + \theta_1 B + \dots + \theta_qB^q)e_t
\end{equation}

The first part of the equation is dedicated to AR with input degree of $p$. The second part is for differencing with the order of $d$. Last part in MA model is stated with the polynomial of degree $q$.

\subsection{Deciding the parameters}

As mentioned earlier, input parameters of $p, d, q$ need to be given to ARIMA model. This could be achieved by observing autocorrelation function plot (figure \ref{inp_stats}). By carefully analyzing plots generated by statistical library we can derive optimal values.

\begin{figure}[H]
  \centering
  \hspace*{-2cm}  
    \includegraphics[width=1.25\textwidth]{{graphs/pred_diag_smtprabbit-1.emservice.cz!Incoming_network_traffic_on_eth0!Incoming_network_traffic_on_eth0}.png}
  \caption{Input data statistics}
  \label{inp_stats}
\end{figure}

Autocorrelation plot shows that the data stick at lag 0 to 1. There is a small hint of seasonality which occurs at peaks of autocorrelation in the frequency of 5 lags. From QQ plot we can summarize that series is a skewed normal distribution \cite{qplot}.

The problem is that this approach couldn't be reproduced automatically. The algorithm can check for autocorrelation as well as an observer of a graph did. On the other hand, it couldn't notice trends suggesting some level of difference. There is a way to estimate the parameters through a full scan automatically. Since the model needs to be generated with an application, scan for parameters needs to be done anyway. To compare which parameters are better or worse than others we can use Akaike Information Criterion (AIC) \cite{AIC}.

AIC can help with comparisons of models. Suppose we have $M$ of the some data $x$. Let $k$ be a count of estimated parameters in the model then $\hat{L}$ is the maximized value of the likelihood function for the model. That means $\hat{L} = P(x|\hat{\theta}, M)$, where $\hat{\theta}$ are parameter values that maximize the likelihood function. 

\begin{equation}
AIC = 2k - 2\ln (\hat{L})
\end{equation}

Let's say we have two models describing the same data with the different parameter values. We want to decide which one did the description better. We perform count for both AIC and compare them. If AIC of the second model is smaller then for the first model, the second model is better for describing the underlying structure of data.

Since we already know autocorrelation function of our data, we can assume that there is going to be a really small size of parameters $p, q$. Scan with AIC for the parameters estimated the best $(p, d, q)$ as following $(1, 1, 7)$. Resulting AIC was $109940.967535$.

\subsection{Testing the model}

Consider model parameters $(1, 1, 7)$. Let's do some preprocessing and divide data into testing and training. A ratio was set to $80\%$ of data are used for the training phase, and $20\%$ are left for testing. After that we can estimate how much different will the mode be in the future. To calculate error, we will use mean squared error function \cite{MSE}.

\begin{figure}[H]
  \centering
  \hspace*{-2cm}  
    \includegraphics[width=1.25\textwidth]{{graphs/predict_smtprabbit-1.emservice.cz!Incoming_network_traffic_on_eth0Incoming_network_traffic_on_eth0!}.png}
  \caption{Real data in comparison with ARIMA model, green line is the prediction and blue line is showing real data}
  \label{predictions}
\end{figure}

The figure shows (figure \ref{predictions}) how far the model missed the real data in the long run. The graph also shows the underlying structure which the model established on input data before creating the forecast. This approach could help in further analysis. The resulting mean squared error was estimated to $25080375.5115$. This shows the relationship with autocorrelation which suggested that long prediction in the future couldn't be successful on this data.  

\section{Reccurent Neural Long Short Term Memory Network}

ARIMA was introduced based on statistical features. What if there is a structure which could be learned and later used in the forecast? That learning could be done without knowing about features like autocorrelation. Therefore some analysis regarding Recurrent Neural Networks needs to be done.

For the forecast of future values, we certainly need to understand the past. Standard neural networks does not provide any advantage in this situation. Their form of learning is not based upon creating persistent knowledge used for further forecasts. Recurrent networks addresses the issue. They use loops in form of the feedback allowing information to stay in more persistent mode.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.15\textwidth]{diagrams/lstm_node.png}
  \caption{One node of reccurent network}
  \label{lstm_node}
\end{figure}

In diagram (figure \ref{lstm_node}) the input $x_t$ is coming in time $t$ with output $h_t$ (hidden state) which is later used as recurrent input in other iterations (figure \ref{lstm_unrolled}). The network can be unrolled and can pass its outputs at time $t-1$ to another unrolled part at time $t$. Each loop can have old hidden states as well as new data on the input. This approach can lead to providing relationships in the network.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.8\textwidth]{diagrams/lstm_node_all.png}
  \caption{Unrolled reccurent network}
  \label{lstm_unrolled}
\end{figure}

So far we haven't mentioned the issue of activation function, which is in RNN node only one (figure \ref{lstm_fnct}). This leads to a problem of long-term dependency. It seems there is only explicit dependency from the last state to new one. That doesn't need to be true as we also observed at ARIMA. RNN can benefit from creating a relationship between not only outputs directly from the last step but from more past steps which do not need to be in sequence. This creates a context which can be used in the network. There is also the limitation on how much past lags can create the context for new output \cite{graddes}. The answer to this problem can be solved with LSTM \cite{Hochreiter:1997:LSM:1246443.1246450}

\begin{figure}[H]
  \centering
    \includegraphics[width=0.6\textwidth]{diagrams/act_normal.png}
  \caption{Activation function in one node}
  \label{lstm_fnct}
\end{figure}

% http://www-dsi.ing.unifi.it/~paolo/ps/tnn-94-gradient.pdf

Long Short-term Memory Networks are a specific kind kind of RNN with the ability to learn long-term dependencies. Instead of using one activation function, there are five of them, each has a different meaning.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.6\textwidth]{diagrams/act_lstm.png}
  \caption{Activation function in one node of LSTM}
  \label{act_lstm}
\end{figure}

The node (figure \ref{act_lstm1}) contains cell state in horizontal line with one multiplication and addition. The state is the way how the network transfers information with some minor linear interactions. The ability to remove or add information is modelled with gates. Each contains one sigmoid and a pointwise multiplication operation. The output of gates is between one and zero expressing the weight of how much information is relevant in the context of the network. There are only three gates to protect or control the cell state.

First step of LSTM (figure \ref{act_lstm1}) is dedicated to what information should be left out. The sigmoid gate responsible for this is called "forget gate". Output is between one and zero where zero discards whole information and one keeps it all. Output of the gate is given by function: $f_t = \sigma (W_f[h_{t-1}, x_t] + b_f)$.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.6\textwidth]{diagrams/act_lstm1.png}
  \caption{First step of LSTM}
  \label{act_lstm1}
\end{figure}

Next step (figure \ref{act_lstm2}) is to decide which information should be stored in cell state. First sigmoid is called "input gate layer" and decides which values should be updated. Next the $\tanh$ function creates vector of a new candidate for $C_t$. The output function is stated as follows: $i_t = \sigma (W_i\cdot [h_{t-1}, x_t] + b_i)$ and $C_{tc} = tanh(W_c\cdot [h_{t-1}, x_t] + b_c)$.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.6\textwidth]{diagrams/act_lstm2.png}
  \caption{Second step of LSTM}
  \label{act_lstm2}
\end{figure}

Update of $C_t$ is created in the last step into the cell state. It's formed out of all the gates from the steps before. New $C_t$ is created from the next equation:

\begin{equation}
C_t = f_t * C_{t-1} + i_t*C_{tc}
\end{equation}

Finally, the decision needs to be made about the output. It is based on the state of the cell. Also filtering is applied. Sigmoid layer decides which part of the cell is going to be outputted. Then the $\tanh$ function pushes the value from $-1$ to $1$ and multiplies it with an output of the sigmoid gate. That will create an output of only selected values. 

\begin{figure}[H]
  \centering
    \includegraphics[width=0.6\textwidth]{diagrams/act_lstm3.png}
  \caption{Output of $h_t$}
  \label{act_lstm3}
\end{figure}

The output $h_t$ (figure \ref{act_lstm3}) is generated by the following equation:

\begin{equation}
o_t = \sigma (W_o [h_{t-1}, x_t] + b_o)
\end{equation}

\begin{equation}
h_t = o_t * \tanh (C_t)
\end{equation}


\subsection{Preprocessing}

% scaler 0-1

Input data has a standard deviation of $22674710$ and mean of $5010694$. Data for LSTM network have to be scaled from $0$ to $1$. Therefore to use model training properly, everything needs to be scaled down.

Another operation would be to change raw data into data with window. Every point of time needs to have a window containing data which have come immediately before this point. For better understanding see figure \ref{windows}. The window size is one of the parameters which should be estimated on data. In this case, we start with the window size of 40.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.4\textwidth]{diagrams/windows.png}
  \caption{Reshaped data for LSTM}
  \label{windows}
\end{figure}

The last thing which needs to be done is to create two subsets of data. Since this is a time series, we can't use any cross-validation. The original context is to forecast future based on old data, which couldn't be done on shuffled data. The ratio of $80\%$ training data to $20\%$ testing was used to see if the approach had similar results as in ARIMA case.

\subsection{Creating models}

\subsubsection{Basic LSTM}

Basic LSTM model contains one layer of nodes (figure \ref{basiclstm}). The output is forwarded to densifier which outputs one number as a result for each input window. There is no relationship between windows. That means there is no need to keep or transfer the state.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.8\textwidth]{diagrams/basiclstm.png}
  \caption{Basic LSTM model with on layer}
  \label{basiclstm}
\end{figure}

Another property which needs be set is loss function \cite{lossfnct}. Training procedure needs to estimate how the model performs in comparison with a real data. For that process mean square error function is used. ADAM algorithm was used as optimizer because it seems to perform the best in most of the cases \cite{adam}. As it was mentioned earlier, we can't make any assumptions about data. Hence ADAM seems to be the best option.

\begin{figure}[H]
  \centering
  \hspace*{-2cm}  
    \includegraphics[width=1.25\textwidth]{{graphs/basic_lstm_smtprabbit-1.emservice.cz!Incoming_network_traffic_on_eth0}.png}
  \caption{Result of basic LSTM network model}
  \label{lstmbasicpred}
\end{figure}

Figure does not show any improvement in forecasting in the long term (figure \ref{lstmbasicpred}). The resulting mean squared error was estimated to $25759944.4567$. That is a little bit worse ($679568.9452$) than ARIMA forecast.

\subsubsection{Stateful LSTM}

Stateful LSTM model contain one layer of nodes (figure \ref{statefulstm}). The output is forwarded to densifier which outputs result. Relationship between windows is propagated through the network with input state marked as $C_{t*}$ (figure \ref{statefulstm})

\begin{figure}[H]
  \centering
    \includegraphics[width=0.8\textwidth]{diagrams/statefulstm.png}
  \caption{LSTM model with state input from past LSTMs}
  \label{statefulstm}
\end{figure}

All the properties related to internal function (optimizer, loss, \dots) stay the same. The only property which is added is statefulness. The problem of the proper size of the window is now less significant since we are propagating state through the network.

\begin{figure}[H]
  \centering
  \hspace*{-2cm}  
    \includegraphics[width=1.25\textwidth]{{graphs/staful_lstm_smtprabbit-1.emservice.cz!Incoming_network_traffic_on_eth0}.png}
  \caption{Result of stateful LSTM network model}
  \label{lstmstatpred}
\end{figure}

Even though it seems there is no improvement, the mean square error shows that there was the change for a better. Mean square error was $25723488.664$ which is $36455.7927$ off than basic LSTM model. But after all, this result is hardly as good as ARIMA model.

\subsubsection{Stacked LSTM}

Another way to improve forecast should be to stack LSTM models (figure \ref{stackedlstm}). Whole layers of hidden states can be forwarded to new layers which can better capture new features of data. For example, changing frequency of a signal can be divided into different layers. Since there is hardly anything we can assume about the input data, this seems to be the best approach for a forecast.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.8\textwidth]{diagrams/stackedlstm.png}
  \caption{Stacked LSTM in two layers}
  \label{stackedlstm}
\end{figure}

The idea sounded promising, but results are not better than in one layer stateful LSTM network. Mean square error was $25752725.4935$ which is worse (the difference is $29236.8295$). As the figure \ref{lstmstackedpred} shows, model does not perform significantly differently from others.

\begin{figure}[H]
  \centering
  \hspace*{-2cm}
    \includegraphics[width=1.25\textwidth]{{graphs/stacked_lstm_smtprabbit-1.emservice.cz!Incoming_network_traffic_on_eth0}.png}
  \caption{Results of stacked LSTM in two layers}
  \label{lstmstackedpred}
\end{figure}

\section{Conclusion}

Collecting data of resources in information systems is an easy task. Every significant part of the system usually has some representation in a monitoring system. It is reasonably harder to create collectors for all the different servers. This resulted in creating the independent layer where for every new system some application can be defined which is responsible for contacting \texttt{REST API} and getting the data which are needed.

To collect all the data from all the resources showed to be unreasonable, usable data were filtered with standard deviation. Any method, which would compare multiple time series and state which should be used, was left out. There was a simple reason behind this. Removing series automatically without the knowledge of context could lead to removing a future source of information. Other reason could be the speed of decision. Standard deviation, which could be estimated reasonably fast, was found sufficient.

The problem with the quality of data continued through the whole paper. Basic analysis proves small autocorrelation that made any long forecast quite impossible. Very promising was MA part of ARIMA model which by AIC estimation showed, that model can use seven lags in the past. Testing proved that ARIMA couldn't create any long forecast.

Another possibility which is currently vital in stock prize analyses was to use some machine learning technique. The first idea was to use some recurrent neuron network. Due to gradient descent problem, basic RNN was skipped and LSTM network was used. Three models were constructed with different inner structure (one layer with long window, stateful and stacked). Unfortunately, for generic data without any expert knowledge models didn't make any major difference in the forecast (table \ref{mse_table}). ARIMA proved to be faster and predicted better in the case of mean square error.

\begin{table}[H]
  \centering
	\begin{tabular}{| c | c |}
		\hline
		Name & \texttt{MSE} \\ \hline \hline
		ARIMA & $25080375.5115$ \\ \hline
		LSTM Network & $25759944.4567$ \\ \hline
		Stateful LSTM Network & $25723488.664$  \\ \hline
 		Stacked Stafeful LSTM Network & $25752725.4935$ \\ \hline
	\end{tabular}
 \caption{Mean square error in trained models}
  \label{mse_table}
\end{table}

The problem does not need to be in LSTM model itself but in input parameters which need to undergo further tunning. In this paper there was the setting for the window of 40 lags, improvement can be done in setting window size of 120 (lags have usually the distance of one minute between each other, 120 would be two hours long window) or even more. This could lead to much longer training time with the unclear result.

The more promising method would be to use LSTM network just to estimate few lags in the future. The network can adapt to runtime and model does not need to be fully trained again and again. Adaptive training methods were left out of this paper because ARIMA model would be at a disadvantage. Further work is going to be focused on adaptive learning in LSTM and if ARIMA can cover long-term predictions better than LSTM. Another promising method would be using ARIMA as preprocessing. With cleared data, LSTM can capture any trend much better. Again, the procedure of estimating forecast needs to be as generic as possible.

The final statement would be to better select time series. With better quality of the data comes better probability of a good prediction. There is no generic model which can create a forecast with a small error on every input data.  


\bibliographystyle{iso690}
\bibliography{mybibliographyfile}



\end{document}
