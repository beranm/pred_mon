\select@language {czech}
\select@language {UKenglish}
\select@language {UKenglish}
\select@language {UKenglish}
\contentsline {subsection}{Citation of this thesis}{vi}{section*.1}
\contentsline {chapter}{Contents}{ix}{section*.5}
\contentsline {chapter}{Introduction}{1}{chapter*.6}
\contentsline {chapter}{\chapternumberline {1}Today's monitoring}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Case study}{4}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Focus of the study}{4}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Methods}{5}{subsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.2.1}Resources}{5}{subsubsection.1.1.2.1}
\contentsline {subsubsection}{\numberline {1.1.2.2}Software}{6}{subsubsection.1.1.2.2}
\contentsline {subsubsection}{\numberline {1.1.2.3}Experience}{6}{subsubsection.1.1.2.3}
\contentsline {subsection}{\numberline {1.1.3}Discussion}{7}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Conclusion}{8}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Existing \texttt {RCA} tools}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}\texttt {RCA} \emph {OpenStack Vitrage}}{9}{subsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.1.1}Description}{9}{subsubsection.1.2.1.1}
\contentsline {subsubsection}{\numberline {1.2.1.2}Drawbacks}{9}{subsubsection.1.2.1.2}
\contentsline {subsection}{\numberline {1.2.2}\texttt {RCA} \emph {Dynatrace}}{12}{subsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.2.1}Description}{12}{subsubsection.1.2.2.1}
\contentsline {subsubsection}{\numberline {1.2.2.2}Drawbacks}{14}{subsubsection.1.2.2.2}
\contentsline {subsection}{\numberline {1.2.3}Comparison}{14}{subsection.1.2.3}
\contentsline {chapter}{\chapternumberline {2}Application design}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Prototype}{21}{section.2.1}
\contentsline {chapter}{\chapternumberline {3}Predicting the future}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Collecting data}{26}{section.3.1}
\contentsline {section}{\numberline {3.2}ARIMA}{30}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}AR}{30}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}MA}{30}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Differencing}{30}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Deciding the parameters}{31}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Testing the model}{32}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Reccurent Neural Long Short Term Memory Network}{33}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Preprocessing}{37}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Creating models}{38}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Basic LSTM}{38}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}Stateful \texttt {LSTM}}{39}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}Stacked \texttt {LSTM}}{40}{subsubsection.3.3.2.3}
\contentsline {subsubsection}{\numberline {3.3.2.4}Practical use of \texttt {LSTM} networks}{41}{subsubsection.3.3.2.4}
\contentsline {section}{\numberline {3.4}Conclusion}{43}{section.3.4}
\contentsline {chapter}{\chapternumberline {4}Root cause analysis}{47}{chapter.4}
\contentsline {section}{\numberline {4.1}Our approach}{47}{section.4.1}
\contentsline {section}{\numberline {4.2}Selecting thresholds}{48}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Static thresholds}{48}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Data driven thresholds}{48}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Time-based relationships}{50}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Logical time}{51}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Real-time}{52}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Fail states aggregation}{54}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Logical time aggregation}{54}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Kernel density}{55}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Correlations}{58}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Time of computation}{59}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Trend removal}{61}{subsection.4.5.2}
\contentsline {chapter}{\chapternumberline {5}Testing of application}{65}{chapter.5}
\contentsline {section}{\numberline {5.1}Clustering}{65}{section.5.1}
\contentsline {section}{\numberline {5.2}Thresholds}{66}{section.5.2}
\contentsline {section}{\numberline {5.3}Predictions}{70}{section.5.3}
\contentsline {chapter}{Conclusion}{73}{chapter*.62}
\contentsline {chapter}{Bibliography}{77}{section*.64}
\contentsline {appendix}{\chapternumberline {A}Acronyms}{81}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Repository description}{83}{appendix.B}
\contentsline {appendix}{\chapternumberline {C}Folder list on attached USB drive}{89}{appendix.C}
