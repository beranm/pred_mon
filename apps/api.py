from flask import Flask, jsonify
from flask_restful import reqparse, abort, Api, Resource
from os import listdir
from os.path import isfile, join
import json
from modules import FileRenames
from modules.dataGetter import get_predict_interval
from resources.how_modules import get_how_description
import time
import datetime
import subprocess
from optparse import OptionParser
import sys
import yaml

# need to refactor and find howto find local url
local_url = 'http://127.0.0.1:5000'
frontend_url = 'http://127.0.0.1:8080'

# graph directory for how analysis
graphs_directory = "./static/hows/graphs"
# predictions graphs directory
predictions_directory = "./static/classify/graphs"

app = Flask(__name__)
api = Api(app)

"""
Add Hateos JSON stuff
resources:
 - root analysis:

    /how/<name> GET
    return image path to analysis

    /hows GET
    return list of images

 - predictions:

    /prediction/<name> GET
    return image path to analysis

    /predictions GET
    return list of images

"""


def get_git_revision_hash():
    """Get hash of current HEAD in git repository."""
    return subprocess.check_output(['git', 'rev-parse', 'HEAD'])


def get_git_revision_short_hash():
    """Get short hash of current HEAD in git repository."""
    return subprocess.check_output(
        ['git', 'rev-parse', '--short', 'HEAD'])[:-1]


resources = ['/hows', '/predictions']
description = {
    'name': 'Predmon monitoring',
    'description': '\
Predictive monitoring for further analysis on monitored data.',
    # 'version': get_git_revision_short_hash(),
    'version': "0.0.1",
    'provided_resources': resources
}


class ResourcesList(Resource):
    """Resource to show what API can do."""
    def get(self):
        """GET for description API."""
        return(jsonify(description))

class HowList(Resource):

    def get(self):
        graphs = [
            f for f in listdir(
                graphs_directory
            ) if isfile(
                join(graphs_directory, f)
            ) and not f.endswith(".png")
        ]
        graphs = sorted(graphs)
        out = []
        for item in graphs:
            if isfile(graphs_directory + "/" + item + ".png"):
                tmp = {}
                tmp['name'] = item
                tmp['dot_file'] = "graph/" + item
                tmp['png_file'] = "graph/" + item + ".png"
                out.append(tmp)

        return(jsonify(out))


class How(Resource):

    def get(self, name):
        if isfile(
            graphs_directory + '/' + name + '.png'
        ):
            description = get_how_description(name, graphs_directory)
            return(jsonify(description))
        else:
            abort(404, message="Resource " + name + " doesn't exist")


class PredictionList(Resource):

    def get(self):
        graphs = [
            f for f in listdir(
                predictions_directory
            ) if isfile(
                join(predictions_directory, f)
            )
        ]
        graphs = sorted(graphs)
        out = []
        for item in graphs:
            tmp = {}
            item = item[:-4]
            split_tmp = item.split('!')
            tmp['name'] = item
            tmp['hostname'] = split_tmp[0]
            tmp['service'] = FileRenames.denormalizeName(split_tmp[1])
            tmp['time_taken'] = split_tmp[2]
            tmp['timestamp'] = time.mktime(
                datetime.datetime.strptime(
                    split_tmp[2], "%d-%m-%Y_%H:%M:%S").timetuple())
            out.append(tmp)
        return(jsonify(out))


class Prediction(Resource):

    def __init__(self):
        self.name = 'prediction'

    def get(self, name):
        global credentials

        if isfile(
            predictions_directory + '/' + name + '.png'
        ):
            graphs = [
                f.replace(".png", "") for f in listdir(
                    predictions_directory
                ) if isfile(
                    join(predictions_directory, f)
                )
            ]
            graphs = sorted(graphs)
            graphs_len = len(graphs)
            idx = graphs.index(name)
            item = name
            split_tmp = item.split('!')
            description = {}
            description['name'] = name
            description['hostname'] = split_tmp[0]
            description['service'] = FileRenames.denormalizeName(split_tmp[1])
            description['time_taken'] = split_tmp[2].replace("_", " ")
            description['timestamp'] = time.mktime(
                datetime.datetime.strptime(
                    split_tmp[2], "%d-%m-%Y_%H:%M:%S").timetuple())
            description['png_file'] = "graph/" + name + ".png"

            (description['valid_from'], description['valid_until']) = get_predict_interval(
                description['hostname'],
                description['service'],
                credentials)

            if idx < graphs_len - 1:
                next_item = graphs[idx + 1]
                links = []
                links.append(
                    {'href': frontend_url + "/static/classify/graphs/" + next_item, 'rel': 'next'}
                )
                description['_links'] = links
            return(jsonify(description))
        else:
            abort(404, message="Resource " + name + " doesn't exist")


api.add_resource(PredictionList, '/predictions')
api.add_resource(Prediction, '/prediction/<string:name>')

api.add_resource(HowList, '/hows')
api.add_resource(How, '/how/<string:name>')

api.add_resource(ResourcesList, '/')

if __name__ == '__main__':
    global credentials
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)
        credentials = credentials['database']

    app.run(debug=True)
