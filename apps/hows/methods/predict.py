from modules import dataGetter
import time
import logging
logging.basicConfig(level=logging.INFO)


def get_bad_predicted_items(credentials):
    logging.info("Getting get_bad_predicted_items")
    out_of_prediction_items = []
    hostnames = dataGetter.getHostnames(credentials)
    progress_counter = 0  # counter based on hostnames count
    for hostname in hostnames:
        services = dataGetter.getServByHst(credentials, hostname)
        for service in services:
            time_fail = dataGetter.get_predict_notice(
                # predict_notice contains if service is going to have bad future
                hostname,
                service,
                int(time.time()),
                credentials
            )
            if time_fail != 0:
                # dtime = datetime.datetime.fromtimestamp(
                #     int(time_fail)
                # ).strftime('%H:%M:%S %d.%m.%Y')
                name = \
                    hostname + \
                    " " + service
                label = name + "\n could fail at near future"
                # grh.node(
                #     name=name,
                #     label=name + "\n could fail at: " + dtime,
                #     color='#bcbcbc')
                out_of_prediction_items.append(
                    {'name': name, 'label': label}
                )
        progress_counter += 1
        # if progress_counter % 10 == 0:
        logging.info(
            "Done get_bad_predicted_items " + str(progress_counter) + "/" + str(len(hostnames)))
    return out_of_prediction_items
