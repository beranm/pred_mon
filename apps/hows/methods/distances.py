from scipy.stats.mstats import gmean


def countAvgDistance(badStated):
    """Count average distance in bad states."""
    distances = []

    for i in badStated:
        for j in badStated:
            distances.append(abs(j['failed']-i['failed']))

    return int(sum(distances)/len(distances)+1)


def countGAvgDistance(badStated):
    """Count geometric average distance in bad states just for nonzero times."""
    distances = []

    for i in badStated:
        for j in badStated:
            if abs(j['failed']-i['failed']) != 0:
                distances.append(abs(j['failed']-i['failed']))

    gm = gmean(distances)

    return 1 if len(distances) == 0 else gm
