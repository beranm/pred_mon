import numpy as np
from numpy import linspace
from scipy.signal import argrelextrema
from scipy.stats import gaussian_kde

"""
def estimate_time_clusters(x):
    x = [int(y) for y in x]

    if len(x) <= 1:
        return []

    a = array(x).reshape(-1, 1)
    try:
        kde = gaussian_kde(array(x))
        kde.set_bandwidth(bw_method='silverman')
        bandwidth = kde.factor
    except numpy.linalg.linalg.LinAlgError:
        print ("Singular!")
        print (x)
        bandwidth = 1
    # kde.set_bandwidth(bw_method='scott')

    kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(a)
    s = linspace(min(x), max(x))
    e = kde.score_samples(s.reshape(-1, 1))

    mi, ma = argrelextrema(e, np.less)[0], argrelextrema(e, np.greater)[0]

    if mi is []:
        return None

    clusters = []
    if len(mi) == 1:
        clusters.append(a[(a < mi[0])])
        clusters.append(a[(a >= mi[0])])
    elif len(mi) == 2:
        clusters.append(a[(a < mi[0])])
        clusters.append(a[(a >= mi[0]) * (a < mi[1])])
        clusters.append(a[(a >= mi[1])])
    elif len(mi) > 2:
        i = 0
        while True:
            if i == 0:
                clusters.append(a[a < mi[i]])
            elif i < len(mi) - 1:
                clusters.append(a[(a >= mi[i-1]) * (a < mi[i])])
            elif i == len(mi) - 1:
                clusters.append(a[(a >= mi[i-1]) * (a < mi[i])])
                clusters.append(a[(a >= mi[i])])
                break
            i = i + 1

    print("Clusters")
    print([cluster.tolist() for cluster in clusters])
    return ([cluster.tolist() for cluster in clusters])

"""

"""
def estimate_time_clusters(x):
    X = np.array(
        list(
            zip(x, np.zeros(len(x)))))
    q = 0.1
    while 1:
        try:
            bandwidth = estimate_bandwidth(X, quantile=q)
            ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
            ms.fit(X)
            break
        except (ValueError, TypeError):
            if q > 1:
                return []
            print("Quantile too low, enlarging " + str(q))
            q = q + 0.1

    labels = ms.labels_
    cluster_centers = ms.cluster_centers_

    labels_unique = np.unique(labels)
    n_clusters_ = len(labels_unique)

    clusters = []

    for k in range(n_clusters_):
        my_members = labels == k
        clusters.append(list(X[my_members, 0]))

    print("Clusters")
    print(clusters)
    return clusters
"""

def estimate_time_clusters(data):
    data = [int(y) for y in data]
    if len(data) <= 1:
        return []
    xmin = min(data)
    xmax = max(data)
    x = linspace(xmin, xmax, 10000)
    # get actual kernel density.
    try:
        density = gaussian_kde(data)
    except:
        return []

    mi, ma = argrelextrema(
        density(x), np.less), argrelextrema(density(x), np.greater)

    """
    print("mi")
    print(x[mi])
    print("ma")
    print(x[ma])
    print("==============")
    for i in np.array(ma).flatten():
        print (i)
    print("==============")
    print("mi")
    print(mi)
    print("ma")
    print(ma)

    # print(data[data >= mi[0]])
    """
    mi = [x[i] for i in np.array(mi).flatten()]
    ma = [x[i] for i in np.array(ma).flatten()]
    data = np.array(data)

    clusters = []
    if len(mi) == 1:
        clusters.append(data[(data < mi[0])])
        clusters.append(data[(data >= mi[0])])
    elif len(mi) == 2:
        clusters.append(data[(data < mi[0])])
        clusters.append(data[(data >= mi[0]) * (data < mi[1])])
        clusters.append(data[(data >= mi[1])])
    elif len(mi) > 2:
        i = 0
        while True:
            if i == 0:
                clusters.append(data[data < mi[i]])
            elif i < len(mi) - 1:
                clusters.append(data[(data >= mi[i - 1]) * (data < mi[i])])
            elif i == len(mi) - 1:
                clusters.append(data[(data >= mi[i - 1]) * (data < mi[i])])
                clusters.append(data[(data >= mi[i])])
                break
            i = i + 1

    return ([cluster.tolist() for cluster in clusters])
