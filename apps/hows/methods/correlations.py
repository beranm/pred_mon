from modules.relation_database import relation_database as rdb
from modules.influx_connector import influx_connector as infc
from scipy import signal
import numpy
import logging
import time
logging.basicConfig(level=logging.INFO)


def create_cor_relations(badStated, delay, credentials, influx_credentials):
    """Create correlation table for badStates in between all the other data items.

    """
    logging.info("Getting create_cor_relations")

    cortab = dict()
    # print("Counting correlations on bad states")
    cached_data = {}

    _rdb = rdb(credentials)
    _influx = infc(influx_credentials)
    progress_counter = 0  # counter based on hostnames count

    for i in badStated:
        data = _influx.get_data_from_time(
            i['message']['hostname'],
            i['message']['service'],
            int(i['message']['time_execution']) - delay)
        hostnames = _rdb.get_hostnames()
        for hostname in hostnames:
            # hostname not cachec
            iteration_start = time.time()
            if hostname not in cached_data.keys():
                cached_data[hostname] = {}
            # selecting services
            services = _rdb.get_services_by_hostname(hostname)
            for service in services:
                # service not cached
                if service not in cached_data[hostname].keys():
                    cached_data[hostname][service] = []
                # asking for same data which are already in badStates
                if (
                    i['message']['hostname'],
                    i['message']['service']
                ) == (hostname, service):
                    continue
                # load data if not cached
                if len(cached_data[hostname][service]) == 0:
                    try:
                        data_service = _influx.get_data_from_time(
                            hostname,
                            service,
                            int(i['message']['time_execution']) - delay)
                    except ValueError:
                        continue
                    if data_service.std(skipna=True)[0] == 0:
                        continue
                    # removing nans from collected data
                    data_service = \
                        data_service[data_service.columns[0]].values
                    data_service = data_service[~numpy.isnan(data_service)]
                else:
                    # loading cached data
                    data_service = cached_data[hostname][service]
                    print("Used cached")
                if len(data) != len(data_service):
                    if len(data) > len(data_service):
                        data = data[-len(data_service):]
                    if len(data) < len(data_service):
                        data_service = data_service[-len(data):]
                if i['message']['hostname'] + ' ' + i['message']['service'] not in cortab.keys():
                    cortab[
                        i['message']['hostname'] +
                        ' ' +
                        i['message']['service']] = dict()
                try:
                    cortab[
                        i['message']['hostname'] +
                        ' ' + i['message']['service']
                    ][
                        hostname + ' ' + service
                    ] = numpy.corrcoef(
                        signal.detrend(
                            data[data.columns[0]].values),
                        signal.detrend(
                            data_service)
                    )[0, 1]
                except (TypeError, ZeroDivisionError):
                    cortab[
                        i['message']['hostname'] +
                        ' ' + i['message']['service']
                    ][
                        hostname + ' ' + service
                    ] = 0
            progress_counter += 1
            # if progress_counter % 10 == 0:
            logging.info(
                "Done create_cor_relations " +
                str(progress_counter) + "/" +
                str(len(hostnames)) +
                " iteration done in " + str(time.time() - iteration_start))
            # for key in cached_data.keys():
            #     logging.info(key + ":")
            #     logging.info(cached_data[key].keys())
    return cortab
