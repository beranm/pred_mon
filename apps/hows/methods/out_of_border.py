import numpy as np
from modules.time_handlers import df_time_to_timestamp
import time
import datetime
from scipy import signal
from modules.influx_connector import influx_connector as infc
from modules.relation_database import relation_database as rdb
import logging
logging.basicConfig(level=logging.INFO)


def get_out_of_border_items(credentials, infc_credentials):
    logging.info("Getting out_of_border_items")
    _rdb = rdb(credentials)
    _infc = infc(infc_credentials)
    period = 'day'  # switch according of len of data
    out_of_border_items = []
    hostnames = _rdb.get_hostnames()
    progress_counter = 0  # counter based on hostnames count
    for hostname in hostnames:
        services = _rdb.get_services_by_hostname(hostname)
        for service in services:
            max_th = _rdb.get_youngest_threshold(
                hostname, service, "ninety_percentil")
            min_th = _rdb.get_youngest_threshold(
                hostname, service, "min")
            if max_th is None or min_th is None or \
                    max_th.created < time.time() - (60 * 60) or \
                    min_th.created < time.time() - (60 * 60):
                try:
                    direct_past_records = _infc.get_data_from_time(
                        hostname, service, time.time() - (60 * 60 * 7 * 24))
                except ValueError:
                    logging.info(
                        "No records for " +
                        hostname + " " +
                        service + " from " +
                        time.ctime(time.time() - (2 * 60))
                    )
                    continue
                if len(direct_past_records) < 1000:
                    continue

                data_trn_rem = direct_past_records[
                    direct_past_records.columns[0]
                ].values

                if np.std(signal.detrend(data_trn_rem)) == 0:
                    # ain't got no sense to continue
                    _rdb.insert_new_threshold(
                        hostname, service, 0,
                        "ninety_percentil", "std_fail")
                    _rdb.insert_new_threshold(
                        hostname, service, 0,
                        "min", "std_fail")
                    continue
                last_value_head = direct_past_records.sort_index(
                    ascending=False).head(1)
                last_value = last_value_head[
                    direct_past_records.columns[0]].values[0]
                last_index = last_value_head.index.values[0]
                # getting second last value
                """
                second_last_value_head = direct_past_records.sort_index(ascending=False) \
                    .head(2).tail(1)
                second_last_value = second_last_value_head[
                    direct_past_records.columns[0]].values[0]
                if second_last_value == last_value:
                    continue
                """
                # counting values for percentile
                values = [float(i) for i in direct_past_records[
                    direct_past_records.columns[0]
                ].values]
                ninety_percentil = np.percentile(
                    values, 99)
                """
                one_percentil = np.percentile(
                    values, 1)
                """
                minima = min(
                    values)
                _rdb.insert_new_threshold(
                    hostname, service, ninety_percentil,
                    "ninety_percentil")
                _rdb.insert_new_threshold(
                    hostname, service, minima,
                    "min")
            elif min_th.thr_attributes == "std_fail":
                continue
            elif max_th.thr_attributes == "std_fail":
                continue
            else:
                minima = min_th.threshold
                ninety_percentil = max_th.threshold
            try:
                direct_past_records = _infc.get_data_from_time(
                    hostname, service, time.time() - (2 * 60))
            except ValueError:
                logging.info(
                    "No records for " +
                    hostname + " " +
                    service + " from " +
                    time.ctime(time.time() - (2 * 60))
                )
                continue
            last_value_head = direct_past_records.sort_index(
                ascending=False).head(1)
            last_value = last_value_head[
                direct_past_records.columns[0]].values[0]
            last_index = last_value_head.index.values[0]

            logging.debug(str(last_value) + " > " + str(ninety_percentil))

            if last_value > ninety_percentil:
                dtime = datetime.datetime.fromtimestamp(
                    df_time_to_timestamp(last_index)
                ).strftime('%H:%M:%S %d.%m.%Y')
                name = hostname + " " + service
                label = name + "\nOut of " + period + " ninetynine percentile\nLast detected: " + dtime
                out_of_border_items.append(
                    {
                        'name': name,
                        'reason': label,
                        'dtime': df_time_to_timestamp(last_index),
                        'value': last_value,
                        'compare': ninety_percentil})
                logging.info(name + "\nOut of " + period + " ninetynine percentile\nLast detected: " + dtime)
            elif last_value < minima:
                dtime = datetime.datetime.fromtimestamp(
                    df_time_to_timestamp(last_index)
                ).strftime('%H:%M:%S %d.%m.%Y')
                name = hostname + " " + service
                label = name + "\nLower than " + period + " minimum\nLast detected: " + dtime
                out_of_border_items.append(
                    {
                        'name': name,
                        'reason': label,
                        'dtime': df_time_to_timestamp(last_index),
                        'value': last_value,
                        'compare': minima
                    }
                )
                logging.info(name + "\nLower than " + period + " minimum\nLast detected: " + dtime)
            """
            elif last_value < one_percentil:
                dtime = datetime.datetime.fromtimestamp(
                    df_time_to_timestamp(last_index)
                ).strftime('%H:%M:%S %d.%m.%Y')
                name = hostname + " " + service
                label = name + "\nLower than " + period + " one percentile\nLast detected: " + dtime
                out_of_border_items.append(
                    {'name': name, 'reason': label, 'dtime': df_time_to_timestamp(last_index)})
                print(name + "\nLower than " + period + " one percentile\nLast detected: " + dtime)
            """
        progress_counter += 1
        # if progress_counter % 10 == 0:
        logging.info(
            "Done out_of_border_items " + str(progress_counter) + "/" + str(len(hostnames)))
    return out_of_border_items

