from optparse import OptionParser
import sys
import yaml
import pika
import json
import graphviz as gv
import datetime
import time
from threading import Thread, Lock
from multiprocessing import Process
from .methods.correlations import create_cor_relations
from .methods.clustering import estimate_time_clusters
from .methods.out_of_border import get_out_of_border_items
from .methods.predict import get_bad_predicted_items
import logging

logging.basicConfig(level=logging.INFO)

credentials = {}
influx_credentials = {}
notice_time = 0
delta_time = time.time()
disabled_features = []


def get_name(i):
    return(i['message']['hostname'] + " " + i['message']['service'])


def not_same(j, i):
    """Compare two messages and return if they are the same.

    In sense of it hostname and service name are the same.
    """
    return \
        j['message']['hostname'] + " " \
        + j['message']['service'] != i['message']['hostname'] + " " \
        + i['message']['service']


def count_time_distance(j, i):
    """Compute time distance between two messages."""
    return int(abs(float(j) - float(i)))


def get_state_by_time_arrived(tm, badStated):
    """Get state from badStated by time arrived."""
    for item in badStated:
        if int(item['time_arrived']) == int(tm):
            return item
    return None


def get_sorted_times_groups(badStated):
    return sorted([
        i['time_arrived'] for i in badStated
    ])


# Threads to sub tasks


def get_correlations(badStated, delay, credentials, shared_list, lock):
    lock.acquire()
    shared_list['correlations'] = create_cor_relations(
        badStated, delay, credentials, influx_credentials)
    lock.release()


def get_clusters(inp, shared_list, lock):
    lock.acquire()
    shared_list['clusters'] = estimate_time_clusters(inp)
    lock.release()


def get_predictions(credentials, shared_list, lock):
    lock.acquire()
    shared_list['predicted_fail_items'] = get_bad_predicted_items(credentials)
    lock.release()


def get_border_items(credentials, influx_credentials, shared_list, lock):
    lock.acquire()
    shared_list['out_of_border_items'] = get_out_of_border_items(
        credentials, influx_credentials)
    lock.release()


def create_graph_and_relations(
        badStated, grh, credentials, influx_credentials):
    """Create directional graph."""
    global disabled_features
    if len(badStated) == 0:
        return [], ""
    # commence_time states the time calculation started
    # if it is far from the current time, everything seems to be useless
    commence_time = datetime.datetime.fromtimestamp(
        time.time()
    ).strftime("_%d-%m-%Y_%H:%M:%S")

    shared_list = {}
    lock = Lock()

    threads = []

    if 'correlations' not in disabled_features:
        print('Correlations are used!')
        threads.append(Thread(
            target=get_correlations,
            args=(
                badStated, 6 * 60, credentials, shared_list, lock, )))

    threads.append(Thread(
        target=get_clusters,
        args=(
            [tm['time_arrived'] for tm in badStated], shared_list, lock, )))

    threads.append(Thread(
        target=get_predictions,
        args=(credentials, shared_list, lock, )))

    if 'out_of_border_items' not in disabled_features:
        print('Out of border items are used!')
        threads.append(Thread(
            target=get_border_items,
            args=(credentials, influx_credentials, shared_list, lock, )))

    for thread in threads:
        thread.start()

    logging.debug("Started all threads for analysis")

    for thread in threads:
        thread.join()

    if 'out_of_border_items' not in disabled_features:
        out_of_border_items = shared_list['out_of_border_items']
    else:
        out_of_border_items = []

    if 'correlations' not in disabled_features:
        correlations = shared_list['correlations']
    else:
        correlations = {}

    clusters = shared_list['clusters']
    predicted_fail_items = shared_list['predicted_fail_items']

    # out_of_border_items = shared_list['out_of_border_items']
    # create cor graph from ten minut data
    # correlations = create_cor_relations(badStated, 10*60, credentials)
    # correlations = {}  # just for debugging purposes

    # Estimating groups of items which are noticed at the same time
    # this is done by KDE
    # clusters = estimate_time_clusters(
    #     [tm['time_arrived'] for tm in badStated])

    # go through the data and try to figure out if there is some prediction
    # stating there is some future problem
    # if so, add node with caption stated name and purpose
    # predicted_fail_items = get_bad_predicted_items(credentials)

    # Estimate states, which can be considered failing
    # cause they are out of 90 percentil of their data.
    # out_of_border_items = get_out_of_border_items(credentials)

    print("clusters")
    print(clusters)
    print("out_of_border_items")
    print(out_of_border_items)

    latest_name = ""

    timely_related_alerts = get_sorted_times_groups(badStated)

    print("timely_related_alerts")
    print(timely_related_alerts)

    last_name = ""  # just a pointer to a last used named in list
    j = 0
    for i in timely_related_alerts:
        state = get_state_by_time_arrived(i, badStated)
        if j == 0:
            # setting atributed of graph node
            dtime = datetime.datetime.fromtimestamp(
                int(state['time_arrived'])
            ).strftime('%H:%M:%S %d.%m.%Y')
            name = get_name(state)
            # appending node
            grh.node(
                # consider some render function, this is sad
                name.replace(":", ""),
                color='red',
                label="Non zero trigger for:\n" + name + "\nLast detected: " + dtime)
            last_name = name
        elif j != 0 and j < len(timely_related_alerts) - 1:
            # setting atributed of graph node
            dtime = datetime.datetime.fromtimestamp(
                int(state['time_arrived'])
            ).strftime('%H:%M:%S %d.%m.%Y')
            name = get_name(state)
            # appending node
            grh.node(
                name.replace(":", ""),
                color='red',
                label="Non zero trigger for:\n" + name + "\nLast detected: " + dtime)
            # creating edge to last known node
            grh.edge(
                last_name.replace(":", ""),
                name.replace(":", ""),
                label="Time detected " + dtime
            )
            last_name = name
        elif j == len(timely_related_alerts) - 1:
            dtime = datetime.datetime.fromtimestamp(
                int(state['time_arrived'])
            ).strftime('%H:%M:%S %d.%m.%Y')
            name = get_name(state)
            latest_name = name
            grh.node(
                name.replace(":", ""),
                color='red',
                label="Non zero trigger for:\n" + name + "\nLast detected: " + dtime)
            # creating edge to last known node
            grh.edge(
                last_name.replace(":", ""),
                name.replace(":", ""),
                label="Time detected " + dtime
            )
        j = j + 1

    if len(latest_name) == 0:
        latest_name = last_name

    # Draw graph for out_of_border_items:
    # - for each item select the closest bad state in badStates
    #   - create an edge between them
    for item in out_of_border_items:
        grh.node(
            name=item['name'].replace(":", ""),
            label=item['reason']
        )
        mnd = 98589934590
        closest_name = ""
        for j in badStated:
            # Counting minimal time distance
            time_distance = count_time_distance(
                j['time_arrived'], item['dtime'])
            if time_distance < mnd:
                mnd = time_distance
                closest_name = get_name(j)
        grh.edge(
            closest_name.replace(":", ""),
            item['name'].replace(":", "")
        )

    # Draw graph for predicted_fail_items
    for i in predicted_fail_items:
        grh.node(
            name="Predicted " + i['name'].replace(":", ""),
            label=i['label'],
            color='coral')
        if len(latest_name) != 0:
            grh.edge(
                latest_name.replace(":", ""),
                "Predicted " + i['name'].replace(":", ""))

    # going through each cluster
    j = 0
    for cluster in clusters:
        if len(cluster) == 1:
            continue
        # getting current times to assigne group to
        with grh.subgraph(name="cluster_" + str(j)) as s:
            time_caption = 0
            # print "cluster {0}: {1}".format(k, X[my_members, 0])
            for item in cluster:
                # print item
                # print badStated
                i = get_state_by_time_arrived(item, badStated)
                time_caption = \
                    i['time_arrived'] if time_caption == 0 else time_caption
                name = get_name(i)
                s.node(
                    name.replace(":", ""))
            dtime = datetime.datetime.fromtimestamp(
                int(time_caption)
            ).strftime('%H:%M:%S %d.%m.%Y')
            s.node_attr.update(style='filled')
            s.attr(label="Noticed at the similar time " + dtime)
            s.attr(color='blue')
        j = j + 1

    # Creating edges for highly correlated items
    for correlated_item in correlations.keys():
        for correlated_nodes in correlations[correlated_item].keys():
            if correlations[correlated_item][correlated_nodes] > 0.8:
                grh.node(correlated_nodes.replace(":", ""), color='indianred')
                grh.edge(
                    correlated_item.replace(":", ""),
                    correlated_nodes.replace(":", ""),
                    label="Correlated")

    # write result to the file
    return grh, commence_time


def msgInBad(message, badStated):
    """Check if the message was bad in last time as well."""
    for omsg in badStated:
        if omsg['message']['hostname'] == message['hostname'] and \
           omsg['message']['service'] == message['service']:
            return True


def remBasState(message, badStated):
    """Remove message from bad states."""
    tmp = []
    for omsg in badStated:
        if not (omsg['message']['hostname'] == message['hostname'] and
                omsg['message']['service'] == message['service']):
                tmp.append(omsg)
    return tmp


def riseCounter(message, badStated):
    """Remove message from bad states."""
    tmp = []
    for omsg in badStated:
        if not (omsg['message']['hostname'] == message['hostname'] and
                omsg['message']['service'] == message['service']):
                tmp.append(omsg)
        else:
            tmp.append(
                {
                    'failed': omsg['failed'] + 1,
                    'message': omsg['message'],
                    'time_arrived': omsg['time_arrived']
                }
            )
    return tmp


badStated = []


# flag if proccess is already running
proc_runner = None
# if timer of running proccess went too far, just kill it
crisis_timer = 0


def count_graph(bad_states_frame, credentials, influx_credentials):
    """Thread for creating graph file."""
    print("Starting graph creation!")
    grh = gv.Digraph('G', format='png')
    grh, dtime = create_graph_and_relations(
        badStated,
        grh,
        credentials,
        influx_credentials
    )
    if dtime == "":
        return
    print("=============================================================")
    print("=============================================================")
    print("=========================RENDERING===========================")
    print("=============================================================")
    print("=============================================================")
    grh.render('static/hows/graphs/graph_' + dtime)


def currentEval(ch, method, properties, body):
    """Evaluate current messages."""
    global badStated, notice_time, delta_time, proc_runner, crisis_timer, \
        credentials, influx_credentials
    # print("Received messages!")
    message = json.loads(body.decode("utf-8"))
    # for message in messages:
    if isinstance(message, list):
        print("Message is list, leaving!")
        return

    if message['state'] != 0 and not msgInBad(message, badStated):
        badStated.append(
            {
                'time_arrived': int(
                    message['time_execution']
                ) ^ (
                    int(message['time_execution']) & 63
                ),
                'failed': 0,
                'message': message
            }
        )
    # we know what is ok since we are getting everything from server
    elif message['state'] == 0 and msgInBad(message, badStated):
        # removing resource from bad list
        badStated = remBasState(message, badStated)
    elif message['state'] != 0 and msgInBad(message, badStated):
        # rising counter
        badStated = riseCounter(message, badStated)

    # if len(badStated) > 2:
    #     print(badStated)
    #     count_graph(badStated, credentials)
    # p = Process(target=count_graph, args=(badStated, credentials, ))
    # p.start()
    # if (delta_time + notice_time) < time.time():
    #     print(json.dumps(badStated, indent=2))
    #     p = Process(target=count_graph, args=(badStated, credentials, ))
    #     p.start()
    #     delta_time = time.time()
    if delta_time + notice_time < time.time():
        if proc_runner is None or not proc_runner.is_alive():
            print(json.dumps(badStated, indent=2))
            proc_runner = Process(
                target=count_graph,
                args=(badStated, credentials, influx_credentials, ))
            crisis_timer = time.time()
            proc_runner.start()
            delta_time = time.time()
        elif proc_runner.is_alive() and crisis_timer < time.time() - 60 * 30:
            # proccess is running too long, going to kill it
            print("Killing anylisis proccess, it took too long")
            proc_runner.terminate()

    # print (json.dumps(badStated, indent=4))
    # print ("===========================")


def main():
    global credentials, notice_time, disabled_features, influx_credentials
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--rabbit",
        action="store",
        default="./credentials/rabbit.yml",
        dest="rabbit",
        type="string",
        help="Transfer data to rabbitmq")
    parser.add_option(
        "--conf",
        action="store",
        default="./configuration/basics.yml",
        dest="conf",
        type="string",
        help="Basic configuration")
    parser.add_option(
        "--influx",
        action="store",
        default="./credentials/influxdb.yml",
        dest="influx",
        type="string",
        help="Influx database credentials")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.conf, 'r') as f:
        basicConfigurations = yaml.load(f)

    if 'disabled_features' in basicConfigurations.keys():
        disabled_features = basicConfigurations['disabled_features']

    if 'collect_delay' in basicConfigurations.keys():
        notice_time = basicConfigurations['collect_delay']

    with open(options.influx, 'r') as f:
        influx_credentials = yaml.load(f)

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)
        credentials = credentials['database']

    with open(options.rabbit, 'r') as f:
        rabbit_credentials = yaml.load(f)

    rabbit_credentials = rabbit_credentials['rabbit']
    influx_credentials = influx_credentials['influxdb']

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            rabbit_credentials['address']
        ))
    channel = connection.channel()
    channel.queue_declare(
        queue='collect',
        durable=True)
    channel.basic_consume(currentEval,
                          queue='collect',
                          no_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == "__main__":
    main()
