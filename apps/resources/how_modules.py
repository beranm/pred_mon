import time
import datetime


def get_how_description(name, graphs_directory):
    """Prepare data to describe how in list."""
    graphs = [
        f for f in listdir(
            graphs_directory
        ) if isfile(
            join(graphs_directory, f)
        ) and not f.endswith(".png")
        ]
    graphs = sorted(graphs)
    graphs_len = len(graphs)
    idx = graphs.index(name)
    stamp = time.mktime(
        datetime.datetime.strptime(
            name, "graph_%d-%m-%Y_%H:%M:%S").timetuple())
    dtime = datetime.datetime.fromtimestamp(
        int(stamp)
    ).strftime('%d-%m-%Y %H:%M:%S')
    description = {}
    description['name'] = name
    description['timestamp'] = stamp
    description['time_taken'] = dtime
    description['png_file'] = "hows/" + name + ".png"
    if idx < graphs_len - 1:
        next_item = graphs[idx + 1]
        links = []
        links.append(
            {'href': local_url + "/how/" + next_item, 'rel': 'next'}
        )
        description['_links'] = links
