
from optparse import OptionParser
import yaml
import sys
import time
import os
import glob


predictions_path = "static/classify/graphs"
hows_path = "static/hows/graphs"


def erase_old(dir_path, permitted_size):
    files = filter(os.path.isfile, glob.glob(dir_path + "*"))
    files.sort(key=lambda x: os.path.getmtime(x))
    while len(files) > permitted_size:
        os.remove(files[0])
        files.pop(0)


def main():
    global credentials, stdyml, channel, delay
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--conf",
        action="store",
        default="./configuration/basics.yml",
        dest="conf",
        type="string",
        help="Basic configuration")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.conf, 'r') as f:
        basicConfigurations = yaml.load(f)

    while True:
        erase_old(
            predictions_path, basicConfigurations['maximum_prediction_graphs'])
        erase_old(
            hows_path, basicConfigurations['maximum_hows_graphs'])
        print("Cleaned directories")
        time.sleep(10)


if __name__ == "__main__":
    main()
