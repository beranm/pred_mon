import re


class dolars:
    """Dolars iterator for parsing Zabbix related expressions in triger function."""

    def __init__(self):
        self.i = 1

    def __iter__(self):
        return self

    def next(self):
        i = self.i
        self.i += 1
        return "$" + str(i)


def parseKeys(keys):
    """Create list of keys."""
    regs = re.compile("\[(.*)\]")
    fields = []
    if len(regs.findall(keys)) >= 1:
        tmp_fields = regs.findall(keys)[0].split(",")
        for i in tmp_fields:
            fields.append(i)
    return fields


def parseName(name, keys):
    """Create name of item from dolar variables and keys."""
    dolar = dolars()
    keys = parseKeys(keys)
    for key in keys:
        name = name.replace(dolar.next(), key)
    # return re.escape(name)
    return name


def convert_to_seconds(s):
    """Converts string like 1m, 1h, ... to seconds."""
    try:
        return int(s)
    except ValueError:
        seconds_per_unit = {
            "s": 1,
            "m": 60,
            "h": 3600,
            "d": 86400,
            "w": 604800
        }
        return int(s[:-1]) * seconds_per_unit[s[-1]]
