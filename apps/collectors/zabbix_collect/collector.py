import sys
import logging
from pyzabbix import ZabbixAPI
from optparse import OptionParser
import time
import requests
import fnmatch
import yaml
import peewee
from peewee import *
from modules.Database import *
from modules.influx_connector import influx_connector as infc
from modules.relation_database import relation_database as rdb
import json
from modules.Iterators import \
    randomStuff, lineZeroes, \
    sinusTime, lineTime, \
    GrowingSinusTime, IterativeSinus
from collectors.test_modules import send_test_point
from collectors.zabbix_collect.zabbix_parsers import parseName
from collectors.zabbix_collect.zabbix_parsers import convert_to_seconds
from amqplib import client_0_8 as amqp
from apscheduler.schedulers.background import BackgroundScheduler
from multiprocessing import Process, Lock, JoinableQueue
import gc
from datetime import datetime
import pandas as pd
import re

logging.basicConfig(level=logging.INFO)


logging.getLogger('apscheduler').setLevel(logging.WARNING)


sched = BackgroundScheduler()
sched.start()
job_list = []
hostnames = ""
predict_items_delay = 8
queue = JoinableQueue()
dumped_items = []
influx_credentials = {}
ignore_services = []


def is_in(item):
    global job_list
    for i in job_list:
        if i[0]['id'] == item:
            return True
    return False


def job_delay_changed(item, delay):
    global job_list
    for i in job_list:
        if i[0]['id'] == item and i[2] != delay:
            return True
    return False


def remove_job_from_scheduler(item):
    global job_list, sched
    tmp_job_list = []
    for i in job_list:
        if (i['hostname'], i['name']) == item[0]['id']:
            job = i[1]
            job.remove()
        else:
            tmp_job_list.append(i)
    job_list = tmp_job_list


def add_job_to_scheduler(item, services, predict_items_delay):
    global hostnames, job_list, sched
    delay = int(item[2] / 60)
    if delay == 0:
        delay = 1
    job = sched.add_job(
        send_item,
        'interval',
        minutes=delay,
        args=[
            services[0],  # zapi api
            services[1],  # database_credentials
            services[2],  # mq options
            item[0],  # hostname
            hostnames,  # hostnames
            item[1],  # item id
            predict_items_delay],
        misfire_grace_time=180)  # item predict delay for predictors
    job_list.append([{'id': (item[0], item[1])}, job, item[2]])


def getItemById(zapi, idi):
    """Return item by id."""
    return zapi.item.get(output='extend', itemids=[idi])


def getBadItems(zapi):
    """Return all the items which are in triggers classified as in troubles."""
    while True:
        try:
            triggers = zapi.trigger.get(
                only_true=1,
                skipDependent=1,
                monitored=1,
                active=1,
                output='extend',
                selectFunctions='extend',
                expandDescription=1,
                selectHosts=['host'],
                filter={"value": 1},
            )
            break
        except requests.exceptions.ConnectionError:
            logging.warning("Zabbix stalling connection! Trying to wait.")
            time.sleep(1)
    times = {}
    ids = []
    for i in triggers:
        for j in i['functions']:
            ids.append(j['itemid'])
            times[j['itemid']] = i['lastchange']
    return ids, times


def fetch_all_data(
    hostname, service, itemid, delay,
    zapi, _influx, database_credentials
):
    logging.info("Fetching all the data for item")
    _rdb = rdb(database_credentials)
    time_till = time.mktime(datetime.now().timetuple())
    time_from = time_till - 60 * 60 * 2 * 24
    service = re.escape(service)
    # Query item's history (integer) data
    history = zapi.history.get(itemids=[itemid],
                               time_from=time_from,
                               time_till=time_till,
                               output='extend',
                               limit='20000',
                               )

    # If nothing was found,
    #   try getting it from history (float) data
    if not len(history):
        history = zapi.history.get(itemids=[itemid],
                                   time_from=time_from,
                                   time_till=time_till,
                                   output='extend',
                                   limit='20000',
                                   history=0,
                                   )
    _rdb.insert_hostname(hostname)
    _rdb.insert_service(hostname, service)

    notice_time = [
        datetime.fromtimestamp(
            int(i['clock'])
        ).strftime('%Y-%m-%dT%H:%M:%SZ')
        for i in history
    ]
    results = [float(i['value']) for i in history]

    df = pd.DataFrame(
        {
            "result": results,
            "state": [0] * len(history),
            "delay": [int(delay)] * len(history)
        }, index=pd.to_datetime(
            notice_time, format='%Y-%m-%dT%H:%M:%SZ', errors='coerce'))
    # print(df.dtypes)
    _influx.insert_frame(hostname, service, df)
    return True


def proc_sender(q, lock):
    logging.info("Starting consumer")
    global zapi, influx_credentials, ignore_services
    work_counter = 0
    predicted_cought_items = 0
    _influx = infc(influx_credentials)
    while(1):
        logging.info("Waiting for message")
        (
            database_credentials,
            channel_path, hostname, hostnames, itemid,
            predict_items_delay
        ) = q.get()

        logging.info("Asking " + str(itemid))

        badItems, times = getBadItems(zapi)
        list_item = zapi.item.get(output="extend", itemids=[itemid])

        logging.info("Got bad items")

        for item in list_item:  # this should be obviously not needed
            # there is always one item id
            # check if item is not ignored
            leave_ignored_item = False
            service = parseName(item['name'], item['key_'])
            for ignore_service in ignore_services:
                if re.match(ignore_service, service):
                    leave_ignored_item = True
                    break
            if leave_ignored_item is True:
                continue
            # =============================

            if item['itemid'] != itemid:
                continue

            if item['lastclock'] != "0":
                if item['itemid'] in badItems:
                    print (
                        "Failed item: " +
                        hostname +
                        " service " +
                        service +
                        " time " +
                        times[item['itemid']])
                    # it already failed, that's why
                    use_time = times[item['itemid']]
                else:
                    use_time = item['lastclock']
                # setting locals to save to db
                state = (
                    1 if item['itemid'] in badItems
                    else 0
                )
                lastclock = datetime.fromtimestamp(
                    int(item['lastclock'])
                ).strftime('%Y-%m-%dT%H:%M:%SZ')
                try:
                    record = float(item['lastvalue'])
                except ValueError:
                    logging.debug(
                        "Last value maybe not int or float, only numeric values can by used")
                    record = -1
                delay = convert_to_seconds(item['delay'])
                service = parseName(item['name'], item['key_'])
                # ==============================
                _influx.insert_data(
                    hostname,
                    service,
                    lastclock,
                    state,
                    record,
                    delay
                )
                connection = amqp.Connection(
                    host=channel_path,
                    virtual_host="/",
                    insist=False)
                channel_collect = connection.channel()
                channel_predict = connection.channel()
                channel_predict.queue_declare(
                    queue="predict",
                    durable=True,
                    exclusive=False,
                    auto_delete=False)
                channel_collect.queue_declare(
                    queue="collect",
                    durable=True,
                    exclusive=False,
                    auto_delete=False)
                channel_predict.exchange_declare(
                    exchange="predmon_exchange",
                    type="direct",
                    durable=True,
                    auto_delete=False)
                channel_collect.exchange_declare(
                    exchange="predmon_exchange",
                    type="direct",
                    durable=True,
                    auto_delete=False)
                channel_collect.queue_bind(
                    queue="collect",
                    exchange="predmon_exchange")
                channel_predict.queue_bind(
                    queue="predict",
                    exchange="predmon_exchange")
                try:
                    toqueue = {
                        'hostname': hostname,
                        'service': service,
                        'state': state,
                        'time_execution': use_time,
                        'result': record}
                    message = amqp.Message(json.dumps(toqueue))
                    if predicted_cought_items % predict_items_delay == 0:
                        channel_predict.basic_publish(
                            message,
                            exchange='',
                            routing_key='predict')
                    predicted_cought_items = predicted_cought_items + 1
                    channel_collect.basic_publish(
                        message,
                        exchange='',
                        routing_key='collect')
                except peewee.IntegrityError:
                    print ("Error...")
                    print (item['state'])
                    print (parseName(item['name'], item['key_']))
                    print (hostname)
                    print (item['lastclock'])
                    print (str(item['lastvalue']))
                    print ("==============")
                channel_collect.close()
                channel_predict.close()
                connection.close()
                work_counter = work_counter + 1

                # check if database has less than 1000 items of this data
                ##########################################################
                # in hostname, service, zapi, _influx, database_credentials
                # out true if done
                current_data_service_hostname_count = 0
                if (hostname, service) not in dumped_items:
                    current_data_service_hostname_count = _influx. \
                        get_data_count(
                            hostname, service)
                if (hostname, service) not in dumped_items \
                        and current_data_service_hostname_count > 1000:
                    lock.acquire()
                    dumped_items.append((hostname, service))
                    lock.release()
                else:
                    logging.info("Asking to fetch the data.")
                    if fetch_all_data(
                        hostname, service, itemid, delay,
                        zapi, _influx, database_credentials
                    ):
                        lock.acquire()
                        dumped_items.append((hostname, service))
                        lock.release()
                ##########################################################
                if work_counter % 100 == 0:
                    print("Pushed " + str(work_counter) + " items")
        q.task_done()


def send_item(
        zapi, database_credentials,
        channel_path, hostname, hostnames, itemid,
        predict_items_delay):
    # logging.info("Collecting " + str(itemid))
    queue.put_nowait(
        (
            database_credentials,
            channel_path, hostname, hostnames, itemid,
            predict_items_delay
        )
    )


def main():
    global threads_pointers, hostnames, predict_items_delay, queue, \
        zapi, influx_credentials, \
        ignore_services
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--ici",
        action="store",
        default="./credentials/zabbix.yml",
        dest="zabbix",
        type="string",
        help="Zabbix credentials")
    parser.add_option(
        "--conf",
        action="store",
        default="./configuration/basics.yml",
        dest="conf",
        type="string",
        help="Basic configuration")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Mysql database credentials")
    parser.add_option(
        "--rabbit",
        action="store",
        default="./credentials/rabbit.yml",
        dest="rabbit",
        type="string",
        help="Rabbitmq credentials")
    parser.add_option(
        "--tests",
        action="store_true",
        dest="tests",
        default=False,
        help="Sends only the sinus and line to the predict queue")
    parser.add_option(
        "--influx",
        action="store",
        default="./credentials/influxdb.yml",
        dest="influx",
        type="string",
        help="Influx database credentials")
    parser.add_option(
        "--settings",
        action="store",
        default="./configuration/zabbix_collector_settings.yml",
        dest="settings",
        type="string",
        help="Collector settings")


    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        database_credentials = yaml.load(f)

    with open(options.zabbix, 'r') as f:
        zabbix_credentials = yaml.load(f)

    with open(options.rabbit, 'r') as f:
        rabbit_credentials = yaml.load(f)

    with open(options.influx, 'r') as f:
        influx_credentials = yaml.load(f)

    with open(options.conf, 'r') as f:
        basic_configurations = yaml.load(f)
        predict_items_delay = basic_configurations["predict_items_delay"]

    with open(options.settings, 'r') as f:
        origin = yaml.load(f)
        hostnames = origin['hostnames']
        ignore_services = origin['ignore_services']

    database_credentials = database_credentials['database']
    zabbix_credentials = zabbix_credentials['zabbix']
    rabbit_credentials = rabbit_credentials['rabbit']
    influx_credentials = influx_credentials['influxdb']

    # Zabbix related settings
    stream = logging.StreamHandler(sys.stdout)
    stream.setLevel(logging.DEBUG)
    log = logging.getLogger('pyzabbix')
    log.addHandler(stream)
    # log.setLevel(logging.DEBUG)

    if not options.tests:
        zapi = ZabbixAPI(zabbix_credentials['url'])
        zapi.login(zabbix_credentials['user'], zabbix_credentials['password'])

    lock = Lock()

    processes = []
    # starting threads for collecting data
    for i in range(0, 8):
        processes.append(
            Process(
                target=proc_sender,
                args=(
                    queue, lock, )
            )
        )
    for i in processes:
        i.start()

    if options.tests:
        sinuses_one = sinusTime()
        sinuses_one.name = "Sinus one"
        sinuses_two = sinusTime()
        sinuses_two.name = "Sinus two"
        lines = lineTime()
        zeroes = lineZeroes()
        it_sinus = IterativeSinus()
        randomizer = randomStuff()
        growing_sinus = GrowingSinusTime()
        while True:
            connection = amqp.Connection(
                host=rabbit_credentials['address'] + ":" + str(rabbit_credentials['port']),
                virtual_host="/",
                insist=False)
            channel = connection.channel()

            messages = []
            db = PostgresqlDatabase(
                database_credentials['name'],
                user=database_credentials['user'],
                password=database_credentials['password'], host=database_credentials['address'])
            database_proxy.initialize(db)
            db.connect()

            # This part is for generating test data
            send_test_point(db, channel, sinuses_one, messages)  # sinus
            send_test_point(db, channel, sinuses_two, messages)
            send_test_point(db, channel, lines, messages)  # lines
            send_test_point(db, channel, zeroes, messages)  # zeroes
            send_test_point(db, channel, randomizer, messages)  # randomizer
            send_test_point(db, channel, growing_sinus, messages)  # growing sinus
            send_test_point(db, channel, it_sinus, messages)  # iterative sinus

            for message in messages:
                message = amqp.Message(json.dumps(message))
                message.properties["delivery_mode"] = 2
                channel.basic_publish(
                    message,
                    exchange="",
                    routing_key="collect")
            channel.close()
            connection.close()
            time.sleep(basic_configurations['collect_delay'])
    else:
        while True:
            logging.info("Adding jobs")
            try:
                hosts = zapi.hostinterface.get(
                    output=["dns", "ip", "useip"],
                    selectHosts=["host"],
                    filter={"main": 1, "type": 1})
            except requests.exceptions.ConnectionError:
                print ("Connection error, leaving for next iteration")
                hosts = []

            for h in hosts:
                hostname = h['hosts'][0]['host']
                if fnmatch.fnmatch(hostname, hostnames):
                    try:
                        list_item = zapi.item.get(output="extend", hostids=[h['hosts'][0]['hostid']])
                    except requests.exceptions.ConnectionError:
                        print ("Failed connection, continue")
                        continue
                    for item in list_item:
                        if not is_in((hostname, parseName(item['name'], item['key_']))):
                            add_job_to_scheduler(
                                (
                                    hostname,
                                    item['itemid'],
                                    convert_to_seconds(item['delay'])),
                                (
                                    zapi,
                                    database_credentials,
                                    rabbit_credentials['address'] + ":" + str(rabbit_credentials['port'])),
                                predict_items_delay)
                        elif job_delay_changed(
                            (hostname, parseName(item['name'], item['key_'])),
                                int(item['delay'])):
                            remove_job_from_scheduler(
                                (hostname, parseName(item['name'], item['key_'])))
                            add_job_to_scheduler(
                                (hostname, item['itemid'], convert_to_seconds(item['delay'])),
                                (
                                    zapi,
                                    database_credentials,
                                    rabbit_credentials['address'] + ":" + str(rabbit_credentials['port'])),
                                predict_items_delay)
                    # print item
                    # exit(1)
            gc.collect()
            time.sleep(basic_configurations['collect_delay'])


if __name__ == "__main__":
    main()
