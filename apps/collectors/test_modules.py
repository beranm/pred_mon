import time
from modules.Database import *
import json
from amqplib import client_0_8 as amqp


def send_test_point(db, channel, point_generator, messages):
    """Generate points from various iterators and pass them to the queue."""
    item = point_generator.next()
    new_rec = Record(
        service=point_generator.service,
        hostname=point_generator.name,
        state=point_generator.get_state(),
        time_execution=str(int(time.time())),
        result=item)
    new_rec.save()
    toqueue = {
            'hostname': new_rec.hostname,
            'service': new_rec.service,
            'state': new_rec.state,
            'time_execution': new_rec.time_execution,
            'result': new_rec.result}
    messages.append(toqueue)
    message = amqp.Message(json.dumps(toqueue))
    channel.basic_publish(
        message,
        exchange='',
        routing_key='predict')

    db.close()
    print(
        new_rec.hostname + " " + new_rec.service + ": "
        + str(new_rec.result) + " state: " + str(new_rec.state))
