#!/usr/bin/env python
import requests
import json
import time
import yaml
from optparse import OptionParser
from modules.DataParser import DataParser
import peewee
from peewee import *
from modules.Database import *
import sys


def main():
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--ici",
        action="store",
        default="./collectors/icinga_collect/credentials/icinga.yml",
        dest="icinga",
        type="string",
        help="Icinga credentials")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        database_credentials = yaml.load(f)

    with open(options.icinga, 'r') as f:
        icinga_credentials = yaml.load(f)

    with open("./collectors/icinga_collect/configure/hostnames.yml", 'r') as f:
        hostnames = yaml.load(f)
        hostnames = hostnames['hostnames']

    database_credentials = database_credentials['database']
    icinga_credentials = icinga_credentials['icinga']

    request_url = icinga_credentials['url']

    headers = {'Accept': 'application/json',
               'X-HTTP-Method-Override': 'GET'}

    data = {"attrs": ["name", "state", "last_check_result"],
            "joins": ["host.name", "host.state", "host.last_check_result"],
            "filter": "match(\"" + hostnames +"\", host.name)", }

    print icinga_credentials['pki_path']

    while True:

        r = requests.post(request_url,
                          headers=headers,
                          auth=(icinga_credentials['user'],
                                icinga_credentials['password']),
                          data=json.dumps(data),
                          verify=icinga_credentials['pki_path'])

        if (r.status_code == 200):

            db = PostgresqlDatabase(
                database_credentials['name'],
                user=database_credentials['user'],
                password=database_credentials['password'], host=database_credentials['address'])

            database_proxy.initialize(db)

            db.connect()

            parser = DataParser()

            jsn = r.json()
            if 'results' in jsn:
                for line in jsn['results']:
                    inp = {}
                    inp['timestamp'] = parser.parseTimestamp(line)
                    inp['name'] = line['name']
                    inp['hostname'] = parser.parseName(
                        line['name'])['hostname']
                    inp['service'] = parser.parseName(
                        line['name'])['service']
                    inp['state'] = parser.parseState(line)
                    inp['value'] = parser.parseValue(
                        inp['service'], parser.parseLastState(line)
                    )
                    new_rec = Record(service=inp['service'],
                                     hostname=inp['hostname'],
                                     state=inp['state'],
                                     time_execution=inp['timestamp'],
                                     result=str(inp['value']))
                    try:
                        new_rec.save()
                    except peewee.IntegrityError:
                        print "Duplicit entry, continue..."
                        print inp['state']
                        print inp['service']
                        print inp['hostname']
                        print inp['timestamp']
                        print str(inp['value'])
            db.close()
        else:
            print r.text
            r.raise_for_status()

        time.sleep(icinga_credentials['delay'])


if __name__ == "__main__":
    main()
