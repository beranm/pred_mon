#!/usr/bin/env python

import requests, json

# Replace 'localhost' with your FQDN and certificate CN
# for SSL verification
request_url = "https://slave.rocksys.cz:5665/v1/objects/services"
headers = {
        'Accept': 'application/json',
        'X-HTTP-Method-Override': 'GET'
        }
data = {
        "attrs": [ "name", "state", "last_check_result" ],
        "joins": [ "host.name", "host.state", "host.last_check_result" ],
        #"filter": "match(\"app-kn-*\", host.name)",
}

r = requests.post(request_url,
        headers=headers,
        auth=('martin.beranek', 'KFhIHeaXPiNXPiN'),
        data=json.dumps(data),
        verify="../pki/ca.crt")

if (r.status_code == 200):
        print json.dumps(r.json())
else:
        print r.text
        r.raise_for_status()