#!/usr/bin/env python

import requests, json, time, datetime

logdir = "./data/"

# Replace 'localhost' with your FQDN and certificate CN
# for SSL verification
request_url = "https://slave.rocksys.cz:5665/v1/objects/services"

headers = {
        'Accept': 'application/json',
        'X-HTTP-Method-Override': 'GET'
}

data = {
        "attrs": [ "name", "state", "last_check_result" ],
        "joins": [ "host.name", "host.state", "host.last_check_result" ],
        "filter": "match(\"*.3mwcloud.cz\", host.name)",
}

while True:

        r = requests.post(request_url,
                headers=headers,
                auth=('martin.beranek', 'KFhIHeaXPiNXPiN'),
                data=json.dumps(data),
                verify="./pki/ca.crt")
                        
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')
        fs = open(logdir + st + ".json","w")

        if (r.status_code == 200):
                fs.write(json.dumps(r.json()))
        else:
                print r.text
                r.raise_for_status()

        fs.close()
        time.sleep(60)
