# =====================
# Collectors part:
# Policy states what should happen if one of many collectors FloatField
#
# option live_until_all states that if for example zabbix
#    collector fail icinga collector would not stop and continue
#    (and wise versa).
#    Its main use is for duplicate monitoring on same resources.
#
# option die_with_one states if one collector dies, other are killed as well.
#    On Docker use this would be solved with stop policy and whole container
#    would start again with all collectors.
#
policy="die_with_one";
collect=("zabbix");
# tests="True";
[[ -z "${tests}" ]] && tests="False";

# prediction workers to create prediction and pass it for classification
max_prediction_workers=4;
