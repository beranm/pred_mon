"""Linear classification of false/true states."""
from optparse import OptionParser
import yaml
import sys
from amqplib import client_0_8 as amqp
import json
from modules import dataGetter
from sklearn.naive_bayes import GaussianNB
# from sklearn.linear_model import LogisticRegression
from modules import farbishData
from modules.Graphy import Graphy
from peewee import *
from modules.Database import *
from modules.time_handlers import app_time_to_timestamp
# from threading import Thread
from modules.threshold_handler import get_latest
from modules.influx_connector import influx_connector as infc
import time

influx_credentials = dict()
credentials = dict()
delay = 30


def classify(body):
    """Wait on comming message."""
    global credentials, delay, influx_credentials
    """
    TODO: add code for ICINGA!
    - load message
    - ask classifier to estimate classmessage['
        - if data are classified badly, sends warning
    - retrain classifier / adaptivly change classifier
    """
    message = json.loads(body)
    print("==================")
    print("Hostname: " + message['hostname'])
    print("Service:  " + message['service'])
    print("Time:  " + message['time_forward'])

    _influx = infc(influx_credentials)

    max_th = get_latest(
        credentials, message['hostname'], message['service'],
        "ninety_percentil")
    min_th = get_latest(
        credentials, message['hostname'], message['service'],
        "min")

    states = []
    graphy = Graphy(
        message['hostname'],
        message['service'])

    if max_th is None or min_th is None \
       or max_th.created < time.time() - (60 * 60) \
       or min_th.created < time.time() - (60 * 60):
        # use some arbitrary classifier if there is no threshold
        #   or threshold is old
        clsf = dataGetter.getClassifier(
            message['hostname'],
            message['service'],
            message['service'], credentials, type='Classify')
        if clsf is None:
            print("None classifier found, training new one!")
            X, Y = _influx.get_data_to_classify(
                message['hostname'], message['service'])
            # print X
            # print Y
            clsf = GaussianNB()
            clsf.partial_fit(X, Y, [0, 1])
            dataGetter.saveClassifier(
                clsf, message['hostname'],
                message['service'],
                credentials, type='Classify')

        try:
            for i in message['data']:
                states.append(clsf.predict(i)[0])
        except ValueError:
            print("============================FAIL===========================")
            print("hostname: " + message['hostname'])
            print("service: " + message['service'])
            print(message['data'])
            print("===========================================================")
    else:
        for i in message['data']:
            if i > max_th.threshold:
                states.append(1)
            elif i < min_th.threshold:
                states.append(1)
            else:
                states.append(0)
    """
    Take predicted data + data from past with same len
        - create graph
            - future in different color
            - mark bad states
    """
    # try:
    dataFromDirectPast = _influx.get_data_limit_from_date(
        message['hostname'],
        message['service'],
        40,  # limit of recors questioned
        message['time_forward'])
    # except ValueError:
    #     print ("Not enough data to create any predictions")
    #     return

    dataPredicted = farbishData.prepareData(
        # consider refactoring since this doesn't get any data,
        #  it just create dataframe from known data.
        message['data'],
        states,
        message['time_forward'],
        delay)

    graphy.plotPastFuture(dataFromDirectPast, dataPredicted)
    fail = False
    if 1 in states:
        fail = True

    valid_until = dataPredicted.index.max()

    print("Noticed at:  " + str(message['time_forward']))
    print("Valid until: " + str(valid_until))

    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    new_rec = Predicted(
        service=message['service'],
        hostname=message['hostname'],
        state=(
            1 if fail
            else 0
        ),
        time_noticed=app_time_to_timestamp(message['time_forward']),
        valid_until=app_time_to_timestamp(valid_until)
        )
    new_rec.save()
    db.close()


def callback(msg):
    """Consume message from queue."""
    (
       channel,
       properties,
       body
    ) = (
       msg.channel,
       msg.properties,
       msg.body
    )
    classify(body.decode())


def main():
    global credentials, influx_credentials, stdyml, channel, delay
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--conf",
        action="store",
        default="./configuration/basics.yml",
        dest="conf",
        type="string",
        help="Basic configuration")
    parser.add_option(
        "--rabbit",
        action="store",
        default="./credentials/rabbit.yml",
        dest="rabbit",
        type="string",
        help="Transfer data to rabbitmq")
    parser.add_option(
        "--influx",
        action="store",
        default="./credentials/influxdb.yml",
        dest="influx",
        type="string",
        help="Influx database credentials")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)
    with open(options.conf, 'r') as f:
        basicConfigurations = yaml.load(f)
    with open(options.rabbit, 'r') as f:
        rabbit_credentials = yaml.load(f)
    with open(options.influx, 'r') as f:
        influx_credentials = yaml.load(f)

    credentials = credentials['database']
    rabbit_credentials = rabbit_credentials['rabbit']
    influx_credentials = influx_credentials['influxdb']

    delay = basicConfigurations['collect_delay']

    connection = amqp.Connection(
        host=rabbit_credentials['address'] + ":" + str(rabbit_credentials['port']),
        virtual_host="/",
        insist=False
    )
    channel = connection.channel()
    # channel.queue_declare(queue='future')
    channel.queue_declare(
        queue='classify',
        durable=True,
        auto_delete=False)
    channel.basic_consume(
        callback=callback,
        queue='classify',
        no_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    while True:
        channel.wait()


if __name__ == "__main__":
    main()
