"""This is just an flask app to show frontend of application.
Print out images from graphviz which are generated and show user what happens.
For API user api.py not frontend.py.
"""
from flask import Flask, render_template, request
from os import listdir
from os.path import isfile, join


# graph directory for how analysis
graphs_directory = "./static/hows/graphs"
# predictions graphs directory
predictions_directory = "./static/classify/graphs"

app = Flask(__name__)


@app.route('/')
def main():
    return render_template('index.html')


@app.route('/predictions', methods=['GET'])
def predictions():
    graphs = [
        f for f in listdir(
            predictions_directory
        ) if isfile(
            join(predictions_directory, f)
        ) and f.endswith(".png")
    ]
    graphs = sorted(graphs, reverse=True)
    out = {}
    for item in graphs:
        item = item.replace(".png", "")
        if isfile(predictions_directory + "/" + item + ".png"):
            hostname = item.split("!")[0]
            if hostname not in out.keys():
                out[hostname] = {}
            service = item.split("!")[1]
            if service not in out[hostname].keys():
                out[hostname][service] = []
            tmp = {}
            tmp['timestamp'] = item.split("!")[2]
            tmp['png_file'] = "graphs/" + item + ".png"
            out[hostname][service].append(tmp)

    hostname = None
    service = None
    timestamps = []
    pngs_files = []
    first_graph = None

    if request.args.get('hostname') and request.args.get('service'):
        hostname = request.args.get('hostname').replace(" ", "+")
        service = request.args.get('service').replace(" ", "+")
        if hostname not in out.keys():
            hostname = hostname + "+"
        if service not in out[hostname].keys():
            service = service + "+"
        for item in out[hostname][service]:
            if request.args.get('timestamp'):
                timestamp = request.args.get('timestamp')
                # print("timestamp " + timestamp)
                # print("item timestamp " + item['timestamp'])
                if item['timestamp'] == timestamp:
                    first_graph = item['png_file']
            # print(item['png_file'])
            timestamps.append(item['timestamp'])
            pngs_files.append(item['png_file'])

    if first_graph is None and len(pngs_files) > 0:
        first_graph = pngs_files[0]

    return render_template(
        'predictions.html',
        graphs=out,
        first_graph=first_graph,
        timestamps=timestamps,
        hostname=hostname,
        service=service)


@app.route('/hows', methods=['GET'])
def hows():
    graphs = [
        f for f in listdir(
            graphs_directory
        ) if isfile(
            join(graphs_directory, f)
        ) and not f.endswith(".png")
    ]
    graphs = sorted(graphs, reverse=True)
    out = []
    for item in graphs:
        if isfile(graphs_directory + "/" + item + ".png"):
            tmp = {}
            tmp['name'] = item
            tmp['dot_file'] = "graphs/" + item
            tmp['png_file'] = "graphs/" + item + ".png"
            out.append(tmp)
    if request.args.get('file'):
        for item in out:
            first_graph = item \
                if request.args.get('file') == item['png_file'] \
                else None
            if request.args.get('file') == item['png_file']:
                break
    elif len(out) > 0:
        first_graph = out[0]
    else:
        first_graph = None
    return render_template(
        'hows.html',
        graphs_one=out[:len(out) / 2],
        graphs_two=out[len(out) / 2:],
        first_graph=first_graph)
