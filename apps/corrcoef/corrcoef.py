"""Count correlation between datasets in database."""
import sys
import pandas as pd
import numpy
import matplotlib.pyplot as plt
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
from modules.Graphy import Graphy
from peewee import *
from modules.Database import *
from sklearn.metrics import mean_squared_error
import yaml
from modules.FileRenames import normalizeName
from math import sqrt
import matplotlib.ticker as ticker
import modules.dataGetter as dataGetter
import numpy

def readRecords(hostname, service):
    records = Record.select() \
                    .where(
                        (Record.hostname == hostname) &
                        (Record.service == service)) \
                    .order_by(Record.time_execution)
    return records


def fillLine(hostname, service, column, valids, credentials):
    out = dict()
    baseData = recpd = dataGetter.getDataFrame(
        hostname, service,
        credentials,
        delay=60)[column]
    for inHostname in valids.keys():
        print "========"
        print "+++++++++++++++++++++++++++++ " + hostname + " ++++++++++++++++++++++"
        for inService in valids[inHostname].keys():
            for inColumn in valids[inHostname][inService]:
                checkData = dataGetter.getDataFrame(
                    inHostname, inService,
                    credentials,
                    delay=60)[inColumn]
                print "=============================================="
                print "ORG: " + hostname + "++" + service + "++" + column
                print "TST: " + inHostname + "++" + inService + "++" + inColumn
                print "baseData len: " + str(len(baseData))
                print "checkData len: " + str(len(checkData))
                if len(baseData) > len(checkData):
                    baseData = baseData[-len(checkData):]
                if len(baseData) < len(checkData):
                    checkData = checkData[-len(baseData):]

                print numpy.corrcoef(baseData, checkData)[0, 1]
                print "=============================================="


def main():
    #plt.style.use('fivethirtyeight')

    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--validated",
        action="store",
        default="./validate/std_valid.yml",
        dest="valids",
        type="string",
        help="Validated data series")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)
    credentials = credentials['database']

    with open(options.valids, 'r') as f:
        valids = yaml.load(f)

    CorrMatrix = dict()

    parser = DataParser()
    hostserv = dataGetter.getHostServ(credentials)

    for hostname in valids.keys():
        print "========"
        print "+++++++++++++++++++++++++++++ " + hostname + " ++++++++++++++++++++++"
        for service in valids[hostname].keys():
            for column in valids[hostname][service]:
                CorrMatrix[hostname + service + column] = \
                    fillLine(
                        hostname, service, column, valids, credentials)


    for line in hostserv:
        print line.hostname + " " + line.service
        recpd = dataGetter.getDataFrame(
            line.hostname, line.service,
            credentials,
            delay=60)
        for column in recpd.columns:
            print line.hostname + line.service + column


if __name__ == "__main__":
    main()
