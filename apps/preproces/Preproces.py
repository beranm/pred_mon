"""Preproces data for later models."""
from peewee import *
from modules.Database import *
import pandas as pd
import numpy
import matplotlib.pyplot as plt
from optparse import OptionParser
import sys
import yaml
from modules.FileRenames import normalizeName
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
from modules.Graphy import Graphy
from statsmodels.nonparametric.smoothers_lowess import lowess
from scipy.signal import savgol_filter


def smoothSavgol(input):
    """Smooth date for later use."""
    values = input.values
    index = input.index
    savgol_filter(values, 5, 2)
    return pd.DataFrame(values, index=index, columns=[input.column])

def main():
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--selected",
        action="store",
        default="./arima/services/selected.yml",
        dest="services",
        type="string",
        help="Services to smooth")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)

    credentials = credentials['database']

    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'], host=database_credentials['address'])

    database_proxy.initialize(db)

    db.connect()

    parser = DataParser()

    pprser = PandaParser()

    graphy = Graphy()

    with open(options.services, 'r') as f:
        docs = yaml.load_all(f)
        for doc in docs:
            for hostname, v in doc.items():
                for service in v['services']:
                    print(service)
                    last_date = (
                        Record
                        .select()
                        .where(
                            (Record.hostname == hostname) &
                            (Record.service == service)
                        ).order_by(Record.time_execution)
                        .desc()
                        .limit(1)
                        .get())
                    last_date = (
                        last_date.time_execution -
                        (60 * 60 * 24 * 7))
                    records = (Record
                               .select()
                               .where(
                                     (Record.hostname == hostname) &
                                     (Record.service == service) &
                                     (Record.time_execution > last_date)
                               ).order_by(Record.time_execution)
                               )
                    sjoin = []
                    oldest_time = 0
                    for i in records:
                        if oldest_time == 0:
                            oldest_time = i.time_execution
                        # print (i.time_execution)
                        i.result = parser.parseResult(service, i.result)
                        tmp = pprser.generateTmp(i.result,
                                                 i.time_execution,
                                                 service)

                        sjoin.append(tmp)
                    if len(sjoin) == 0:
                        raise ValueError('empty list')
                    recpd = pd.concat(sjoin)
                    for column in recpd.columns:
                        smoothed = smoothSavgol(recpd[column])
                        graphy.plotPred(
                            recpd[column], smoothed)
    db.close()


if __name__ == "__main__":
    main()
