"""Validate data in database by service column per hostname."""
from peewee import *
from modules.Database import *
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
import yaml
import sys
import numpy
import pandas as pd
import statsmodels.tsa.api as smt
import modules.dataGetter as dataGetter


def autocorr(x):
    result = numpy.correlate(x, x, mode='full')
    return result[result.size/2:]

def main():
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--dump",
        action="store",
        default="./validate/std_valid.yml",
        dest="acr",
        type="string",
        help="Output yaml containing ready services")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()
    with open(options.database, 'r') as f:
        credentials = yaml.load(f)

    credentials = credentials['database']

    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'], host=credentials['address'])

    database_proxy.initialize(db)
    db.connect()
    parser = DataParser()
    pprser = PandaParser()

    outputdict = {}

    hostnames = (
        Record
        .select(Record.hostname).group_by(Record.hostname)
        .desc()
        )
    for i in hostnames:
        services = (
            Record
            .select(Record.service).group_by(Record.service)
            .where(Record.hostname == i.hostname)
            .desc()
            )
        print i.hostname
        outputdict[i.hostname] = {}
        for j in services:
            recpd = dataGetter.getDataFrame(
                i.hostname, j.service,
                credentials,
                delay=60)
            # print(recpd.head())
            print ("===========================")
            outputdict[i.hostname][j.service] = {}
            for column in recpd.columns:
                print recpd[column].describe()
                print "STD: ", recpd[column].describe()[2]
                if not numpy.isnan(recpd[column].describe()[2]) \
                        and recpd[column].describe()[2] != 0:
                    if numpy.isnan(numpy.sum(smt.acf(recpd[column]))):
                        print "Can't estimate autocorrelation"
                        outputdict[i.hostname][j.service][column] = False
                    else:
                        try:
                            print smt.acf(recpd[column])
                            print smt.pacf(recpd[column])
                            outputdict[i.hostname][j.service][column] = True
                        except:
                            print "Can't estimate anything"
                            outputdict[i.hostname][j.service][column] = False
                else:
                    outputdict[i.hostname][j.service][column] = False
                print ("+++++++++++++++++++++++++++")
            if len(outputdict[i.hostname][j.service].keys()) == 0:
                del outputdict[i.hostname][j.service]
    with open(options.acr, "w") as f:
        yaml.safe_dump(
            outputdict, f,
            default_flow_style=False,
            encoding='utf-8', allow_unicode=True)

    db.close()


if __name__ == "__main__":
    main()
