#!/usr/bin/env python
import sys
import json
from optparse import OptionParser
from IcingaParser import IcingaParser
from os import listdir
from os.path import isfile, join
import os.path
import yaml
from peewee import *
import peewee
from Database import *


def main():
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "-d",
        "--directory",
        action="store",
        dest="dirname",
        default="./data/",
        type="string",
        help="Input directory containing JSONs from Icinga")
    parser.add_option(
        "-o",
        "--out",
        action="store",
        dest="outflname",
        default="out.py",
        type="string",
        help="Output file name")
    parser.add_option(
        "-t",
        "--type",
        action="store",
        default="pyd",
        dest="otype",
        type="string",
        help="Output data type, supported pythonized json")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    files = [
        f for f in listdir(options.dirname)
        if isfile(join(options.dirname, f))]

    if options.otype == "pyd":

        parser = IcingaParser()

        results = []

        for file in sorted(files):
            with open(options.dirname + "/" + file) as content_file:
                data = content_file.read()
                jsn = json.loads(data)
                if 'results' in jsn:
                    for line in jsn['results']:
                        inp = {}
                        inp['timestamp'] = parser.parseTimestamp(line)
                        inp['name'] = line['name']
                        inp['hostname'] = parser.parseName(
                            line['name'])['hostname']
                        inp['service'] = parser.parseName(
                            line['name'])['service']
                        # inp['state'] = parseLastState(line)
                        inp['value'] = parser.parseValue(
                            inp['service'], parser.parseLastState(line)
                        )
                        inp['state'] = parser.parseState(line)
                        results.append(inp)

        with open(options.outflname, 'wa') as f:
            f.write(json.dumps(results,
                    sort_keys=True, indent=4, separators=(',', ': ')))

    elif options.otype == "csv":

        parser = IcingaParser()
        options.outflname = "out.csv"

        names = set()
        results = {}

        for file in sorted(files):
            with open(options.dirname + file) as content_file:
                data = content_file.read()
                jsn = json.loads(data)
                parser = IcingaParser()
                if 'results' in jsn:
                    for line in jsn['results']:
                        inp = {}
                        names.add(line['name'])
                        inp['service'] = parser.parseName(
                            line['name'])['service']
                        results[parser.parseTimestamp(line)] = {}
                        results[parser.parseTimestamp(line)][line['name']] = \
                            parser.parseValue(
                                inp['service'], parser.parseLastState(line)
                        )
                        inp['state'] = parser.parseState(line)

        names = sorted(names)

        with open(options.outflname, 'wa') as f:
            output_res = "Names "
            output_res = output_res + ' '.join(names) + "\n"
            f.write(output_res)
            output_res = ""
            for key in sorted(results.iterkeys()):
                output_res = str(key) + " "
                for nm in names:
                    if nm in results[key].iterkeys():
                        output_res = output_res + str(results[key][nm]) + " "
                    else:
                        output_res = output_res + "- "
                output_res = output_res + "\n"
                f.write(output_res)
                output_res = ""

    if os.path.exists(options.database) and options.otype == "dbs":

        with open(options.database, 'r') as f:
            credentials = yaml.load(f)

        credentials = credentials['database']

        db = PostgresqlDatabase(credentials['name'],
                                user=credentials['user'],
                                password=credentials['password'], host=credentials['address'])

        database_proxy.initialize(db)

        db.connect()

        parser = IcingaParser()

        for file in sorted(files):
            with open(options.dirname + "/" + file) as content_file:
                data = content_file.read()
                jsn = json.loads(data)
                if 'results' in jsn:
                    for line in jsn['results']:
                        inp = {}
                        try:
                            inp['timestamp'] = parser.parseTimestamp(line)
                            inp['name'] = line['name']
                            inp['hostname'] = parser.parseName(
                                line['name'])['hostname']
                            inp['service'] = parser.parseName(
                                line['name'])['service']
                            # inp['state'] = parseLastState(line)
                            inp['value'] = parser.parseValue(
                                inp['service'], parser.parseLastState(line)
                            )
                            inp['state'] = parser.parseState(line)

                        except (IndexError, TypeError, ValueError):
                            print("One service " + file + " data failed to be loaded, skipping!")
                            try:
                                print(parser.parseLastState(line))
                            except (IndexError, TypeError, ValueError):
                                print ("Didn't even parsed service output")
                            print("===========================")
                            try:
                                print(inp['service'])
                            except KeyError:
                                print ("Didn't even parsed service name")
                            print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+")
                            continue

                        new_rec = Record(state=inp['state'],
                                         service=inp['service'],
                                         hostname=inp['hostname'],
                                         time_execution=inp['timestamp'],
                                         result=str(inp['value']))
                        try:
                            new_rec.save()
                        except peewee.IntegrityError:
                            print "Duplicit entry, continue..."
                            print inp['state']
                            print inp['service']
                            print inp['hostname']
                            print inp['timestamp']
                            print str(inp['value'])

        # db = Database(doc)


        # with open(options.outflname, 'wa') as f:
        #    f.write(json.dumps(results, sort_keys=True, indent=4, separators=(',', ': ')))

"""
    results = sorted(results, key=lambda k: k['timestamp'])
    times = sorted(times)
    names = sorted(names)

    i = 0

    with open(options.outflname, 'wa') as f:
        output_res = "Names "
        output_res = output_res + ' '.join(names) + "\n"
        f.write(output_res)
        output_res = ""
        for tm in times:
            output_res = output_res + str(tm) + " "
            while  i < len(results) and results[i]['timestamp'] == tm:
                for nm in names:
                    if results[i]['name'] == nm:
                        output_res = output_res + str(results[i]['value']) + " "
                    else:
                        output_res = output_res + "-" + " "
                i = i + 1
                print(nm)

            output_res = output_res + "\n"
            f.write(output_res)
            output_res = ""
"""

if __name__ == "__main__":
    main()

"""
args = sys.argv[1:]
(options, args) = parser.parse_args()
if options.filename:
    if os.path.exists(options.filename):
        with open(options.filename, mode='r', encoding='utf-8') as stream:
            for intp in stream.readlines():
                istr = istr + intp
    else:
        print("Soubor neexistuje")
else:
    istr = input()






if len(sys.argv) != 2:
   sys.exit(1)

with open(sys.argv[1]) as content_file:
   data = content_file.read()

jsn = json.loads(data)


# create dict of dicts with following structure
# host : services : state

results = {}

parser = IcingaParser()

if 'results' in jsn:
   for line in jsn['results']:
      inp = {}
      inp['timestamp'] = parser.parseTimestamp(line)
      inp['hostname'] = parser.parseName(line['name'])['hostname']
      inp['service'] = parser.parseName(line['name'])['service']
      #inp['state'] = parseLastState(line)
      inp['value'] = parser.parseValue(inp['service'], parser.parseLastState(line))
      print inp
      #if inp['ids']['service'] == "ping4":
      #   print inp
         #print(json.dumps(line,sort_keys=True, indent=4, separators=(',', ': ')))
      
      #print(parseLastState(line['attrs']['last_check_result']))


#/run/user/1000/gvfs/sftp\:host=10.0.0.4\,user=pi/data/collect/data/2017-04-17_15:48:58.json
"""
