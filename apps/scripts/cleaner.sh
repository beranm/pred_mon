#!/bin/bash
#
# Running dir is app dir
#

runned_pids=();
. ./config.sh

#############################

function compare_pids(){
	while [[ -n $1 ]];do
		ps -p $1 -o comm= >/dev/null || return 1;
		shift;
	done
}

function kill_left(){
	echo "Killing left proccesses";
       	for i in ${runned_pids[@]};do
                ps -p $i -o comm= && kill $i;
                shift;
        done
	exit 1;
}

function check_pids(){
        while [[ -n $1 ]];do
                ps -p $1 -o comm= >/dev/null && return 0;
                shift;
        done
	return 1;
}

trap kill_left SIGTERM
trap kill_left SIGINT

python -m cleaner.cleaner & runned_pids+=($!)

echo "======= RUNNING PIDS OF COLLECTORS ======="
echo ${runned_pids[@]}
if [[ "$policy" == "die_with_one" ]];then
	echo "======= ON END OF ONE COLLECTOR OF THEM, ENDING ALL ======="
elif [[ "$policy" == "live_until_all" ]];then
	echo "======= WAITING ON FAIL OF ALL COLLECTORS ======="
fi

while [[ 1 ]]; do

	if [[ "$policy" == "die_with_one" ]];then
		compare_pids ${runned_pids[@]} || {
			echo "One collector failed, ending!";
			kill_left;
			exit 1;
		}

	elif [[ "$policy" == "live_until_all" ]];then

		check_pids ${runned_pids[@]} || {
			echo "None collector running, ending all!";
			exit 1;
		}
	fi
	sleep 4;
done;
