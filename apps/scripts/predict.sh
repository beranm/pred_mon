#!/bin/bash
#
# Running dir is app dir
#

function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

eval $(parse_yaml credentials/database.yml);
eval $(parse_yaml credentials/rabbit.yml);

runned_pids=();
. ./config.sh

# check if ports are in place

while [[ 1 ]]; do
	nc -zv ${database_address} 5432 && break;
	sleep 1;
done;

while [[ 1 ]]; do
	nc -zv ${rabbit_address} ${rabbit_port} && break;
	sleep 1;
done;

#############################

function compare_pids(){
	while [[ -n $1 ]];do
		ps -p $1 -o comm= >/dev/null || return 1;
		shift;
	done
}

function kill_left(){
	echo "Killing left proccesses";
       	for i in ${runned_pids[@]};do
                ps -p $i -o comm= && kill $i;
                shift;
        done
	exit 1;
}

function check_pids(){
        while [[ -n $1 ]];do
                ps -p $1 -o comm= >/dev/null && return 0;
                shift;
        done
	return 1;
}

trap kill_left SIGTERM
trap kill_left SIGINT

python -m predict.predict --process ${max_prediction_workers} & runned_pids+=($!)

echo "======= RUNNING PIDS OF COLLECTORS ======="
echo ${runned_pids[@]}
echo "======= ON END OF ONE PREDICTOR OF THEM, ENDING ALL ======="

while [[ 1 ]]; do
	compare_pids ${runned_pids[@]} || {
		echo "One collector failed, ending!";
		kill_left;
		exit 1;
	}
	sleep 4;
done;
