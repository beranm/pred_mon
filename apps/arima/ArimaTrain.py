import pandas as pd
import numpy
import statsmodels.api as sm
import statsmodels
import statsmodels.tsa.api as smt
import warnings
import itertools
import matplotlib.pyplot as plt
import yaml
from modules.Database import *
import time
import pickle
import ast
from modules.PandaParser import PandaParser
from modules.FileRenames import normalizeName

class ArimaTrain(object):

    def loadConf(self):
        self.conf_path = "./arima/configuration/arima_conf.yml"
        with open(self.conf_path, 'r') as f:
            self.config = yaml.load(f)
        self.config = self.config['service']

    def findSmallestAIC(self, y, hostname, service, column):

        print("HOSTNAME: " + hostname + " SERVICE: " + service + " COLUMN: " + column)

        warnings.filterwarnings("ignore")
        min_aic = 0
        order = (1, 1, 1)
        classifier = None

        flag = 0
        pq_rng = range(0, 8)
        ma_rng = range(0, 4)
        d_rng = range(0, 8)

        for i in pq_rng:
            for d in d_rng:
                for j in ma_rng:
                    if (i, d, j) == (0, 0, 0):
                        continue
                    try:
                        print('ARIMA: {}'.format((i, d, j)))
                        tmp_mdl = smt.ARIMA(y, order=(i, d, j))
                        results = tmp_mdl.fit(
                            method='mle', trend='nc', disp=False)

                        print(' - AIC:{}'.format(results.aic))

                        if flag == 0:
                            order = (i, d, j)
                            classifier = results
                            min_aic = results.aic
                            flag = 1
                            ttab = results.summary()
                            with open(
                                "./arima/graphs/table_" +
                                normalizeName(
                                    hostname + "!" +
                                    service + "!" +
                                    column + ".txt"
                                ), "w"
                            ) as f:

                                f.write(
                                    "AIC: " +
                                    str(results.aic) +
                                    "\n" + str((i, d, j)) +
                                    "\n" +
                                    "=================================\n" +
                                    str(ttab))
                        if results.aic < min_aic:
                            order = (i, d, j)
                            classifier = results
                            min_aic = results.aic
                            ttab = results.summary()
                            with open(
                                "./arima/graphs/table_" +
                                normalizeName(
                                    hostname + "!" +
                                    service + "!" +
                                    column + ".txt"
                                ), "w"
                            ) as f:

                                f.write(
                                    "AIC: " +
                                    str(results.aic) +
                                    "\n" + str((i, d, j)) +
                                    "\n" +
                                    "=================================\n" +
                                    str(ttab))
                    except (numpy.linalg.linalg.LinAlgError, ValueError):
                        print("FAILED!")
                        continue

        print "Estimating MA component"
        ma_rng = range(0, 30)
        lstorder = list(order)
        i = lstorder[0]
        d = lstorder[1]
        for j in ma_rng:
            try:
                print('ARIMA: {}'.format((i, d, j)))
                tmp_mdl = smt.ARIMA(y, order=(i, d, j))
                results = tmp_mdl.fit(
                    method='mle', trend='nc', disp=False)

                print(' - AIC:{}'.format(results.aic))

                if flag == 0:
                    order = (i, d, j)
                    classifier = results
                    min_aic = results.aic
                    flag = 1
                    ttab = results.summary()
                    with open(
                        "./arima/graphs/table_" +
                        normalizeName(
                            hostname + "!" +
                            service + "!" +
                            column + ".txt"
                        ), "w"
                    ) as f:

                        f.write(
                            "AIC: " +
                            str(results.aic) +
                            "\n" + str((i, d, j)) +
                            "\n" +
                            "=================================\n" +
                            str(ttab))
                if results.aic < min_aic:
                    order = (i, d, j)
                    classifier = results
                    min_aic = results.aic
                    ttab = results.summary()
                    with open(
                        "./arima/graphs/table_" +
                        normalizeName(
                            hostname + "!" +
                            service + "!" +
                            column + ".txt"
                        ), "w"
                    ) as f:

                        f.write(
                            "AIC: " +
                            str(results.aic) +
                            "\n" + str((i, d, j)) +
                            "\n" +
                            "=================================\n" +
                            str(ttab))
            except (numpy.linalg.linalg.LinAlgError, ValueError):
                print("FAILED!")
                continue

        return order, classifier


    def saveClassifier(
            self,
            mod,
            hostname, service, column,
            order, time_created,
            newone=False):
        classifier_path = "./arima/arimaxes/" + \
            normalizeName(hostname + service + column.replace("/", "") + ".p")
        with open(classifier_path, "w") as f:
            pickle.dump(mod, f)
        if newone is True:
            savetime = str(time.time())
        else:
            savetime = time_created

        new_rec = Classifier(time_created=savetime,
                             time_retrained=time.time(),
                             hostname=hostname,
                             service=service,
                             column=column,
                             classifier_path=classifier_path,
                             seasonal_pdq="None",
                             pdq=str(order),
                             )
        new_rec.save()
        classifier = (Classifier
                      .select()
                      .where(
                            (Classifier.hostname == hostname) &
                            (Classifier.column == column) &
                            (Classifier.service == service) &
                            (Classifier.pdq == str(order))
                      ).order_by(Classifier.time_created).desc().limit(1)
                       .get())
        return classifier.id



    def trainArima(self, y, hostname, service, column, pdq, time_created):
        mod = smt.ARIMA(y[column],
                        order=pdq)
        results = mod.fit(method='mle', trend='nc',disp=False)
        return self.saveClassifier(results, hostname, service, column, pdq, time_created)


    def trainClassifier(self, y, hostname, service, column, time_created):
        order, classifier = self.findSmallestAIC(y[column], hostname, service, column)
        return self.saveClassifier(classifier, hostname, service, column, order, time_created, True)

    def getClassifier(self, y, hostname, service, db):
        self.loadConf()

        classifiers = (Classifier
                       .select()
                       .where(
                             (Classifier.hostname == hostname) &
                             (Classifier.service == service)
                       ).order_by(Classifier.time_created)
                       )

        if len(classifiers) == 0:
            print "There is none classifier train for data, training!"
            retcls = []
            for column in y.columns:
                retcls.append(self.trainClassifier(y, hostname, service, column, time.time()))
            return retcls
        else:
            print "There are some classifiers found!"
            retcls = []
            colsdone = []
            for cls in classifiers:
                if time.time() > (cls.time_created + 60 * 60 * self.config['full_retrain_time']):
                    print ("Classifiers is older than full_retrain_time, new one is going to be build with new pdq!")
                    (Classifier
                     .delete()
                     .where(
                           (Classifier.hostname == hostname) &
                           (Classifier.service == service) &
                           (Classifier.time_created == cls.time_created)
                     ).execute()
                     )
                    # for column in y.columns:
                    trained = self.trainClassifier(y, cls.hostname, cls.service, cls.column, cls.time_created)
                    retcls.append(trained)
                    # return retcls
                    colsdone.append(cls.column)
                elif time.time() > (cls.time_retrained + 60 * 60 * self.config['retrain_time']):
                    print ("Classifiers for is older than retrain_time, new one is going to be build!")
                    (Classifier
                     .delete()
                     .where(
                           (Classifier.hostname == hostname) &
                           (Classifier.service == service) &
                           (Classifier.time_retrained == cls.time_retrained)
                     ).execute()
                     )
                    trained = self.trainArima(y,
                                              cls.hostname,
                                              cls.service,
                                              cls.column,
                                              ast.literal_eval(cls.pdq),
                                              cls.time_created)
                    retcls.append(trained)
                    # return retcls
                    colsdone.append(cls.column)
                else:
                    colsdone.append(cls.column)
                    classifier = (
                        Classifier
                        .select()
                        .where(
                            (Classifier.hostname == hostname) &
                            (Classifier.column == cls.column) &
                            (Classifier.service == service)
                        ).order_by(Classifier.time_created).desc().limit(1)
                         .get())

                    retcls.append(classifier.id)
            for column in y.columns:
                if column not in colsdone:
                    retcls.append(self.trainClassifier(y, hostname, service, column, time.time()))
            return retcls

    def predictFuture(self, y, id, hostname, service, db, steps=60, keys=None):
        classifier = (Classifier
                      .select()
                      .where(
                            (Classifier.hostname == hostname) &
                            (Classifier.service == service) &
                            (Classifier.id == id)
                      ).order_by(Classifier.time_created).desc().limit(1)
                       .get())
        # print("XXXXXXXXX")
        # print(classifier.id)
        # print("XXXXXXXXX")
        print("PDQ: " + classifier.pdq + " COLUMN: " + classifier.column)
        model = smt.ARIMA(y[classifier.column], order=list(ast.literal_eval(classifier.pdq)))
        try:
            model_fit = model.fit(disp=0, transparams=False)
        except ValueError:
            print("Couldn't pass trend c, trying nc")
            model_fit = model.fit(disp=0, trend='nc', transparams=False)
        try:
            prediction = model_fit.predict(start=1, end=len(y[classifier.column].index) + steps, typ='levels')
        except TypeError:
            print ("Trying without levels")
            prediction = model_fit.predict(start=1, end=len(y[classifier.column].index) + steps)
        # prediction = prediction[len(y[classifier.column].index):(len(y[classifier.column].index) + steps)]
        # preddates = pd.date_range(y[classifier.column].index[-1], periods=steps, freq='1Min')
        # prediction = prediction[len(y[classifier.column].index):(len(y[classifier.column].index) + steps)]
        # print(y[classifier.column].index)
        preddates = y[classifier.column].index
        if keys is None:
            preddates = preddates.append(pd.date_range(y[classifier.column].index[-1], periods=steps, freq='1Min'))
        else:
            preddates = preddates.append(keys)
        print(len(y[classifier.column].index))
        print(len(preddates))
        if keys is not None:
            print(len(keys))
        print(steps)
        print(len(prediction.values))
        prediction = pd.DataFrame(prediction.values, index=preddates, columns=[classifier.column])
        # print prediction

        return prediction
