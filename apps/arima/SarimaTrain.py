import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels
import warnings
import itertools
import matplotlib.pyplot as plt
import yaml
from Database import *
import os
import time
import pickle
import ast
import matplotlib.pyplot as plt

class ArimaTrain(object):

    def loadConf(self):
        self.conf_path = "./configuration/arima_conf.yml"
        with open(self.conf_path, 'r') as f:
            self.config = yaml.load(f)
        self.config = self.config['service']

    def createPDQs(self):
        p = d = q = range(0, 2)
        pdq = list(itertools.product(p, d, q))
        seasonal_pdq = [(x[0], x[1], x[2], 12)
                        for x in list(itertools.product(p, d, q))]
        return pdq, seasonal_pdq

    def findSmallestAIC(self, y, hostname, service, column):

        pdq, seasonal_pdq = self.createPDQs()

        warnings.filterwarnings("ignore")

        min_aic = 0

        order = (1, 1, 1)
        seasonal_order = (1, 1, 1, 12)
        classifier = None

        i = j = 0

        flag = 0

        while classifier is None:
            for param in pdq:
                i = i + 1
                for param_seasonal in seasonal_pdq:
                    try:
                        print('ARIMA{}x{}'.format(param, param_seasonal))

                        mod = sm.tsa.statespace.SARIMAX(y,
                                                        order=param,
                                                        seasonal_order=param_seasonal,
                                                        enforce_stationarity=False,
                                                        enforce_invertibility=False)

                        results = mod.fit(disp=False)
                        print(results.summary().tables[1])

                        results.plot_diagnostics(figsize=(15, 12))
                        plt.savefig("./graphs/diagnostics_" + str(i) + "itr" + str(j) + ".png")

                        j = j + 1

                        ttab = results.summary()

                        with open("./graphs/table_" +
                                  str(param).replace("(", "").replace(")", "") +
                                  "_" +
                                  str(param_seasonal).replace("(", "").replace(")", "") +
                                  ".txt", "w") as f:
                            f.write(str(ttab))

                        print(' - AIC:{}'.format(results.aic))

                        if flag == 0:
                            results.plot_diagnostics(figsize=(15, 12))
                            plt.savefig("./graphs/diagnostics_" + hostname + "!" + service + "!" + column.replace("/", "") + "_" + str(results.aic) + ".png")
                            order = param
                            seasonal_order = param_seasonal
                            classifier = results
                            min_aic = results.aic
                            flag = 1
                        if results.aic < min_aic:
                            results.plot_diagnostics(figsize=(15, 12))
                            plt.savefig("./graphs/diagnostics_" + hostname + "!" + service + "!" + column.replace("/", "") + "_" + str(results.aic) + ".png")
                            order = param
                            seasonal_order = param_seasonal
                            classifier = results
                            min_aic = results.aic
                    except (ValueError, numpy.linalg.linalg.LinAlgError):
                        print("FAILED!")
                        print(i, " ", j)
                        print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))
                        continue
            if classifier is None:
                print("FAILED TO GENERATE CLASSIFIER")
        return order, seasonal_order, classifier

    def saveClassifier(self, mod, hostname, service, column, seasonal_order, order, time_created, newone=False):
        classifier_path = "./sarimaxes/" + hostname + service + column.replace("/", "") + ".p"
        mod.save(classifier_path, remove_data=True)

        if newone is True:
            savetime = str(time.time())
        else:
            savetime = time_created

        new_rec = Classifier(time_created=savetime,
                             time_retrained=time.time(),
                             hostname=hostname,
                             service=service,
                             column=column,
                             classifier_path=classifier_path,
                             seasonal_pdq=str(seasonal_order),
                             pdq=str(order),
                             )
        new_rec.save()

    def trainArima(self, y, hostname, service, column, seasonal_pdq, pdq, time_created):
        mod = sm.tsa.statespace.SARIMAX(y[column],
                                        order=pdq,
                                        seasonal_order=seasonal_pdq,
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)
        results = mod.fit(disp=False)
        self.saveClassifier(results, hostname, service, column, seasonal_pdq, pdq, time_created)
        return results

    def trainClassifier(self, y, hostname, service, column, time_created):
        order, seasonal_order, classifier = self.findSmallestAIC(y[column], hostname, service, column)
        self.saveClassifier(classifier, hostname, service, column, seasonal_order, order, time_created, True)
        return classifier

    def getClassifier(self, y, hostname, service, db):
        self.loadConf()

        classifiers = (Classifier
                       .select()
                       .where(
                             (Classifier.hostname == hostname) &
                             (Classifier.service == service)
                       ).order_by(Classifier.time_created)
                       )

        if len(classifiers) == 0:
            print "There is none classifier train for data, training!"
            retcls = []
            for column in y.columns:
                retcls.append(self.trainClassifier(y, hostname, service, column, time.time()))
            return retcls
        else:
            print "There are some classifiers found!"
            retcls = []
            colsdone = []
            for cls in classifiers:
                if time.time() > (cls.time_created + 60 * 60 * self.config['full_retrain_time']):
                    print ("Classifiers is older than full_retrain_time, new one is going to be build with new pdq!")
                    (Classifier
                     .delete()
                     .where(
                           (Classifier.hostname == hostname) &
                           (Classifier.service == service) &
                           (Classifier.time_created == cls.time_created)
                     ).execute()
                     )
                    # for column in y.columns:
                    trained = self.trainClassifier(y, cls.hostname, cls.service, cls.column, cls.time_created)
                    retcls.append(trained)
                    # return retcls
                    colsdone.append(cls.column)
                elif time.time() > (cls.time_retrained + 60 * 60 * self.config['retrain_time']):
                    print ("Classifiers for is older than retrain_time, new one is going to be build!")
                    (Classifier
                     .delete()
                     .where(
                           (Classifier.hostname == hostname) &
                           (Classifier.service == service) &
                           (Classifier.time_retrained == cls.time_retrained)
                     ).execute()
                     )
                    trained = self.trainArima(y,
                                              cls.hostname,
                                              cls.service,
                                              cls.column,
                                              ast.literal_eval(cls.seasonal_pdq),
                                              ast.literal_eval(cls.pdq),
                                              cls.time_created)
                    retcls.append(trained)
                    # return retcls
                    colsdone.append(cls.column)
                else:
                    colsdone.append(cls.column)
                    trained = statsmodels.tsa.statespace.sarimax.SARIMAXResults.load(cls.classifier_path)
                    retcls.append(trained)
            for column in y.columns:
                if column not in colsdone:
                    retcls.append(self.trainClassifier(y, hostname, service, column, time.time()))
            return retcls

        # for i in records:
