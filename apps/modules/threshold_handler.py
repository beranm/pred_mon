from peewee import *
from .Database import *
import time


def get_latest(credentials, hostname, service, threshold_type):
    """Get the youngest threshold."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    """
    >>> (Tweet
    ...  .select()
    ...  .join(User)
    ...  .where(User.username == 'charlie')
    ...  .order_by(Tweet.created_date.desc())
    ...  .get())
    """
    try:
        line = (
            Threshold
            .select()
            .where(
                (Threshold.hostname == hostname) &
                (Threshold.service == service) &
                (Threshold.threshold_type == threshold_type)
            ).order_by(
                Threshold.created.desc()
            ).get()
        )
    except DoesNotExist:
        db.close()
        return None
    db.close()
    return line


def insert_new(
    credentials,
    hostname, service,
    new_threshold, threshold_type, attr_thr="none"
):
    """Insert new threshold, give it the latest timestamp."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    new_rec = Threshold(
        service=service,
        hostname=hostname,
        threshold=new_threshold,
        created=int(time.time()),
        threshold_type=threshold_type,
        thr_attributes=attr_thr
    )
    new_rec.save()
    db.close()
