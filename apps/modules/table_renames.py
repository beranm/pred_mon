

def rename_alph_num(some_string):
    return ''.join(
        ch for ch in some_string if ch.isalnum()
    )
