from .Database import *
from peewee import *
import time


class relation_database:

    def __init__(self, credentials):
        self.db = PostgresqlDatabase(
            credentials['name'],
            user=credentials['user'],
            password=credentials['password'],
            host=credentials['address'])
        database_proxy.initialize(self.db)
        self.db.connect()
        self.hostnames = []

    def get_hostnames(self):
        self.hostnames = (
            Hostname
            .select(Hostname.id, Hostname.hostname)
            .desc()
        )
        return [i.hostname for i in self.hostnames]

    def lookup_hostname_id(self, hostname):
        id_of_hostname = -1
        for i in self.hostnames:
            if i.hostname == hostname:
                id_of_hostname = i.id
                break
        if id_of_hostname == -1:
            self.get_hostnames()
            for i in self.hostnames:
                if i.hostname == hostname:
                    id_of_hostname = i.id
                    break
        return id_of_hostname

    def get_services_by_hostname(self, hostname):
        id_of_hostname = self.lookup_hostname_id(hostname)
        return [i.service for i in (
            Service
            .select(Service.service)
            .where(Service.id_hostname == id_of_hostname)
        )]

    def insert_hostname(self, hostname):
        if hostname not in [i.hostname for i in self.hostnames] \
                and self.lookup_hostname_id(hostname) == -1:
            new_rec = Hostname(
                hostname=hostname
            )
            new_rec.save()
            self.get_hostnames()

    def insert_service(self, hostname, service):
        hostname_id = self.lookup_hostname_id(hostname)
        if hostname_id == -1:
            self.insert_hostname(hostname)
            hostname_id = self.lookup_hostname_id(hostname)
        new_rec = Service(
            id_hostname=hostname_id,
            service=service
        )
        new_rec.save()

    def get_youngest_threshold(self, hostname, service, threshold_type):
        """Get the youngest threshold."""
        try:
            line = (
                Threshold
                .select()
                .where(
                    (Threshold.hostname == hostname) &
                    (Threshold.service == service) &
                    (Threshold.threshold_type == threshold_type)
                ).order_by(
                    Threshold.created.desc()
                ).get()
            )
        except DoesNotExist:
            return None
        return line

    def insert_new_threshold(
        self,
        hostname, service,
        new_threshold, threshold_type, attr_thr="none"
    ):
        """Insert new threshold, give it the latest timestamp."""
        new_rec = Threshold(
            service=service,
            hostname=hostname,
            threshold=new_threshold,
            created=int(time.time()),
            threshold_type=threshold_type,
            thr_attributes=attr_thr
        )
        new_rec.save()

    def __del__(self):
        self.db.close()
