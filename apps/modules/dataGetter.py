from peewee import *
from .Database import *
from .DataParser import DataParser
from .PandaParser import PandaParser
import pandas as pd
from .FileRenames import normalizeName
from sklearn.externals import joblib
import time
import datetime
from sqlalchemy import create_engine
# from sqlalchemy import text as adjust_text
import os
from .PandaParser import convert_fill as convert_fill
from .table_renames import rename_alph_num
# import re


def getYoungesLine(credentials):
    """Get the youngest line timestamp."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    line = (
        Record
        .select(
            Record.time_execution
        ).group_by(
            Record.time_execution
        ).desc()
    )
    db.close()
    return line.time_execution


def getHostnames(credentials):
    """Get list of hostnames."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    hostnames = (
        Record
        .select(Record.hostname).group_by(Record.hostname)
        .desc()
    )
    db.close()
    return [record.hostname for record in hostnames]


def get_services_by_hostname(hostname, credentials):
    """Get list of hostnames."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])

    database_proxy.initialize(db)
    db.connect()
    services = (
        Record
        .select(Record.service).group_by(Record.service)
        .where(
            (Record.hostname == hostname)
        )
        .desc()
    )
    db.close()
    return [record.service for record in services]


def getClassifier(hostname, service, column, credentials, type='Predict'):
    """Get classifier from DB."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    try:
        clsf = (
            Classifier
            .select()
            .where(
                (Classifier.hostname == hostname) &
                (Classifier.service == service) &
                (Classifier.column == service) &
                (Classifier.type == type)
            ).limit(1)
            .get()
        )
    # except Exception as exception:
    except DoesNotExist:
        db.close()
        return None
    db.close()
    if not os.path.isfile(clsf.classifier_path):
        return None
    if type == 'Predict':
        from keras.models import load_model
        from keras import backend as K
        ret_model = load_model(clsf.classifier_path)
        K.clear_session()
        return ret_model
    elif type == 'Classify':
        return joblib.load(clsf.classifier_path)


def saveClassifier(model, hostname, service, credentials, type='Predict'):
    """Save classifier to DB and h5 file."""
    if type == 'Predict':
        classifier_path = \
            'models/' + normalizeName(hostname + "!" + service) + '.h5'
        model.save(classifier_path)
    elif type == 'Classify':
        classifier_path = \
            'models/' + normalizeName(hostname + "!" + service) + '.pkl'
        joblib.dump(model, classifier_path)

    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'], host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    newRec = Classifier(
        service=service,
        hostname=hostname,
        column=service,
        classifier_path=classifier_path,
        type=type
    )
    newRec.save()
    db.close()


def get_predict_notice(hostname, service, time_notice, credentials):
    """Get fails prediction which are between time_notice and valid_until.

    Returns:
        time of fail if there is some state which is bad or 0 otherwise

    """
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    records = (
        Predicted
        .select()
        .where(
            (Predicted.hostname == hostname) &
            (Predicted.service == service) &
            (Predicted.valid_until > time_notice)
            # (Predicted.valid_until >= time_notice)
        )  # .order_by(Record.time_execution.desc())
    )
    # s = tuple("\'" + str(i) + "\'" for i in records.sql()[1])
    # record_query = records.sql()[0] % s
    # print(record_query)
    for record in records:
        if record.state == 1:
            return record.time_noticed
    db.close()
    return 0


def get_predict_interval(hostname, service, credentials):
    """Get latest fail prediction intervals of time_notice and valid_until.

    Returns:
        two times which create interval for which the prediction is valid
    """
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    records = (
        Predicted
        .select()
        .where(
            (Predicted.hostname == hostname) &
            (Predicted.service == service)
        ).order_by(Predicted.time_noticed.desc()).limit(1)
    )
    db.close()

    for record in records:
        return (record.time_noticed, record.valid_until)
    return (None, None)


def getServByHst(credentials, hostname):
    """Get list of services by hostname."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])

    database_proxy.initialize(db)
    db.connect()
    services = (
        Record
        .select(Record.service)
        .where(
            (Record.hostname == hostname)
        ).group_by(Record.service)
        .desc()
    )
    db.close()
    return [record.service for record in services]


def getServices(credentials):
    """Get list of services."""
    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'],
        host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    services = (
        Record
        .select(Record.service).group_by(Record.service)
        .desc()
    )
    db.close()
    return services


def getHostServ(credentials):
    """Get hostname and services as group list of database."""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    services = (
        Record
        .select(
            Record.hostname, Record.service
        ).group_by(
            Record.hostname, Record.service)
        .desc()
    )
    db.close()
    return services


def getDataLimit(hostname, service, credentials, limit):
    """Get dataframe with latest data by count given by limit."""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    records = (Record
               .select()
               .where(
                     (Record.hostname == hostname) &
                     (Record.service == service)
               ).order_by(Record.time_execution.desc())
               .limit(limit)
               )
    parser = DataParser()
    pprser = PandaParser()
    sjoin = []
    oldest_time = 0
    for i in records:
        if oldest_time == 0:
            oldest_time = i.time_execution
        i.result = parser.parseResult(service, i.result)
        tmp = pprser.generateTmp(i.result,
                                 i.time_execution,
                                 service)

        sjoin.append(tmp)
    if len(sjoin) == 0:
        db.close()
        raise ValueError('empty list')
    recpd = pd.concat(sjoin)
    db.close()
    return recpd


def getDataLimitFromDate(hostname, service, credentials, limit, givenTime):
    """Get dataframe with latest data by count given by limit."""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()

    # 2017-08-10 19:56:09
    daStamp = time.mktime(
        datetime.datetime.strptime(
            givenTime,
            "%Y-%m-%d %H:%M:%S"
        ).timetuple())
    records = (Record
               .select()
               .where(
                     (Record.hostname == hostname) &
                     (Record.service == service) &
                     (Record.time_execution <= daStamp)
               ).order_by(Record.time_execution.desc())
               .limit(limit)
               )
    parser = DataParser()
    pprser = PandaParser()
    sjoin = []
    oldest_time = 0
    for i in records:
        if oldest_time == 0:
            oldest_time = i.time_execution
        i.result = parser.parseResult(service, i.result)
        tmp = pprser.generateTmp(i.result,
                                 i.time_execution,
                                 service)
        tmp['states'] = [i.state]
        sjoin.append(tmp)
    if len(sjoin) == 0:
        db.close()
        raise ValueError('empty list')
    recpd = pd.concat(sjoin)
    db.close()
    return recpd


def get_data_count(hostname, service, credentials):
    """Get count of data by hostname and service."""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()

    records_count = (
        Record
        .select()
        .where(
            (Record.hostname == hostname) &
            (Record.service == service)
        ).count()
    )
    db.close()

    return records_count



def getDataFromDate(
        hostname,
        service,
        credentials,
        givenTime):
    """Get dataframe with data bigger than giveTime"""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()

    records = (
        Record
        .select(
            Record.result,
            Record.state,
            SQL('TO_CHAR(to_timestamp(time_execution), \'YYYY-MM-DD HH24:MI:SS\') AS index'))
        .where(
            (Record.hostname == hostname) &
            (Record.service == service.replace("'", '\'')) &
            # what is directly behind timestamp given in givenTime
            (Record.time_execution >= givenTime)
        ).order_by(Record.time_execution.desc())
    )

    # print(records.sql())
    # s = records.sql()[0] % tuple(records.sql()[1])
    # print(s)
    s = tuple(
        "'" +
        str(i)
        .replace("'", "\'") +
        "'" for i in records.sql()[1])
    s = records.sql()[0] % s
    engine = create_engine(
        "postgresql+psycopg2://" +
        credentials['user'] + ":" +
        credentials['password'] + "@" +
        credentials['address'] + "/" +
        credentials['name'])

    # loaded_frame = pd.read_sql(adjust_text(record_query), engine)
    try:
        loaded_frame = pd.read_sql(s, engine)
    except:
        raise ValueError('empty list')
    # renaming index column
    loaded_frame.index = loaded_frame['index'].values
    # deleting unusable index column
    del loaded_frame["index"]
    # rename state column to states
    columns = loaded_frame.columns.tolist()
    index_column = loaded_frame.columns.tolist().index('state')
    columns[index_column] = 'states'
    loaded_frame.columns = columns

    # renaming result column
    columns = loaded_frame.columns.tolist()
    result_column = loaded_frame.columns.tolist().index('result')
    columns[result_column] = service
    loaded_frame.columns = columns

    db.close()
    if loaded_frame.empty:
        raise ValueError('empty list')
    loaded_frame = loaded_frame.drop_duplicates()
    return convert_fill(loaded_frame)



def get_data_from_date_with_relevancy(
        hostname,
        service,
        credentials,
        givenTime,
        upperTime=time.time(),
        interval=5 * 60):
    """Get dataframe with data bigger than giveTime"""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()


    test_select_count = (
        Record
        .select(
            Record.result,
            Record.state)
        .where(
            (Record.hostname == hostname) &
            (Record.service == service) &
            (Record.time_execution >= (upperTime - interval))
        ).order_by(Record.time_execution.desc())
    ).wrapped_count()

    if (test_select_count == 0):
        db.close()
        raise ValueError('Data not current')

    records = (
        Record
        .select(
            Record.result,
            Record.state,
            SQL('TO_CHAR(to_timestamp(time_execution), \'YYYY-MM-DD HH24:MI:SS\') AS index'))
        .where(
            (Record.hostname == hostname) &
            (Record.service == service) &
            # what is directly behind timestamp given in givenTime
            (Record.time_execution > givenTime)
        ).order_by(Record.time_execution.desc())
    )
    # print(records.sql())
    s = tuple(
        "'" +
        str(i)
        .replace("'", "\'") +
        "'" for i in records.sql()[1])

    s = records.sql()[0] % s
    # print(s)
    # s = tuple("\'" + str(i) + "\'" for i in records.sql()[1])
    # record_query = records.sql()[0] % s
    engine = create_engine(
        "postgresql+psycopg2://" +
        credentials['user'] + ":" +
        credentials['password'] + "@" +
        credentials['address'] + "/" +
        credentials['name'])
    try:
        loaded_frame = pd.read_sql(s, engine)
    except:
        raise ValueError('empty list')
    # renaming index column
    loaded_frame.index = loaded_frame['index'].values
    # deleting unusable index column
    del loaded_frame["index"]
    # rename state column to states
    columns = loaded_frame.columns.tolist()
    index_column = loaded_frame.columns.tolist().index('state')
    columns[index_column] = 'states'
    loaded_frame.columns = columns

    # renaming result column
    columns = loaded_frame.columns.tolist()
    result_column = loaded_frame.columns.tolist().index('result')
    columns[result_column] = service
    loaded_frame.columns = columns

    db.close()
    if loaded_frame.empty:
        raise ValueError('empty list')
    loaded_frame = loaded_frame.drop_duplicates()
    return convert_fill(loaded_frame)


def getDataFrame(hostname, service, credentials, delay=(60 * 60 * 24 * 7)):
    """Get dataframe from last known date to week old data."""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    parser = DataParser()
    pprser = PandaParser()
    last_date = (
        Record
        .select()
        .where(
            (Record.hostname == hostname) &
            (Record.service == service)
        ).order_by(Record.time_execution)
        .desc()
        .limit(1)
        .get())

    records = (Record
               .select()
               .where(
                     (Record.hostname == hostname) &
                     (Record.service == service) &
                     (Record.time_execution > last_date)
               ).order_by(Record.time_execution)
               )
    sjoin = []
    oldest_time = 0
    for i in records:
        if oldest_time == 0:
            oldest_time = i.time_execution
        # print (i.time_execution)
        i.result = parser.parseResult(service, i.result)
        tmp = pprser.generateTmp(i.result,
                                 i.time_execution,
                                 service)

        sjoin.append(tmp)
        # print(recpd.head())
        # print i.result
    if len(sjoin) == 0:
        db.close()
        raise ValueError('empty list')
    recpd = pd.concat(sjoin)
    db.close()
    return recpd


def crtSupData(hostname, service, credentials, delay=(60 * 60 * 24 * 7)):
    """Get data dated week old data in form of X['results'], Y['states']."""
    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'],
                            host=credentials['address'])
    database_proxy.initialize(db)
    db.connect()
    parser = DataParser()
    pprser = PandaParser()
    last_date = (
        Record
        .select()
        .where(
            (Record.hostname == hostname) &
            (Record.service == service)
        ).order_by(Record.time_execution)
        .desc()
        .limit(1)
        .get())

    records = (Record
               .select()
               .where(
                     (Record.hostname == hostname) &
                     (Record.service == service) &
                     (Record.time_execution > last_date)
               ).order_by(Record.time_execution)
               )
    X = []
    Y = []

    for i in records:
        X.append([parser.parseResult(service, i.result)])
        Y.append(i.state)

    db.close()

    return X, Y
