import pandas as pd
from influxdb import DataFrameClient
import datetime
import time
import hashlib


class influx_connector:

    def __init__(self, credentials):
        self.client = DataFrameClient(
            credentials['address'],
            credentials['port'],
            credentials['user'],
            credentials['password'],
            credentials['dbname'])
        self.dbname = credentials['dbname']
        self.client.create_database(
            self.dbname)

    def get_db_name(self, hostname, service):
        m = hashlib.md5()
        m.update((hostname + service).encode())
        return m.hexdigest()

    def get_time(self, time_from):
        return datetime.datetime.fromtimestamp(
            int(time_from)
        ).strftime('%Y-%m-%d %H:%M:%S')

    def get_data_from_time(
        self, hostname, service, time_from
    ):
        dbname = self.get_db_name(hostname, service)
        req_query = \
            "SELECT * FROM \"" + dbname + "\"" + \
            " WHERE time >= \'" + self.get_time(time_from) + "\';"
        # print(req_query)
        results = self.client.query(
            req_query
        )
        if not results:
            raise ValueError('empty list')
        return pd.DataFrame(
            self.client.query(
                req_query
            )[dbname]
        )

    def insert_frame(
        self,
        hostname, service,
        input_data_frame
    ):
        dbname = self.get_db_name(hostname, service)
        self.client.write_points(
            input_data_frame,
            dbname,
            protocol='json')

    def insert_data(
        self,
        hostname, service, notice_time, state, result, delay
    ):
        dbname = self.get_db_name(hostname, service)
        df = pd.DataFrame(
            {
                "state": [int(state)],
                "result": [float(result)],
                "delay": [int(delay)]
            }, index=[pd.to_datetime(
                notice_time, format='%Y-%m-%dT%H:%M:%SZ', errors='coerce')])
        self.client.write_points(
            df,
            dbname,
            protocol='json')

    def get_data_frame(self, hostname, service, delay=(60 * 60 * 24 * 7)):
        """Get dataframe from last known date to week old data."""
        return self.get_data_from_time(
            hostname, service,
            int(time.time() - delay)
        )

    def get_data_limit(self, hostname, service, limit):
        dbname = self.get_db_name(hostname, service)
        return pd.DataFrame(
            self.client.query(
                "SELECT * FROM \"" + dbname + "\"" +
                " ORDER BY time DESC LIMIT " + str(limit) + ";"
            )[dbname]
        )

    def get_data_to_classify(
            self,
            hostname, service, delay=(60 * 60 * 24 * 7)):
        pdata = self.get_data_from_time(
            hostname, service,
            int(time.time() - delay)
        )
        X = [[i] for i in pdata['result'].values]
        Y = pdata['state'].values
        return X, Y

    def get_data_limit_from_date(
            self,
            hostname, service, limit, given_time):
        dbname = self.get_db_name(hostname, service)
        daStamp = time.mktime(
            datetime.datetime.strptime(
                given_time.split('+')[0],
                "%Y-%m-%d %H:%M:%S"
            ).timetuple())
        req_query = \
            "SELECT * FROM \"" + dbname + "\"" + \
            " WHERE time <= \'" + self.get_time(daStamp) + "\'" + \
            "  ORDER BY time DESC LIMIT " + str(limit) + ";"
        results = self.client.query(
            req_query
        )
        if not results:
            raise ValueError('empty list')
        return pd.DataFrame(
            results[dbname]
        )

    def get_data_count(self, hostname, service):
        dbname = self.get_db_name(hostname, service)
        req_query = \
            "SELECT COUNT(*) FROM \"" + dbname + "\";"
        try:
            ret_val = int(
                self.client.query(
                    req_query
                )[dbname]['count'][0]
            )
        except KeyError:
            return 0
        return ret_val
