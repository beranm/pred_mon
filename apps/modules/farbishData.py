"""
Module for farbishing data.

Helping module with functions for editing data in non other way
than adding non important features.
"""

import pandas as pd


def prepareData(
    # consider refactoring since this doesn't get any data,
    #  it just create dataframe from known data.
        inputData, states, startTime, delay):
    """Generate dataframe from future data."""
    inp = {
        'data': inputData,
        'state': states
        }
    # print inp
    idx = pd.date_range(startTime, periods=len(inputData), freq=str(delay) + 's')
    # print idx
    return pd.DataFrame(inp, index=idx)
