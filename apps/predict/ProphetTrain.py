import pandas as pd
import numpy as np
from fbprophet import Prophet
import pandas as pd


"""
Consider refactoring to the class where model is goint to be atribute of class.
Also input data have somewhat only object meaning. And cheereous since it's
  weekend and it's time to drop some easter eggs.
"""

def get_model():
    return Prophet()


def prepare_df(data):
    new_df = pd.DataFrame(
        {
            'y': data[data.columns[0]].values,
            'ds': data.index.values
        }
    )
    new_df.index = range(0, len(data[data.columns[0]].values))
    return new_df


def train_model(model, frame):
    return model.fit(frame)


def post_df(forecast):
    new_frame = pd.DataFrame({'data': forecast['yhat'].values})
    new_frame.index = forecast['ds'].values
    return new_frame


def predict(model, look_ahead, freq):
    future = model.make_future_dataframe(
        periods=look_ahead,
        freq=str(freq) + 's')[0:look_ahead]
    # print (look_ahead)
    forecast = model.predict(future)
    # forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()
    return post_df(forecast)
