"""LSTM Network prediction."""
import yaml
import sys
from optparse import OptionParser
import pika
import json
from modules import dataGetter
# import predict.LstmTrain as LstmTrain
# import predict.ProphetTrain as ProphetTrain
import LstmTrain as LstmTrain
import ProphetTrain as ProphetTrain
# import pprint
# import gc
# from memory_profiler import profile
from keras import backend as K
# import matplotlib.pyplot as plt
from multiprocessing import Process, Lock, Queue
import time
from modules.influx_connector import influx_connector as infc


credentials = dict()
influx_credentials = dict()
stdyml = ""
look_back = 10
channel = None
predict_alg = ""  # which algortihm should be used for predictions
process_count = 1
queue = Queue()


def validate_data(message):
    global credentials, influx_credentials, stdyml, channel
    _influx = infc(influx_credentials)
    lock = Lock()
    lock.acquire()
    with open(stdyml, 'r') as f:
        try:
            valids = yaml.load(f)
        except (
            yaml.parser.ParserError,
            yaml.scanner.ScannerError,
            yaml.reader.ReaderError
        ):
            open(stdyml, 'w').close()
            return False

    if valids is None:
        valids = {}

    if message['hostname'] in valids.keys():
        if message['service'] in valids[message['hostname']].keys():
            if type(valids[message['hostname']][message['service']]) == str:
                stdCheck = None
            elif message['service'] in valids[message['hostname']][message['service']].keys():
                stdCheck = valids[
                    message['hostname']][
                        message['service']][
                            message['service']]
            else:
                stdCheck = None
        else:
            stdCheck = None
    else:
        stdCheck = None
    if stdCheck is None:
        # data = dataGetter.getDataFrame(
        #     message['hostname'], message['service'], credentials)
        try:
            data = _influx.get_data_frame(
                message['hostname'], message['service'])
        except ValueError:
            return False
        stdCheck = False if data['result'].std() == 0 else True
        if message['hostname'] not in valids.keys():
            valids[message['hostname']] = dict()
        if message['service'] not in valids[message['hostname']]:
            valids[message['hostname']][message['service']] = dict()
        if message['service'] not in valids[message['hostname']][message['service']] is None:
            valids[message['hostname']][message['service']][message['service']] = dict()

        valids[
            message['hostname']][
                message['service']][
                    message['service']] = stdCheck
        saveValids(valids, stdyml)
    lock.release()
    return stdCheck


def getLSTMPrediction(message):
        data = None
        clsf = dataGetter.getClassifier(
            message['hostname'],
            message['service'],
            message['service'], credentials)
        _influx = infc(influx_credentials)
        if clsf is None:
            # need to create classifier
            if data is None:
                # data = dataGetter.getDataFrame(
                #     message['hostname'], message['service'], credentials)
                data = _influx.get_data_frame(
                    message['hostname'], message['service'])
            if len(data) < look_back:
                print("Too little data, ending...")
                return
            dataX, dataY = LstmTrain.preproData(data.values, look_back)
            model = LstmTrain.create_stacked_lstm(
                dataX, dataY, look_back)

            dataGetter.saveClassifier(
                model, message['hostname'], message['service'], credentials)
            print(model.summary())
        else:
            # classifier already given, gonna use that and give new data to it
            model = clsf
            data = _influx.get_data_limit(
                message['hostname'],
                message['service'],
                4 * look_back)
            # data = dataGetter.getDataLimit(
            #     message['hostname'],
            #     message['service'],
            #     credentials, 4 * look_back)
            dataX, dataY = LstmTrain.preproData(data.values, look_back)
            print("Adaptively retraining!")
            model = LstmTrain.adaptiveTrain(model, dataX, dataY, look_back)
            dataGetter.saveClassifier(
                model, message['hostname'], message['service'], credentials)
            print(model.summary())
        # creating message to queue
        outputData = LstmTrain.getPrediction(
            model, dataX, dataY, look_back, batch_size=1)
        outputData = outputData.reshape((len(outputData))).tolist()
        return outputData, data.index.max()


def getProphetPrediction(message):
        global freq
        data = None
        _influx = infc(influx_credentials)
        if data is None:
            try:
                # data = dataGetter.getDataLimit(
                #     message['hostname'], message['service'], credentials,
                #     look_back * 2 * 60).sort_index()
                data = _influx.get_data_limit(
                    message['hostname'],
                    message['service'],
                    look_back * 2 * 60)
                print(data.head())
                print(data.tail())
                # consider rethink on this, since this is
                # just for hour of samples
                # (tested with 30sec delay on collection)
            except ValueError:
                print("Fail ")
        if len(data) < look_back:
            print("Too little data, ending...")
            return

        last_val = data.tail(n=1).values[0][0]
        print (last_val)
        model = ProphetTrain.get_model()
        prepared_data = ProphetTrain.prepare_df(data)
        model = ProphetTrain.train_model(model, prepared_data)
        # print ("===============================================")
        # print (data)
        # outputData = ProphetTrain.predict(model, look_back, freq)
        # print (outputData)
        outputData = ProphetTrain.predict(
            model, look_back, freq
        ).values
        outputData = [i[0] + last_val for i in outputData]
        print ("===============================================")
        print (data.head())
        print (data.tail())
        # creating message to queue
        return outputData, data.index.max()


def saveValids(valids, stdyml):
    """Save valids resources to YML file."""
    with open(stdyml, "w") as f:
        yaml.safe_dump(
            valids, f,
            default_flow_style=False,
            encoding='utf-8', allow_unicode=True)


def create_prediction(q, rabbit_address):
    """Prediction threat to compute fucure in more concurency level."""
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=rabbit_address
        ))
    channel = connection.channel()
    channel.queue_declare(
        queue='classify',
        durable=True)
    while 1:
        body = q.get()
        message = json.loads(body)
        stdCheck = validate_data(message)
        if stdCheck is False:
            # STD zero, no need to predict
            print("Leaving since STD is zero!")
            print(message['hostname'] + " " + message['service'])
        elif stdCheck is True:
            # STD not zero, gonna predict
            print("Going to create estimates!")
            print(message['hostname'] + " " + message['service'])
            if predict_alg == "lstm":
                try:
                    outputData, time_onward = getLSTMPrediction(message)
                except:
                    print("Fail on prediction, maybe next time")
                    return
            elif predict_alg == "Prophet":
                # outputData, time_onward = getProphetPrediction(message)
                try:
                    outputData, time_onward = getProphetPrediction(message)
                except:
                    print("Fail on prediction, maybe next time")
                    return
            print("XXXXXXXXXXXXXXXXX " + str(time_onward) + " XXXXXXXXXXXXXXXXX")
            toqueue = {
                'hostname': message['hostname'],
                'service': message['service'],
                'time_forward': str(time_onward),
                'data': outputData}
            print(toqueue)
            try:
                channel.basic_publish(
                    exchange='',
                    routing_key='classify',
                    body=json.dumps(toqueue))
            except pika.exceptions.ConnectionClosed:
                print("Failed to push message to queue! Better luck next time.")
            print("Cleaning session...")
            K.clear_session()


def callback(ch, method, properties, body):
    """Wait on comming message."""
    """
    TODO: add code for ICINGA!
    - load related model if data are marked as usable
      - if none model was generated, create one
    - give it new data and ask for prediction
    - send predicted data to classify pipe
    """
    global predict_items_delay, process_count, queue, \
        credentials, influx_credentials, stdyml, channel, predict_alg, freq
    """
    - test if there is enough data
      - if so test STD and append marking to YML file
    """
    queue.put(body)
    # create_prediction(channel, message)
    # print data.std(skipna=True)
    # print 0 if data.std(skipna=True)[0] == 0 else 1


def process_check(queue, process_list, process_count, rabbit_address):
    while 1:
        for p in process_list:
            if not p.is_alive():
                process_list.remove(p)

        for i in range(len(process_list) - 1, process_count):
            procs = Process(
                target=create_prediction,
                args=(queue, rabbit_address, ))
            procs.start()
            process_list.append(procs)
        time.sleep(10)


def main():
    """Main function."""
    global credentials, influx_credentials, process_count, queue, \
        stdyml, channel, predict_alg, freq
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--rabbit",
        action="store",
        default="./credentials/rabbit.yml",
        dest="rabbit",
        type="string",
        help="Transfer data to rabbitmq")
    parser.add_option(
        "--influx",
        action="store",
        default="./credentials/influxdb.yml",
        dest="influx",
        type="string",
        help="Influx database credentials")
    parser.add_option(
        "--process",
        action="store",
        default="1",
        dest="process",
        type="string",
        help="Number of process to count predictions")
    parser.add_option(
        "--valids",
        action="store",
        default="validate/std_valid.yml",
        dest="valids",
        type="string",
        help="Where is YML of precomputed STD")
    parser.add_option(
        "--config",
        action="store",
        default="configuration/basics.yml",
        dest="basics",
        type="string",
        help="Basic configuration path")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()
    with open(options.database, 'r') as f:
        credentials = yaml.load(f)

    with open(options.basics, 'r') as f:
        basic_conf = yaml.load(f)
        predict_alg = basic_conf["predict_alg"]
        freq = basic_conf["collect_delay"]
    with open(options.rabbit, 'r') as f:
        rabbit_credentials = yaml.load(f)
    rabbit_credentials = rabbit_credentials['rabbit']

    with open(options.influx, 'r') as f:
        influx_credentials = yaml.load(f)
    influx_credentials = influx_credentials['influxdb']

    # process_count = int(options.process)
    process_count = 1

    credentials = credentials['database']
    stdyml = options.valids

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=rabbit_credentials['address']
        ))
    channel = connection.channel()
    channel.queue_declare(
        queue='predict',
        durable=True)
    channel.basic_consume(
        callback,
        queue='predict',
        no_ack=True)

    process_list = []

    Process(
        target=process_check,
        args=(
            queue,
            process_list,
            process_count,
            rabbit_credentials['address'], )
    ).start()

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == "__main__":
    main()
