CREATE OR REPLACE FUNCTION watch(INTEGER, TEXT) RETURNS VOID AS $$
DECLARE
    delay ALIAS FOR $1;
    request ALIAS FOR $2;
    result TEXT;
BEGIN
    LOOP
	PERFORM '\! cls';
	RAISE INFO '======================================';
	EXECUTE request INTO result;
	RAISE INFO '%', result;
	PERFORM pg_sleep(delay);
    END LOOP;
END;
$$
language plpgsql;
