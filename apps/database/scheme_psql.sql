DROP DATABASE IF EXISTS "predmon";
DROP USER IF EXISTS "predmon";
CREATE ROLE "predmon" WITH PASSWORD 'predmon' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;
CREATE DATABASE "predmon" ENCODING 'utf8';
GRANT ALL PRIVILEGES ON DATABASE "predmon" to "predmon";

\c  "predmon";

DROP TABLE IF EXISTS "record" ;

CREATE TABLE IF NOT EXISTS "record" (
  "time_execution" BIGINT  NOT NULL,
  "service" VARCHAR(120) NOT NULL,
  "result" TEXT NULL,
  "hostname" VARCHAR(120) NOT NULL,
  "state" DOUBLE PRECISION NULL,
  "id" SERIAL
);
CREATE INDEX ON "record" ("time_execution", "id");

ALTER TABLE "record" OWNER TO "predmon";

DROP TABLE IF EXISTS "classifier" ;

CREATE TABLE IF NOT EXISTS "classifier" (
  "id" SERIAL,
  "hostname" TEXT NULL,
  "service" TEXT NULL,
  "column" TEXT NULL,
  "classifier_path" TEXT NULL,
  "type" TEXT NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "classifier" OWNER TO "predmon";

DROP TABLE IF EXISTS "predicted" ;

CREATE TABLE IF NOT EXISTS "predicted" (
  "id" SERIAL,
  "hostname" TEXT NULL,
  "service" TEXT NULL,
  "state" INT NULL,
  "time_noticed" DOUBLE PRECISION NULL,
  "valid_until" DOUBLE PRECISION NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "predicted" OWNER TO "predmon";

DROP TABLE IF EXISTS "threshold" ;

CREATE TABLE IF NOT EXISTS "threshold" (
  "id" SERIAL,
  "hostname" TEXT NULL,
  "service" TEXT NULL,
  "created" DOUBLE PRECISION NULL,
  "threshold" DOUBLE PRECISION NULL,
  "threshold_type" TEXT NULL,
  "thr_attributes" TEXT NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "threshold" OWNER TO "predmon";

DROP TABLE IF EXISTS "hostname" ;

CREATE TABLE IF NOT EXISTS "hostname" (
  "id" SERIAL,
  "hostname" TEXT NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "hostname" OWNER TO "predmon";

DROP TABLE IF EXISTS "service" ;

CREATE TABLE IF NOT EXISTS "service" (
  "id" SERIAL,
  "id_hostname" INTEGER,
  "service" TEXT NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "service" OWNER TO "predmon";

CREATE TABLE IF NOT EXISTS "cluster" (
  "id" SERIAL,
  "time_execution" BIGINT  NOT NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "service" OWNER TO "predmon";

CREATE TABLE IF NOT EXISTS "cluster_items" (
  "id" SERIAL,
  "hostname" TEXT NULL,
  "service" TEXT NULL,
  "time_execution" BIGINT  NOT NULL,
  PRIMARY KEY ("id"));

ALTER TABLE "service" OWNER TO "predmon";


