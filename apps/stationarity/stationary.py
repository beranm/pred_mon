import warnings
import itertools
import sys
import pandas as pd
import numpy as np
import numpy
import statsmodels.api as sm
import matplotlib.pyplot as plt
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
from peewee import *
from modules.Database import *
import yaml
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from statsmodels.tsa.stattools import adfuller
from numpy import log

def readRecords(hostname, service):
    records = Record.select() \
                    .where(
                        (Record.hostname == hostname) &
                        (Record.service == service)) \
                    .order_by(Record.time_execution)
    return records


def main():
    plt.style.use('fivethirtyeight')

    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--ici",
        action="store",
        default="./services/selected.yml",
        dest="services",
        type="string",
        help="Services to test on stationarity")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)

    credentials = credentials['database']

    db = PostgresqlDatabase(credentials['name'],
                            user=credentials['user'],
                            password=credentials['password'], host=credentials['address'])

    database_proxy.initialize(db)

    db.connect()

    parser = DataParser()
    pprser = PandaParser()

    with open(options.services, 'r') as f:
        docs = yaml.load_all(f)
        for doc in docs:
            for hostname, v in doc.items():
                for service in v['services']:
                    print(service)
                    last_date = (
                        Record
                        .select()
                        .where(
                            (Record.hostname == hostname) &
                            (Record.service == service)
                        ).order_by(Record.time_execution)
                        .desc()
                        .limit(1)
                        .get())
                    last_date = (
                        last_date.time_execution -
                        (60 * 60 * 24 * 7))
                    records = (Record
                               .select()
                               .where(
                                     (Record.hostname == hostname) &
                                     (Record.service == service) &
                                     (Record.time_execution > last_date)
                               ).order_by(Record.time_execution)
                               )
                    sjoin = []
                    oldest_time = 0
                    for i in records:
                        if oldest_time == 0:
                            oldest_time = i.time_execution
                        # print (i.time_execution)
                        i.result = parser.parseResult(service, i.result)

                        tmp = pprser.generateTmp(i.result,
                                                 i.time_execution,
                                                 service)

                        sjoin.append(tmp)
                        # print(recpd.head())
                        # print i.result
                    if len(sjoin) == 0:
                        raise ValueError('empty list')
                    recpd = pd.concat(sjoin)

                    for column in recpd.columns:
                        print recpd[column].head()
                        X = recpd[column].values
                        try:
                            X = log(X)
                            result = adfuller(X)
                            print('ADF Statistic: %f' % result[0])
                            print('p-value: %f' % result[1])
                            for key, value in result[4].items():
                                print('\t%s: %.3f' % (key, value))
                        except numpy.linalg.linalg.LinAlgError:
                            print("Values did not even converged")

    db.close()


if __name__ == "__main__":
    main()
