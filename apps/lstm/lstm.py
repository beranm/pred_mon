"""LSTM Network prediction."""
import sys
import pandas as pd
import numpy
import matplotlib.pyplot as plt
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
# from lstm.lstmTrain import lstmTrain
from modules.Graphy import Graphy
from peewee import *
from modules.Database import *
import yaml
import LstmTrain
from modules.FileRenames import normalizeName
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
import os
import time
import modules.dataGetter as dataGetter
import warnings
import numpy as np
from numpy import newaxis
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler
from math import sqrt
from sklearn.metrics import mean_squared_error


# look_back = 8
look_back = 60
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Hide messy TensorFlow warnings
warnings.filterwarnings("ignore")  # Hide messy Numpy warnings


def main():
    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--services",
        action="store",
        default="./services/selected.yml",
        dest="services",
        type="string",
        help="Services to manage in LSTM Network model")
    parser.add_option(
        "--test",
        action="store_true",
        default=False,
        dest="tests",
        help="Create test on data with training and testing dataset")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()
    with open(options.database, 'r') as f:
        credentials = yaml.load(f)

    credentials = credentials['database']

    db = PostgresqlDatabase(
        credentials['name'],
        user=credentials['user'],
        password=credentials['password'], host=credentials['address'])

    database_proxy.initialize(db)
    parser = DataParser()
    pprser = PandaParser()

    hostnames = dataGetter.getHostnames(credentials)

    with open(options.services, 'r') as f:
        docs = yaml.load_all(f)
        for doc in docs:
            for hostname, v in doc.items():
                for service in v['services']:
                    print(service)
                    recpd = dataGetter.getDataFrame(
                        hostname, service, credentials)
                    # print(recpd.head())
                    recpd.plot(figsize=(15, 6))
                    plt.savefig(
                        "./lstm/graphs/" +
                        normalizeName(
                            "all_" + hostname + "!" + service + ".png"
                            )
                        )
                    plt.close()
                    for column in recpd.columns:
                        inputData = recpd[column]
                        dataset = inputData.values
                        if options.tests:
                            batch_size = 1
                            # rescale as bellow
                            # print scaler.inverse_transform(dataset)
                            # dataset = np.cos(np.arange(1000)*(20*np.pi/1000))[0:200][:, None]
                            dataset = dataset[:, None]
                            scaler = MinMaxScaler(feature_range=(0, 1))
                            dataset = scaler.fit_transform(dataset)
                            train, test = pprser.splitTrainTest(dataset)
                            train_size, test_size = len(train), len(test)
                            trainX, trainY = LstmTrain.create_dataset(
                                train, look_back)
                            testX, testY = LstmTrain.create_dataset(
                                test, look_back)
                            trainX = np.reshape(
                                trainX, (trainX.shape[0], trainX.shape[1], 1))
                            testX = np.reshape(
                                testX, (testX.shape[0], testX.shape[1], 1))
                            print trainX.shape
                            print trainX
                            # ==============================================================
                            # Stateful LSTM stacked
                            # ==============================================================
                            print "Stacked stateful LSTM"
                            model = LstmTrain.create_stacked_lstm(
                                trainX, trainY, look_back, batch_size)
                            print model.summary()
                            trainScore = model.evaluate(trainX, trainY, batch_size=batch_size, verbose=0)
                            print('Train Score: ', trainScore)
                            testScore = model.evaluate(testX[:252], testY[:252], batch_size=batch_size, verbose=0)
                            print('Test Score: ', testScore)
                            look_ahead = len(testX)
                            # print look_ahead
                            trainPredict = [np.vstack([trainX[-1][1:], trainY[-1]])]
                            predictions = np.zeros((len(testX), 1))
                            for i in range(look_ahead):
                                prediction = model.predict(np.array([trainPredict[-1]]), batch_size=batch_size)
                                predictions[i] = prediction
                                trainPredict.append(np.vstack([trainPredict[-1][1:], prediction]))
                            plt.figure(figsize=(12, 5))
                            # print len(predictions)
                            # print len(dataset[train_size:(train_size+look_ahead)])
                            plt.plot(np.arange(look_ahead), scaler.inverse_transform(predictions) ,'r', label="Prediction")
                            plt.plot(np.arange(look_ahead), scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]), label="Real data")
                            plt.legend()
                            # plt.show()
                            plt.savefig(
                                "./lstm/graphs/" +
                                normalizeName(
                                    "stacked_lstm_" + hostname + "!" + service + ".png"
                                    )
                                )
                            rmse = sqrt(mean_squared_error(
                                scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]),
                                scaler.inverse_transform(predictions)))
                            print rmse
                            print "=============================================================="
                            # ==============================================================
                            # Basic LSTM
                            # ==============================================================
                            print "Basic LSTM"
                            model = LstmTrain.create_small_lstm(
                                trainX, trainY, batch_size)
                            print model.summary()
                            trainScore = model.evaluate(trainX, trainY, batch_size=batch_size, verbose=0)
                            print('Train Score: ', trainScore)
                            testScore = model.evaluate(testX[:252], testY[:252], batch_size=batch_size, verbose=0)
                            print('Test Score: ', testScore)
                            look_ahead = len(testX)
                            # print look_ahead
                            trainPredict = [np.vstack([trainX[-1][1:], trainY[-1]])]
                            predictions = np.zeros((len(testX), 1))
                            for i in range(look_ahead):
                                prediction = model.predict(np.array([trainPredict[-1]]), batch_size=batch_size)
                                predictions[i] = prediction
                                trainPredict.append(np.vstack([trainPredict[-1][1:], prediction]))
                            plt.figure(figsize=(12, 5))
                            # print len(predictions)
                            # print len(dataset[train_size:(train_size+look_ahead)])
                            plt.plot(np.arange(look_ahead), scaler.inverse_transform(predictions) ,'r', label="Prediction")
                            plt.plot(np.arange(look_ahead), scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]), label="Real data")
                            plt.legend()
                            # plt.show()
                            plt.savefig(
                                "./lstm/graphs/" +
                                normalizeName(
                                    "basic_lstm_" + hostname + "!" + service + ".png"
                                    )
                                )
                            rmse = sqrt(mean_squared_error(
                                scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]),
                                scaler.inverse_transform(predictions)))
                            print rmse
                            print "=============================================================="
                            # ==============================================================
                            # Stateful LSTM
                            # ==============================================================
                            print "Stateful LSTM"
                            model = LstmTrain.create_stateful_lstm(
                                trainX, trainY, look_back, batch_size)
                            print model.summary()
                            trainScore = model.evaluate(trainX, trainY, batch_size=batch_size, verbose=0)
                            print('Train Score: ', trainScore)
                            testScore = model.evaluate(testX[:252], testY[:252], batch_size=batch_size, verbose=0)
                            print('Test Score: ', testScore)
                            look_ahead = len(testX)
                            # print look_ahead
                            trainPredict = [np.vstack([trainX[-1][1:], trainY[-1]])]
                            predictions = np.zeros((len(testX), 1))
                            for i in range(look_ahead):
                                prediction = model.predict(np.array([trainPredict[-1]]), batch_size=batch_size)
                                predictions[i] = prediction
                                trainPredict.append(np.vstack([trainPredict[-1][1:], prediction]))
                            plt.figure(figsize=(12, 5))
                            # print len(predictions)
                            # print len(dataset[train_size:(train_size+look_ahead)])
                            plt.plot(np.arange(look_ahead), scaler.inverse_transform(predictions) ,'r', label="Prediction")
                            plt.plot(np.arange(look_ahead), scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]), label="Real data")
                            plt.legend()
                            # plt.show()
                            plt.savefig(
                                "./lstm/graphs/" +
                                normalizeName(
                                    "staful_lstm_" + hostname + "!" + service + ".png"
                                    )
                                )
                            rmse = sqrt(mean_squared_error(
                                scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]),
                                scaler.inverse_transform(predictions)))
                            print rmse
                            print "=============================================================="


if __name__ == "__main__":
    main()
