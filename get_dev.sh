docker run -d -p 8086:8086 influxdb
docker run -d -v `pwd`/apps/dockerfiles/init.sql:/docker-entrypoint-initdb.d/init.sql -p 5432:5432 library/postgres
docker run -d -p 5672:5672 rabbitmq:3

#  ps | grep python | awk '{print $1}' | while read line; do kill $line; done
