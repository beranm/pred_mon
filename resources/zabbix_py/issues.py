from pyzabbix import ZabbixAPI
import json

# The hostname at which the Zabbix web interface is available
#ZABBIX_SERVER = 'https://zabbix.lukapo.cz/'

#zapi = ZabbixAPI(ZABBIX_SERVER)

# Login to the Zabbix API
#zapi.login('martin.beranek', '7ldeYTa3Ks')


zapi = ZabbixAPI("https://secure.nemor.cz/zabbix/")
zapi.login('predmon', '$completelly&hidden@non@used$password')

# Get a list of all issues (AKA tripped triggers)


def getItemById(zapi, idi):
    """Return item by id."""
    return zapi.item.get(output='extend', itemids=[idi])


def getBadItems(zapi):
    """Return all the items which are in triggers classified as in troubles."""
    triggers = zapi.trigger.get(only_true=1,
                                skipDependent=1,
                                monitored=1,
                                active=1,
                                output='extend',
                                selectFunctions='extend',
                                expandDescription=1,
                                selectHosts=['host'],
                                filter={"value": 1},
                                )
    ids = []
    for i in triggers:
        for j in i['functions']:
            ids.append(j['itemid'])
    return ids


# Do another query to find out which issues are Unacknowledged
# unack_triggers = zapi.trigger.get(only_true=1,
#                                   skipDependent=1,
#                                   monitored=1,
#                                   active=1,
#                                   output='extend',
#                                   expandDescription=1,
#                                   selectFunctions='extend',
#                                   selectHosts=['host'],
#                                   withLastEventUnacknowledged=1,
#                                   )
# unack_trigger_ids = [t['triggerid'] for t in unack_triggers]
# for t in triggers:
#    t['unacknowledged'] = True if t['triggerid'] in unack_trigger_ids \
#        else False

# Print a list containing only "tripped" triggers
#
# triggers = zapi.trigger.get(only_true=1,
#                             skipDependent=1,
#                             monitored=1,
#                             active=1,
#                             output='extend',
#                             selectFunctions='extend',
#                             expandDescription=1,
#                             selectHosts=['host'],
#                             filter={"value": 1},
#                             )
#
# for t in triggers:
#     print "=============================================================="
#     print json.dumps(t, indent=2)
#     print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

for i in getBadItems(zapi):
    print i
    print json.dumps(getItemById(zapi, i), indent=2)
