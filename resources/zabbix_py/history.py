import sys
import logging
from pyzabbix import ZabbixAPI
from datetime import datetime
import time
import requests
import json

stream = logging.StreamHandler(sys.stdout)
stream.setLevel(logging.DEBUG)
log = logging.getLogger('pyzabbix')
log.addHandler(stream)
log.setLevel(logging.DEBUG)

ZABBIX_SERVER = 'https://zabbix.lukapo.cz/'
zapi = ZabbixAPI(ZABBIX_SERVER)
zapi.login('martin.beranek', 'letadlo$potato@')

item_id = '52466'

# Create a time range
time_till = time.mktime(datetime.now().timetuple())
time_from = time_till - 60 * 60 * 4  # 4 hours

# Query item's history (integer) data
history = zapi.history.get(itemids=[item_id],
                           time_from=time_from,
                           time_till=time_till,
                           output='extend',
                           limit='5000',
                           )

# If nothing was found, try getting it from history (float) data
if not len(history):
    history = zapi.history.get(itemids=[item_id],
                               time_from=time_from,
                               time_till=time_till,
                               output='extend',
                               limit='5000',
                               history=0,
                               )

for i in history:
    print(i)
