  var READTHEDOCS_DATA = {
    project: "hdbscan",
    version: "latest",
    language: "en",
    subprojects: {},
    canonical_url: "http://hdbscan.readthedocs.io/en/latest/",
    theme: "sphinx_rtd_theme",
    builder: "sphinx",
    docroot: "/docs/",
    source_suffix: ".rst",
    api_host: "https://readthedocs.org",
    commit: "181fcbf6"
  };
  