import numpy as np
from numpy import array, linspace
import numpy
from sklearn.neighbors.kde import KernelDensity
from scipy.signal import argrelextrema
from scipy.stats import gaussian_kde
import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth
import matplotlib.pyplot as plt
import datetime
from time import gmtime, strftime, mktime
import matplotlib as mpl 
import matplotlib.style
mpl.style.use('classic')


def timestamp_to_app_time(time_caption):
    dtime = datetime.datetime.fromtimestamp(
        int(time_caption)
    ).strftime('%H:%M:%S %d.%m.%Y')
    return dtime


# bw_method='silverman_factor'
# bw_method='scotts_factor'
def estimate_time_clusters_gausian(data, bw_method='scott'):
    xmin = min(data)
    xmax = max(data)
    x = linspace(xmin, xmax, 100000)
    # get actual kernel density.
    density = gaussian_kde(data, bw_method=bw_method)
    mi, ma = argrelextrema(
        density(x), np.less), argrelextrema(density(x), np.greater)

    """
    print("mi")
    print(x[mi])
    print("ma")
    print(x[ma])
    print("==============")
    for i in np.array(ma).flatten():
        print (i)
    print("==============")

    # print(data[data >= mi[0]])
    """
    mi = [x[i] for i in np.array(mi).flatten()]
    ma = [x[i] for i in np.array(ma).flatten()]
    # if len(mi) == 0:
    #     mi.append()
    data = np.array(data)
    print("mi")
    print(mi)
    print("ma")
    print(ma)

    clusters = []
    if len(mi) == 1:
        clusters.append(data[(data < mi[0])])
        clusters.append(data[(data >= mi[0])])
    elif len(mi) == 2:
        clusters.append(data[(data < mi[0])])
        clusters.append(data[(data >= mi[0]) * (data < mi[1])])
        clusters.append(data[(data >= mi[1])])
    elif len(mi) > 2:
        i = 0
        while True:
            if i == 0:
                clusters.append(data[data < mi[i]])
            elif i < len(mi) - 1:
                clusters.append(data[(data >= mi[i - 1]) * (data < mi[i])])
            elif i == len(mi) - 1:
                clusters.append(data[(data >= mi[i - 1]) * (data < mi[i])])
                clusters.append(data[(data >= mi[i])])
                break
            i = i + 1

    frst_stp = min(x) - 5
    i = 1
    plt.clf()
    
    """
    while frst_stp < max(x):
        plt.axvline(x=frst_stp, label="Sample " + str(i))
        frst_stp = frst_stp + 30
        i = i + 1
    plt.plot(data, np.zeros(len(data)).tolist(), '+', label='Sampled data')
    plt.legend(loc=1)
    plt.xlabel('Time')
    plt.savefig("./timestamps_data_sampled.png")
    plt.clf()
    """

    plt.plot(data, np.zeros(len(data)).tolist(), '+', label='Data')
    # plt.plot(data, np.zeros(len(data)).tolist(), '+', label='Sampled data')
    plt.xlabel('Time')
    plt.legend()
    plt.savefig("./timestamps_data.png")
    plt.clf()
    plt.plot(x, density(x), label='Density function')
    plt.plot(data, np.zeros(len(data)).tolist(), '+', label='Sampled data')
    plt.xlabel('Time')
    plt.ylabel('Density')
    plt.legend()
    plt.savefig("./timestamps_density_" + bw_method + ".png")
    plt.clf()

    plt.plot(data, np.zeros(len(data)).tolist(), '+', label='Sampled data')
    plt.plot(x, density(x), label='Density function')
    for i in mi:
        plt.axvline(x=i, label="Local minimum")

    plt.xlabel('Time')
    plt.ylabel('Density')
    plt.legend()
    plt.savefig("./divided_density_" + bw_method + ".png")
    plt.clf()

    return ([cluster.tolist() for cluster in clusters])



# data_input = [1509203289, 1509203301, 1509203313, 1509203624, 1509203634, 1509203627]

# data_input = [1509267873, 1509269229, 1509269181, 1509271244, 1509271256, 1509271286]


# data_input = [1509113588, 1509113677.252376, 1509113679.252376, 1509213677.252376, 1509213675.252376]
# data = [1, 2, 3, 6, 7, 8, 19, 20, 21]
# data = signal.detrend(data)

# data_input = [1509113588, 1509113677.252376, 1509113679.252376, 1509213677.252376, 1509213675.252376]

# data_input = [9113588, 9113677.25238, 9113679.25238, 9213677.25238, 9213675.25238]
# data_input = [8, 77.25238, 179.25238, 177.25238, 213675.25238]
# data_input = [1, 2, 3, 6, 7, 8, 19, 20, 21]
"""
print(data_input)

print("====================MEAN SQUARE===========================")

print(estimate_time_clusters_ms(
    data_input))

print("==========================================================")

print(estimate_time_clusters(
    data_input))

print("===================GAUSIAN================================")
"""
"""
data_input = [
    1509900001, 1509900002, 1509900003,
    1509901051, 1509901052, 1509901053,
    1509902001, 1509902002, 1509902003]

for item in data_input:
    print (str(item) + " & " + str(timestamp_to_app_time(item)) + " \\\\ \hline")

print(estimate_time_clusters_gausian(
    data_input))

"""
"""
data_input = [
    1509900001, 1509900002, 1509900003, 1509900004, 1509900005,
    1509901051, 1509901052, 1509901053, 1509901054, 1509901055,
    1509902001, 1509902002, 1509902003, 1509902003, 1509902003,
    1509902003, 1509902003]


mus, sigmas = 1509900011, 10
s = np.random.normal(mus, sigmas, 10).tolist()

muz, sigmaz = 1509900041, 10
z = np.random.normal(muz, sigmaz, 10).tolist()

ss = s + z

ss = [
    1509900004.3724349, 1509900015.628644, 1509900012.2582219,
    1509900009.0419207, 1509900018.8295271, 1509900017.3246212,
    1509900016.1838686, 1509900010.4363189, 1509899998.0527883,
    1509899989.7086194, 1509900041.3610735, 1509900037.3080232,
    1509900027.6299543, 1509900044.9632342, 1509900033.0536473,
    1509900036.0570729, 1509900035.0507596, 1509900048.574011,
    1509900049.0906413, 1509900038.6390197
]
"""

ss = [
    
    # 1515071124, 1515071134, 1515071140, 1515071141,
    # 1515071143, 1515071148, 1515071152, 1515071158,
    
    1515077704, 1515077714, 1515077725, 1515077729,
    1515077734, 1515077774, 1515077784, 1515077789,

    1515078316,
    1515078816,
    1515079026,
    1515079316,
    1515079526,
    1515079626,
    1515079826,
    1515079926,
    1515079946,
    1515079966,
    
]

for i in ss:
    dtime = datetime.datetime.fromtimestamp(
        int(i)
    ).strftime('%H:%M:%S')
    print (dtime)


# for item in data_input:
#     print (str(item) + " & " + str(timestamp_to_app_time(item)) + " \\\\ \hline")


# bw_method='silverman_factor'
# bw_method='scotts_factor'
print(estimate_time_clusters_gausian(
    ss, 'silverman'))
print(estimate_time_clusters_gausian(
    ss, 'scott'))




# [[1509900004.3724349, 1509900015.628644, 1509900012.2582219, 1509900009.0419207, 1509900018.8295271, 1509900017.3246212, 1509900016.1838686, 1509900010.4363189, 1509899998.0527883, 1509899989.7086194], [1509900041.3610735, 1509900037.3080232, 1509900027.6299543, 1509900044.9632342, 1509900033.0536473, 1509900036.0570729, 1509900035.0507596, 1509900048.574011, 1509900049.0906413, 1509900038.6390197]]
# 
# 
# 
