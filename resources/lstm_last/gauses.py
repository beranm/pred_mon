from scipy import signal
from scipy.fftpack import fft, fftshift
import matplotlib.pyplot as plt


window = signal.gaussian(130, std=22)


raw_values = []
for k in range(4):
    for i in window.tolist():
        raw_values.append(int(i * 100))

print(raw_values)
plt.plot(raw_values)
plt.show()
