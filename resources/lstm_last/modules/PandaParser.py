import pandas as pd
import numpy as np
import datetime
import time


def convert_fill(df):
    # print(df.head())
    df[df.columns[0]].apply(pd.to_numeric).fillna(0)
    df[df.columns[0]] = pd.to_numeric(df[df.columns[0]])
    return df
    # return df.stack().apply(pd.to_numeric, errors='ignore').fillna(0).unstack()


class PandaParser(object):

    def getPandaTime(self, stamp):
        return datetime.datetime.fromtimestamp(
            int(stamp)
        ).strftime('%Y-%m-%d %H:%M:%S')

    def splitTrainDfTest(self, data, ration=0.8):
        dvs = int(len(data.index)*ration)
        return data.iloc[:dvs], data.iloc[dvs:]

    def splitTrainTest(self, data, ration=0.8):
        dvs = int(len(data)*ration)
        # print(dvs)
        return data[0:dvs], data[dvs:]


    def generateTmp(self, result, time_execution, service):
        if (service == "ping4"):
            return pd.DataFrame([result],
                                index=[pd.Timestamp(
                                       datetime.datetime.fromtimestamp(
                                           int(time_execution)
                                       ).strftime('%Y-%m-%d %H:%M:%S')
                                       )],
                                columns=['Lost', 'Avg'])
        elif (service == "load"):
            return pd.DataFrame([result],
                                index=[pd.Timestamp(
                                       datetime.datetime.fromtimestamp(
                                           int(time_execution)
                                       ).strftime('%Y-%m-%d %H:%M:%S')
                                       )],
                                columns=['One minute',
                                         'Five minutes',
                                         'Fiveteen minutes'])
        elif (service == "swap"):
            return pd.DataFrame([result],
                                index=[pd.Timestamp(
                                       datetime.datetime.fromtimestamp(
                                           int(time_execution)
                                       ).strftime('%Y-%m-%d %H:%M:%S')
                                       )],
                                columns=['Taken'])
        elif (service == "users"):
            return pd.DataFrame([result],
                                index=[pd.Timestamp(
                                       datetime.datetime.fromtimestamp(
                                           int(time_execution)
                                       ).strftime('%Y-%m-%d %H:%M:%S')
                                       )],
                                columns=['Logged in'])
        elif (service == "remote_disk"):
            # {u'/boot': [32.0], u'/var/www/cdn': [263698.0, 339087.0, 357925.0, 376764.0], u'/': [34515.0]}
            data = []

            for k in result.keys():
                data.append(result[k][0])

            return pd.DataFrame([data],
                                index=[pd.Timestamp(
                                       datetime.datetime.fromtimestamp(
                                           int(time_execution)
                                       ).strftime('%Y-%m-%d %H:%M:%S')
                                       )],
                                columns=result.keys())
        else:
            return pd.DataFrame([result],
                                index=[pd.Timestamp(
                                       datetime.datetime.fromtimestamp(
                                           int(time_execution)
                                       ).strftime('%Y-%m-%d %H:%M:%S')
                                       )],
                                columns=[service])
