from pandas import DataFrame
from pandas import Series
from pandas import concat
from pandas import read_csv
from pandas import datetime
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from math import sqrt
from matplotlib import pyplot
import matplotlib.style
import numpy
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
import logging
from pyzabbix import ZabbixAPI
import sys
import time
import pandas as pd
from scipy import signal

pyplot.style.use('classic')

def parser(x):
    return datetime.strptime(x, '%Y-%m')


# frame a sequence as a supervised learning problem
def timeseries_to_supervised(data, lag=1):
    df = DataFrame(data)
    columns = [df.shift(i) for i in range(1, lag + 1)]
    columns.append(df)
    df = concat(columns, axis=1)
    df.fillna(0, inplace=True)
    return df


# create a differenced series
def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return Series(diff)


# invert differenced value
def inverse_difference(history, yhat, interval=1):
    return yhat + history[-interval]


# scale train and test data to [-1, 1]
def scale(train, test):
    # fit scaler
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaler = scaler.fit(train)
    # transform train
    train = train.reshape(train.shape[0], train.shape[1])
    train_scaled = scaler.transform(train)
    # transform test
    test = test.reshape(test.shape[0], test.shape[1])
    test_scaled = scaler.transform(test)
    return scaler, train_scaled, test_scaled


# inverse scaling for a forecasted value
def invert_scale(scaler, X, value):
    new_row = [x for x in X] + [value]
    array = numpy.array(new_row)
    array = array.reshape(1, len(array))
    inverted = scaler.inverse_transform(array)
    return inverted[0, -1]


# fit an LSTM network to training data
def fit_lstm(train, batch_size, nb_epoch, neurons):
    X, y = train[:, 0:-1], train[:, -1]
    X = X.reshape(X.shape[0], 1, X.shape[1])
    model = Sequential()
    model.add(LSTM(
        neurons,
        batch_input_shape=(batch_size, X.shape[1], X.shape[2]), stateful=True))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    print(nb_epoch)
    for i in range(nb_epoch):
        model.fit(
            X, y, epochs=1, batch_size=batch_size, verbose=0, shuffle=False)
        model.reset_states()
        print("Yep")
    return model


# make a one-step forecast
def forecast_lstm(model, batch_size, X):
    X = X.reshape(1, 1, len(X))
    yhat = model.predict(X, batch_size=batch_size)
    return yhat[0, 0]


parser = DataParser()
pprser = PandaParser()

window = signal.gaussian(130, std=22)

raw_values = []
for k in range(4):
    for i in window.tolist():
        raw_values.append(int(i * 100))

diff_values = difference(raw_values, 1)


supervised = timeseries_to_supervised(diff_values, 1)
supervised_values = supervised.values

sz = len(supervised.values)

print(sz)
print(supervised_values)

print(supervised_values.shape)

train, test = supervised_values[0: int(- sz * 0.2)], supervised_values[int(- sz * 0.2):]

print('train')
print(len(train))
print('test')
print(len(test))

scaler, train_scaled, test_scaled = scale(train, test)

print("tst")
print(test_scaled[-1, 0:-1])
print("tst")
# fit the model
# lstm_model = fit_lstm(train_scaled, 1, 4, 1)
lstm_model = fit_lstm(train_scaled, 1, 1, 1)
# lstm_model = fit_lstm(train_scaled, 1, 1500, 1)
# forecast the entire training dataset to build up state for forecasting
train_reshaped = train_scaled[:, 0].reshape(len(train_scaled), 1, 1)
lstm_model.predict(train_reshaped, batch_size=1)

# walk-forward validation on the test data
predictions = list()

for i in range(len(test_scaled)):
    # make one-step forecast
    X, y = test_scaled[i, 0:-1], test_scaled[i, -1]
    print(X)
    yhat = forecast_lstm(lstm_model, 1, X)
    # invert scaling
    yhat = invert_scale(scaler, X, yhat)
    # invert differencing
    yhat = inverse_difference(raw_values, yhat, len(test_scaled) + 1 - i)
    # store forecast
    predictions.append(yhat)
    expected = raw_values[len(train) + i + 1]
    # print('Iteration=%d, Predicted=%f, Expected=%f' % (i + 1, yhat, expected))

rmse = sqrt(mean_squared_error(raw_values[int(- sz * 0.2):], predictions))
print('Test RMSE: %.3f' % rmse)
pyplot.plot(
    range(len(raw_values[0: int(- sz * 0.2)])),
    raw_values[0: int(- sz * 0.2)],
    label='Training data')


bsl = len(raw_values[0: int(- sz * 0.2)])
psl = len(raw_values[int(- sz * 0.2):])

print(bsl)
print(psl)
print(len(range(bsl, psl)))
print(len(predictions))

pyplot.plot(
    range(bsl, bsl + psl),
    raw_values[int(- sz * 0.2):],
    label='Test data')
pyplot.plot(
    range(bsl, bsl + psl),
    predictions,
    label='Prediction')



preds = []
preds.append(test_scaled[-1, 0:-1])
last_kn = raw_values[-1:]
print(last_kn)

for i in range(100):
    # make one-step forecast
    X = numpy.array(preds[-1:])
    print(X)
    yhat = forecast_lstm(lstm_model, 1, X)
    # invert scaling
    yhat = invert_scale(scaler, X, yhat)
    # invert differencing
    yhat = yhat + last_kn
    # store forecast
    preds.append(yhat)

preds[0] = last_kn

pyplot.plot(
    range(bsl + psl, bsl + psl + len(preds)),
    preds,
    label='Uknown future')


pyplot.legend(loc='best')
pyplot.savefig('./result2.png')

