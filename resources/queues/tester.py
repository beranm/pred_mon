import pika
import json


"""
SENDER PART
"""
# add to checks, options and so on
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='collect')

channel.basic_publish(exchange='',
                      routing_key='collect',
                      body=json.dumps({"ahoj":"papa", "blabla": "bla"}))
print(" [x] Sent 'Hello World!'")
connection.close()

"""
RECEIVER PART
"""


connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue='collect')

def callback(ch, method, properties, body):
    print(" [x] Received %r" % json.loads(body))


channel.basic_consume(callback, # Return in callback functions as JS, AWSme
                      queue='collect',
                      no_ack=True)


print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
