import re
import ast


class DataParser(object):

    """
    def __init__(self):
    """

    def parseName(self, name):
        rtr = name.split("!")
        if(len(rtr) != 2):
            raise ValueError('The name of service is not in specific format')
        out = {}
        out['hostname'] = rtr[0]
        out['service'] = rtr[1]
        return(out)

    def parseLastState(self, line):
        return(line['attrs']['last_check_result'])

    def parseTimestamp(self, line):
        return(line['attrs']['last_check_result']['execution_end'])

    def parseState(self, line):
        return(line['attrs']['state'])

    def parseValue(self, name, output):
        tmp = []
        if name == "load":
            tmp = output['output'].split(" ")
            output = []
            output.append(float(tmp[4].replace(",", "")))
            output.append(float(tmp[5].replace(",", "")))
            output.append(float(tmp[6].replace(",", "")))
        elif name == "ping4":
            tmp = output['output'].split(" ")
            output = []
            output.append(float(tmp[6].replace("%", "").replace(",", "")))
            output.append(float(tmp[9]))
        elif name == "swap":
            tmp = output['output'].split(" ")
            # output = []
            # output.append(tmp[3])
            output = 100 - float(tmp[3].replace("%", ""))
        elif name == "users":
            tmp = output['output'].split(" ")
            # output = []
            # output.append(tmp[3])
            output = float(tmp[3])
        elif name == "remote_disk":
            tmp = output['performance_data']
            output = {}
            for line in tmp:
                output[line.split("=")[0]] = []
                if re.match(".*GB", line.split("=")[1].split(";")[0]):
                    output[line.split("=")[0]] \
                        .append(
                            float(
                                line.split("=")[1]
                                .split(";")[0].replace("GB", "")
                            ) * 1024
                    )
                else:
                    output[line.split("=")[0]] \
                        .append(
                            float(
                                line.split("=")[1]
                                .split(";")[0].replace("MB", "")
                            )
                    )
            output[line.split("=")[0]] \
                .append(
                    float(
                        line.split("=")[1].split(";")[1]
                    )
            )
            output[line.split("=")[0]] \
                .append(float(line.split("=")[1].split(";")[2]))
            output[line.split("=")[0]] \
                .append(float(line.split("=")[1].split(";")[4]))
        else:
            output = "\"" + output['output'].replace("\n", "") + "\""
        return output

    def parseResult(self, service, result):
        if(service == "remote_disk"):
            return ast.literal_eval(result)
        elif(service == "load"):
            return ast.literal_eval(result)
        elif(service == "ping4"):
            return ast.literal_eval(result)
        elif(service == "swap"):
            return ast.literal_eval(result)
        elif(service == "users"):
            return ast.literal_eval(result)
        elif(service == "ssh"):
            if result.split()[1] == "OK":
                return 0
            return 1
        elif(service.find("-processes-check") != -1):
            try:
                return float(result.split()[2])
            except ValueError:
                return 0
        try:
            return (float(result))
        except ValueError:
            return 0
