from peewee import *

"""
class Database(object):

    def __init__(self, credentials):
db = MySQLDatabase(credentials['name'],
                   user=credentials['name'],
                   charset='utf8_general_ci')
"""

database_proxy = Proxy()


class BaseModel(Model):
    class Meta:
        database = database_proxy


class Record(BaseModel):
    state = FloatField(unique=True)
    service = TextField(unique=True)
    result = TextField(unique=True)
    hostname = TextField(unique=True)
    time_execution = FloatField(unique=True)


class Classifier(BaseModel):
    time_created = FloatField(unique=True)
    time_retrained = FloatField(unique=True)
    hostname = TextField(unique=True)
    service = TextField(unique=True)
    column = TextField(unique=True)
    classifier_path = TextField(unique=True)
    seasonal_pdq = TextField(unique=True)
    pdq = TextField(unique=True)
    id = IntegerField(unique=True)
