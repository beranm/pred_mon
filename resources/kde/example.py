from numpy import array, linspace
import numpy as np
from sklearn.neighbors.kde import KernelDensity
from matplotlib.pyplot import plot
from scipy.signal import argrelextrema
import sys
from scipy.stats import gaussian_kde

inp = [10, 11, 9, 23, 21, 11, 45, 44, 43, 20, 11, 12, 65, 66, 67]
# inp = [1, 2, 3, 7, 8, 9, 12, 13, 14, 17, 18, 19]
a = array(inp).reshape(-1, 1)
print (a)
kde = gaussian_kde(array(inp))
kde.set_bandwidth(bw_method='scott')
kde.set_bandwidth(bw_method='silverman')
print (kde.factor)


kde = KernelDensity(kernel='gaussian', bandwidth=kde.factor).fit(a)
s = linspace(0, 50)
e = kde.score_samples(s.reshape(-1, 1))
plot(s, e)


mi, ma = argrelextrema(e, np.less)[0], argrelextrema(e, np.greater)[0]

if mi is []:
    return None


# 1, 5, 10

print("=====================")
clusters = []
if len(mi) == 1:
    clusters.append(a[(a < mi[0])])
    clusters.append(a[(a >= mi[0])])
elif len(mi) == 2:
    clusters.append(a[(a < mi[0])])
    clusters.append(a[(a >= mi[0]) * (a < mi[1])])
    clusters.append(a[(a >= mi[1])])
elif len(mi) > 2:
    i = 0
    while True:
        if i == 0:
            clusters.append(a[a < mi[i]])
        elif i < len(mi) - 1:
            clusters.append(a[(a >= mi[i-1]) * (a < mi[i])])
        elif i == len(mi) - 1:
            clusters.append(a[(a >= mi[i-1]) * (a < mi[i])])
            clusters.append(a[(a >= mi[i])])
            break
        i = i + 1

return ([cluster.tolist() for cluster in clusters])
