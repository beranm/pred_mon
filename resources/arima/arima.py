"""Create graphs, arimas and stuff."""
import sys
import pandas as pd
import numpy
import matplotlib.pyplot as plt
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
from ArimaTrain import ArimaTrain
from modules.Graphy import Graphy
from peewee import *
from modules.Database import *
from sklearn.metrics import mean_squared_error
import yaml
from modules.FileRenames import normalizeName
from math import sqrt
import matplotlib.ticker as ticker
import modules.dataGetter as dataGetter


def main():
    #plt.style.use('fivethirtyeight')

    parser = OptionParser(
        usage="usage: %prog [options] files",
        version="%prog 0.1")
    parser.add_option(
        "--ici",
        action="store",
        default="./services/selected.yml",
        dest="services",
        type="string",
        help="Services to manage in ARIMA model")
    parser.add_option(
        "--db",
        action="store",
        default="./credentials/database.yml",
        dest="database",
        type="string",
        help="Transfer data to database")
    parser.add_option(
        "--test",
        action="store_true",
        default=False,
        dest="tests",
        help="Create test on data with training and testing dataset")

    args = sys.argv[1:]
    (options, args) = parser.parse_args()

    with open(options.database, 'r') as f:
        credentials = yaml.load(f)

    credentials = credentials['database']

    db = PostgresqlDatabase(credentials['name'],
                       user=credentials['user'],
                       password=credentials['password'], host=credentials['address'])

    database_proxy.initialize(db)

    db.connect()

    parser = DataParser()

    pprser = PandaParser()

    artrain = ArimaTrain()

    graphy = Graphy()

    with open(options.services, 'r') as f:
        docs = yaml.load_all(f)
        for doc in docs:
            for hostname, v in doc.items():
                for service in v['services']:
                    print(service)
                    recpd = dataGetter.getDataFrame(
                        hostname, service, credentials)
                    print(recpd.describe())
                    # remove this later please
                    xtic = map(
                        lambda x: str(x),
                        recpd.index[0::100].strftime('%Y-%m-%d %H:%M').tolist())
                    print xtic
                    # plt.set_xticks(recpd.index[0::100])
                    # plt.set_xticklabels(xtic, fontsize=7)
                    # =========================
                    ax = recpd.plot(figsize=(15, 6))
                    ax.xaxis.set_major_formatter(ticker.FixedFormatter(xtic))
                    plt.gcf().autofmt_xdate()
                    plt.savefig(
                        "./arima/graphs/" +
                        normalizeName(
                            "all_" + hostname + "!" + service + ".png"
                            )
                        )
                    # SPLITTING TO TRAIN TEST FOR FURTHER ANALYSIS
                    if options.services:
                        train, test = pprser.splitTrainTest(recpd)
                        ids = artrain.getClassifier(
                            train, hostname, service, db)
                    else:
                        ids = artrain.getClassifier(
                            recpd, hostname, service, db)
                    print ids

                    for id in ids:
                        try:
                            if options.tests:
                                steps = test.size
                                predictions = artrain.predictFuture(
                                    train, id, hostname, service, db,
                                    steps, keys=test.index)  # need more steps
                            else:
                                steps = 60
                                predictions = artrain.predictFuture(
                                    recpd, id, hostname, service, db)

                            # print predictions
                            graphy.tsplot(
                                recpd[predictions.columns[0]],
                                path="./arima/graphs/pred_diag_" +
                                normalizeName(
                                    hostname +
                                    "!" +
                                    service +
                                    "!" +
                                    predictions.columns[0] +
                                    ".png"), lags=30)
                            graphy.plotPred(
                                recpd[predictions.columns[0]],
                                predictions,
                                path="./arima/graphs/predict_" +
                                normalizeName(
                                    hostname +
                                    "!" +
                                    service +
                                    predictions.columns[0] +
                                    "!" +
                                    ".png"), lags=30)

                            if options.tests:
                                rmse = sqrt(mean_squared_error(test, predictions[-steps:]))
                                print rmse

                            if not options.tests:
                                graphy.plotPred(
                                    recpd[
                                        (
                                            recpd[predictions.columns[0]].index[-1]
                                            - pd.DateOffset(minutes=steps)
                                        ):
                                        recpd[predictions.columns[0]].index[-1]][
                                            predictions.columns[0]],
                                    # recpd[predictions.columns[0]],
                                    predictions[
                                        (
                                            predictions[predictions.columns[0]] \
                                            .index[-1]
                                            - pd.DateOffset(minutes=steps)
                                        ):
                                        predictions[predictions.columns[0]] \
                                        .index[-1]][predictions.columns[0]],
                                    path="./arima/graphs/predict_hour_" +
                                    normalizeName(
                                        hostname +
                                        "!" +
                                        service +
                                        "!" +
                                        predictions.columns[0] +
                                        ".png"), lags=30)
                        except numpy.linalg.linalg.LinAlgError:
                            print("Failed on prediction")

    db.close()


if __name__ == "__main__":
    main()
