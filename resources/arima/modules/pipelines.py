"""
Pipelining for intern communication in application. Consider refactoring to message queue systems later.
"""
import os, fcntl
import ast


def main():
    print "Just to import"


def createPipe(pth):
    """Create named pipeline if not exists."""
    try:
        if not os.path.exists(pth):
            os.mkfifo(pth)
    except OSError:
        print
        pass


def getReadPipe(pth):
    """Opens a pipe for later work."""
    return open(pth, 'r')


def getWritePipe(pth):
    """Opens a pipe for later work."""
    # return os.open(pth, os.O_NONBLOCK | os.O_WRONLY)
    return os.open(pth, os.O_NONBLOCK)


def writePipe(pth, input):
    """Write somthing to pipeline."""
    fd = getWritePipe(pth)
    os.write(fd, str(input))
    os.close(fd)


def readPipe(pth):
    """Write somthing to pipeline."""
    fd = getReadPipe(pth)
    tmp = fd.read()
    fd.close()
    out = []
    for i in tmp.split("\n"):
        out.append(ast.literal_eval(i))
    return out



if __name__ == "__main__":
    main()
