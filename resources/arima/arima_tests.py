"""Create graphs, arimas and stuff."""
import sys
import pandas as pd
import numpy
import matplotlib.pyplot as plt
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
from ArimaTrain import ArimaTrain
from modules.Graphy import Graphy
from peewee import *
from modules.Database import *
from sklearn.metrics import mean_squared_error
import yaml
from modules.FileRenames import normalizeName
from math import sqrt
import matplotlib.ticker as ticker
import modules.dataGetter as dataGetter
from pyzabbix import ZabbixAPI
import logging
import time
from datetime import datetime
import statsmodels.api as sm
import statsmodels
import statsmodels.tsa.api as smt
import warnings


def main():

    parser = DataParser()

    pprser = PandaParser()

    artrain = ArimaTrain()

    graphy = Graphy()

    hostname = 'linode-mioweb1.smartcluster.net'
    service = 'Accepted connections per min'

    stream = logging.StreamHandler(sys.stdout)
    stream.setLevel(logging.DEBUG)
    log = logging.getLogger('pyzabbix')
    log.addHandler(stream)
    log.setLevel(logging.DEBUG)

    ZABBIX_SERVER = 'https://zabbix.lukapo.cz/'
    zapi = ZabbixAPI(ZABBIX_SERVER)
    zapi.login('martin.beranek', 'letadlo$')

    item_id = '52466'

    # Create a time range
    time_till = time.mktime(datetime.now().timetuple())
    time_from = time_till - 60 * 60 * 24 * 3  # 4 hours

    # Query item's history (integer) data
    history = zapi.history.get(itemids=[item_id],
                               time_from=time_from,
                               time_till=time_till,
                               output='extend'
                               # limit='5000',
                               )

    # If nothing was found, try getting it from history (float) data
    if not len(history):
        history = zapi.history.get(itemids=[item_id],
                                   time_from=time_from,
                                   time_till=time_till,
                                   output='extend',
                                   history=0,
                                   )
    sjoin = []
    for i in history:
        tmp = pprser.generateTmp(i['value'],
                                 i['clock'],
                                 service)

        sjoin.append(tmp)
    recpd = pd.concat(sjoin)

    print(recpd.describe())
    print(recpd.head())
    # remove this later please
    xtic = list(map(
        lambda x: str(x),
        recpd.index[0::100].strftime('%Y-%m-%d %H:%M').tolist()))
    print (xtic)
    # plt.set_xticks(recpd.index[0::100])
    # plt.set_xticklabels(xtic, fontsize=7)
    # =========================
    recpd = recpd.astype(float)
    ax = recpd.plot(figsize=(15, 6))
    ax.xaxis.set_major_formatter(ticker.FixedFormatter(xtic))
    plt.gcf().autofmt_xdate()
    plt.savefig(
        "./" +
        normalizeName(
            "all_" + hostname + "!" + service + ".png"
        )
    )

    # SPLITTING TO TRAIN TEST FOR FURTHER ANALYSIS
    train, test = pprser.splitTrainTest(recpd)



    # training for parameters
    warnings.filterwarnings("ignore")
    min_aic = 0
    order = (1, 1, 1)
    flag = 0
    pq_rng = range(0, 8)
    ma_rng = range(0, 4)
    d_rng = range(0, 8)

    for i in pq_rng:
        for d in d_rng:
            for j in ma_rng:
                if (i, d, j) == (0, 0, 0):
                    continue
                try:
                    print('ARIMA: {}'.format((i, d, j)))
                    tmp_mdl = smt.ARIMA(train[service], order=(i, d, j))
                    mod_fit = tmp_mdl.fit(
                        method='mle',
                        trend='nc',
                        disp=False, transparams=False)
                    prediction = mod_fit.predict(
                        start=1,
                        end=len(train[service].index),
                        typ='levels')

                    print(' - AIC:{}'.format(mod_fit.aic))

                    if flag == 0:
                        order = (i, d, j)
                        min_aic = mod_fit.aic
                        flag = 1
                    if mod_fit.aic < min_aic:
                        order = (i, d, j)
                except (numpy.linalg.linalg.LinAlgError, ValueError, TypeError):
                    print("FAILED!")
                    continue

    print("===============")
    print(order)
    print("===============")
    mod = smt.ARIMA(train[service],
                    order=order)
    mod_fit = mod.fit(method='mle', trend='nc', disp=False, transparams=False)


    steps = 60
    prediction = mod_fit.predict(
        start=1,
        end=len(train[service].index) + steps,
        typ='levels')

    prediction = pd.DataFrame(prediction)

    print(prediction.index)

    preddates = train[service].index
    preddates = preddates.append(
        pd.date_range(
            recpd[service].index[-1], periods=steps, freq='1Min')
    )
    prediction.index = preddates

    graphy.tsplot(
        recpd[service],
        path="./graphs/pred_diag_" +
        normalizeName(
            hostname +
            "!" +
            service +
            "!" +
            ".png"), lags=30)
    graphy.plotPred(
        recpd[service],
        prediction,
        path="./graphs/predict_" +
        normalizeName(
            hostname +
            "!" +
            service +
            "!" +
            ".png"), lags=30)
    """
    rmse = sqrt(mean_squared_error(test[service], prediction[-steps:]))
    print (rmse)
    """
    

if __name__ == "__main__":
    main()
