import os
import glob

search_dir = "/etc/"

files = filter(os.path.isfile, glob.glob(search_dir + "*"))
files.sort(key=lambda x: os.path.getmtime(x))


print files