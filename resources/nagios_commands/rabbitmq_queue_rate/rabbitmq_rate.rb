#!/bin/ruby
# encoding: utf-8

require 'json'
require 'net/http'
require 'net/https'
require 'uri'
require 'pp' #for debugging
require 'optparse'
 
options = {}
parsed = nil

#url = "http://rabbit-01.3mwcloud.cz:15672/api/queues/%2fkolonialcz/google-analytics?lengths_age=300"
#url = "http://rabbit-01.3mwcloud.cz:15672/api/queues/%2fkolonialcz/mongo-db?lengths_age=300"
#url = "http://rabbit-cluster-02.echit.cz:15672/api/queues/%2Fbambinosk/testovani-clusteru?lengths_age=60&lengths_incr=5&msg_rates_age=60&msg_rates_incr=10&data_rates_age=60&data_rates_incr=10"
#uri = URI(url)


 optparse = OptionParser.new do |opts|
   # Set a banner, displayed at the top
   # of the help screen.
   opts.banner = "Usage: optparse1.rb [options] file1 file2 ..."

   options[:rabbitmq_user] = nil
   opts.on( '-u', '--user USER', 'Login user for RabbitMQ') do |rabbitmq_user|
	options[:rabbitmq_user] = rabbitmq_user
   end

   options[:rabbitmq_password] = nil
   opts.on( '-P', '--password PASSWORD', 'Login password for RabbitMQ') do |rabbitmq_password|
        options[:rabbitmq_password] = rabbitmq_password
   end
 
   options[:ssl] = "http"
   opts.on( '-s', '--ssl', 'Enable SSL' ) do
     options[:ssl] = "https"
   end

   options[:rabbitmq_host] = "localhost"
   opts.on( '-h', '--host HOST', 'Set RabbitMQ host' ) do |rabbitmq_host|
     options[:rabbitmq_host] = rabbitmq_host 
   end

   options[:rabbitmq_port] = 15672
   opts.on( '-p', '--port PORT', 'Set RabbitMQ port' ) do |rabbitmq_port|
     options[:rabbitmq_port] = rabbitmq_port
   end

   options[:rabbitmq_vhost] = "%2f"
   opts.on( '-v', '--vhost HTTP', 'Set name of RabbitMQ vhost' ) do |rabbitmq_vhost|
     options[:rabbitmq_vhost] = rabbitmq_vhost
   end

   options[:rabbitmq_queue] = "all"
   opts.on( '-q', '--queue QUEUE', 'Set name of RabbitMQ queue' ) do |rabbitmq_queue|
     options[:rabbitmq_queue] = rabbitmq_queue
   end

   options[:rabbitmq_data_age] = 300
   opts.on( '-a', '--data_age NUM', 'Set seconds of data age from now' ) do |rabbitmq_data_age|
     options[:rabbitmq_data_age] = rabbitmq_data_age
   end

   options[:rabbitmq_data_increase] = 30
   opts.on( '-i', '--data_increase NUM', 'Set seconds of data series' ) do |rabbitmq_data_increase|
     options[:rabbitmq_data_increase] = rabbitmq_data_increase
   end

   options[:rabbitmq_queue_warning] = 9000
   opts.on( '-w', '--queue_warning NUM', 'Set number of warning queue size' ) do |rabbitmq_queue_warning|
     options[:rabbitmq_queue_warning] = rabbitmq_queue_warning
   end

   options[:rabbitmq_queue_critical] = 10000
   opts.on( '-c', '--queue_critical NUM', 'Set number of critical queue size' ) do |rabbitmq_queue_critical|
     options[:rabbitmq_queue_critical] = rabbitmq_queue_critical
   end

   options[:logfile] = nil
   opts.on( '-l', '--logfile FILE', 'Write log to FILE' ) do |file|
     options[:logfile] = file
   end
 end

optparse.parse!

#pp options



url = "#{options[:ssl]}://#{options[:rabbitmq_host]}:#{options[:rabbitmq_port]}/api/queues/#{options[:rabbitmq_vhost]}/#{options[:rabbitmq_queue]}?lengths_age=#{options[:rabbitmq_data_age]}&lengths_incr=#{options[:rabbitmq_data_increase]}&msg_rates_age=#{options[:rabbitmq_data_age]}&msg_rates_incr=#{options[:rabbitmq_data_increase]}&data_rates_age=#{options[:rabbitmq_data_age]}&data_rates_incr=#{options[:rabbitmq_data_increase]}"
#puts url
uri = URI(url)

Net::HTTP.start(uri.host, uri.port,
  :use_ssl => uri.scheme == 'https', 
  :verify_mode => OpenSSL::SSL::VERIFY_NONE
) do |http|
	request = Net::HTTP::Get.new(uri.request_uri)
	#request.basic_auth('3MWcloudAdmin', '3mwCloudHeslo123')
	request.basic_auth(options[:rabbitmq_user], options[:rabbitmq_password])

	response = http.request(request) # Net::HTTPResponse object

	case response
    	when Net::HTTPSuccess
        	parsed = JSON.parse(response.body)
    	when Net::HTTPUnauthorized
		puts "#{response.message}: username and password set and correct?"
		exit 1
	when Net::HTTPServerError
		puts "#{response.message}: try again later?"
		exit 1
    	else
		puts response.message
		exit 1
	end
end

rabbitmq_consumers = parsed["consumers"].to_i
rabbitmq_messages_count = parsed["messages"].to_i

if defined?(parsed["message_stats"]["ack_details"]["avg_rate"]) then
	rabbitmq_ack_avg_rate = parsed["message_stats"]["ack_details"]["avg_rate"].to_f
else 
	rabbitmq_ack_avg_rate = 0.0
end
exit_code = 0

#puts "Messages: #{rabbitmq_messages_count}"
#puts "AckAvgRate: #{rabbitmq_ack_avg_rate}"

if rabbitmq_messages_count != 0 and rabbitmq_consumers == 0 then
	puts "CRIT: No consumers for queue #{options[:rabbitmq_queue]}, Messages: #{rabbitmq_messages_count}, AvgAckRate: #{rabbitmq_ack_avg_rate}, Consumers: #{rabbitmq_consumers}"
	exit_code = 2
elsif rabbitmq_messages_count != 0 and rabbitmq_ack_avg_rate == 0 then
        puts "CRIT: No acknowledged message in #{options[:rabbitmq_data_age]} seconds, Messages: #{rabbitmq_messages_count}, AvgAckRate: #{rabbitmq_ack_avg_rate}, Consumers: #{rabbitmq_consumers}"
        exit_code = 2
elsif rabbitmq_messages_count >= options[:rabbitmq_queue_critical].to_i then
        puts "CRIT: #{rabbitmq_messages_count} messages in queue #{options[:rabbitmq_queue]}, Messages: #{rabbitmq_messages_count}, AvgAckRate: #{rabbitmq_ack_avg_rate}, Consumers: #{rabbitmq_consumers}"
        exit_code = 2
elsif rabbitmq_messages_count >= options[:rabbitmq_queue_warning].to_i then
	puts "WARN: #{rabbitmq_messages_count} messages in queue #{options[:rabbitmq_queue]}, Messages: #{rabbitmq_messages_count}, AvgAckRate: #{rabbitmq_ack_avg_rate}, Consumers: #{rabbitmq_consumers}"
	exit_code = 1
else
	puts "OK: Messages: #{rabbitmq_messages_count}, AvgAckRate: #{rabbitmq_ack_avg_rate}, Consumers: #{rabbitmq_consumers}"
end

puts url

exit exit_code
