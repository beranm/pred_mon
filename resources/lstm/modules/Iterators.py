import numpy as np
import time
import random


class sinusTime:
    """Sinus wave generator to make some tests."""

    def __init__(self):
        self.nowTime = time.time()
        self.name = "Sinus"
        self.service = "Test"

    def get_state(self):
        return 1 if abs(np.sin(self.nowTime)) > 0.5 else 0

    def __iter__(self):
        return self

    def next(self):
        i = self.nowTime
        self.nowTime = time.time()
        return np.sin(i)

class IterativeSinus:
    """Sinus wave generator to make some tests."""

    def __init__(self, step=0.1):
        self.step = step
        self.i = 0
        self.name = "Sinus with " + str(self.step) + " step"
        self.service = "Test"

    def get_state(self):
        return 1 if abs(np.sin(self.i)) > 0.5 else 0

    def __iter__(self):
        return self

    def next(self):
        i = self.i
        self.i += self.step
        return np.sin(i)


class GrowingSinusTime:
    """Sinus wave generator to make some tests."""

    def __init__(self):
        self.i = 1
        self.name = "Growing sinus"
        self.service = "Test"

    def get_state(self):
        return 0

    def __iter__(self):
        return self

    def next(self):
        i = self.i
        self.i += 1
        return i*np.sin(i)


class lineTime:
    """Line generator to make some tests."""

    def __init__(self):
        self.i = 1
        self.name = "Line"
        self.service = "Test"

    def get_state(self):
        return 1 if self.i % 10 == 0 else 0

    def __iter__(self):
        return self

    def next(self):
        i = self.i
        self.i += 1
        return i


class lineZeroes:
    """Line generator to make some tests."""

    def __init__(self):
        self.name = "Zeroes"
        self.service = "Test"

    def get_state(self):
        return 1 if time.time() % 3 == 0 else 0

    def __iter__(self):
        return self

    def next(self):
        return 0


class randomStuff:
    """Random generator to make some data on percentil tests."""

    def __init__(self):
        self.name = "Randomizer"
        self.service = "Test"

    def get_state(self):
        return 0

    def __iter__(self):
        return self

    def next(self):
        num = random.randint(-10, 10)
        if time.time() % 10 == 0:
            return num*10
        else:
            return num
