"""Containing Graphy class."""
import pandas as pd
import statsmodels.tsa.api as smt
import statsmodels.api as sm
import scipy.stats as scs
import matplotlib as mpl
from time import gmtime, strftime
import matplotlib.dates as mdates
from modules import FileRenames
import numpy as np
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


class Graphy:
    """Graphy class on creating some basic lines and stuff."""

    def __init__(self, hostname="", service=""):
        """Init, setting hostname and service."""
        self.hostname = hostname
        self.service = service

    def tsplot(self, y, path, lags=None, figsize=(15, 12), style=None):
        """Ploting some data analysis."""
        plt.rc('ytick', labelsize=8)
        plt.rc('xtick', labelsize=8)
        plt.locator_params(bins=100)
        if not isinstance(y, pd.Series):
            y = pd.Series(y)
        if style is not None:
            plt.style.use(style)
        # fig = plt.figure(figsize=figsize)
        layout = (3, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=4)
        xtic = list(map(
            lambda x: str(x),
            y.index[0::100].strftime('%Y-%m-%d %H:%M').tolist()))
        # print xtic
        ts_ax.set_xticks(y.index[0::100])
        ts_ax.set_xticklabels(xtic, fontsize=7)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))
        qq_ax = plt.subplot2grid(layout, (2, 0))
        pp_ax = plt.subplot2grid(layout, (2, 1))

        y.plot(ax=ts_ax)
        ts_ax.set_title('Time Series Analysis Plots')
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)
        sm.qqplot(y, line='s', ax=qq_ax)
        qq_ax.set_title('QQ Plot')
        scs.probplot(y, sparams=(y.mean(), y.std()), plot=pp_ax)

        plt.tight_layout()
        plt.savefig(path)

    def plotPred(self, y1, y2, path, lags=None, figsize=(15, 10), style=None):
        """Ploting prediction and past data in two dataframes."""
        """
        if not isinstance(y1, pd.Series):
            y1 = pd.Series(y1)
        if not isinstance(y2, pd.Series):
            y2 = pd.Series(y2)
        """
        # print(y1)
        # print("XXXXXXXXXXXX")
        # print(y2)
        # print("XXXXXXXXXXXXXXXXXXXX")
        plt.close("all")
        if style is not None:
            plt.style.use(style)
        fig = plt.figure(figsize=figsize)
        ax = y1.plot()
        y2.plot(ax=ax)
        xtic = list(map(
            lambda x: str(x),
            y2.index[0::100].strftime('%Y-%m-%d %H:%M').tolist()))
        # print xtic
        ax.xaxis.set_major_formatter(ticker.FixedFormatter(xtic))
        plt.gcf().autofmt_xdate()
        plt.tight_layout()
        plt.savefig(path)

    def plotPastFuture(self, dataFromDirectPast, dataPredicted):
        """Plot past data and near future data with points."""
        dtime = strftime("%d.%m.%Y %H:%M:%S", gmtime())
        # plt.hold(True)

        fig, ax = plt.subplots(1)
        fig.autofmt_xdate()

        fig.suptitle(
            'Estimating: ' +
            self.hostname + ' ' +
            self.service + ' \n' + dtime, fontsize=10)

        # painting backgroung colors
        plt.plot(
            dataFromDirectPast.index,
            dataFromDirectPast.drop('states', axis=1).values,
            color='b')

        plt.plot(
            dataPredicted.index,
            dataPredicted.drop('states', axis=1).values,
            color='c')

        # creating scatter points from past with bad states
        # estimating for direct past
        warnings = np.array([x == 1 for x in dataFromDirectPast['states']])

        plt.scatter(
            dataFromDirectPast.index.values[warnings],
            dataFromDirectPast.drop('states', axis=1).values[warnings],
            label='Past warnings!', c='r')
        plt.scatter(
            dataFromDirectPast.index.values[~warnings],
            dataFromDirectPast.drop('states', axis=1).values[~warnings],
            label='Past Normal', c='g')

        # estimating for near future
        warnings = np.array([x == 1 for x in dataPredicted['states']])

        plt.scatter(
            dataPredicted.index.values[warnings],
            dataPredicted.drop('states', axis=1).values[warnings],
            label='Predicted warnings!', c='y')
        plt.scatter(
            dataPredicted.index.values[~warnings],
            dataPredicted.drop('states', axis=1).values[~warnings],
            label='Predicted normal', c='c')

        # creating formatter for index data on line X
        xfmt = mdates.DateFormatter('%d.%m.%y %H:%M')
        ax.xaxis.set_major_formatter(xfmt)
        dtime = strftime("%d-%m-%Y_%H:%M:%S", gmtime())

        plt.legend(loc=2, prop={'size': 10})
        plt.savefig(
            'static/classify/graphs/' +
            FileRenames.normalizeName(
                self.hostname + '!' +
                self.service + '!' +
                dtime +
                '.png')
        )
        plt.close("all")
