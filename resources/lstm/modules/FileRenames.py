# -*- coding: utf-8 -*-
"""Module for renaming files in the same manner in whole project.

This module keeps few functions to keep names of files in same standard.
Removing slashes dots and white spaces.
"""


def normalizeName(input):
    """Normalize names.

    Args:
        input: Input name of file

    Returns:
        Normalized name

    """
    return input \
        .replace(" ", "_") \
        .replace("/", "+")


def denormalizeName(input):
    """Denormalize names.

    Args:
        input: Input name of file

    Returns:
        Denormalized name

    """
    return input \
        .replace("_", " ") \
        .replace("+", "/")
