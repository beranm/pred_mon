import datetime
from time import gmtime, strftime, mktime


def timestamp_to_app_time(time_caption):
    dtime = datetime.datetime.fromtimestamp(
        int(time_caption)
    ).strftime('%H:%M:%S %d.%m.%Y')
    return dtime


def app_time_to_timestamp(time_caption):
    return mktime(
        datetime.datetime.strptime(
            str(time_caption), '%Y-%m-%d %H:%M:%S').timetuple())


def df_time_to_timestamp(time_caption):
    return mktime(
        datetime.datetime.strptime(
            str(time_caption), '%Y-%m-%d %H:%M:%S').timetuple())
