from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
from pandas import Series
from pandas import concat
from pandas import read_csv
from pandas import datetime
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
from math import sqrt
from matplotlib import pyplot
import numpy
from pandas import DataFrame
import numpy as np
import time

def create_dataset(dataset, look_back=1):
    """Create dataset for LSTM."""
    dataX, dataY = [], []
    for i in range(len(dataset)-look_back):
        dataX.append(dataset[i:(i+look_back), 0])
        dataY.append(dataset[i + look_back, 0])
    return np.array(dataX), np.array(dataY)


def create_small_lstm(trainX, trainY, batch_size=1):
    """Create small LSTM network."""
    # theano.config.compute_test_value = "ignore"
    # create and fit the LSTM network
    model = Sequential()
    # model.add(LSTM(128, input_dim=1))
    model.add(LSTM(128, input_dim=1))
    model.add(Dropout(0.3))
    model.add(Dense(1))
    # model.compile(loss='mean_squared_error', optimizer='adam')
    model.compile(loss='mean_squared_error', optimizer='rmsprop')
    model.fit(trainX, trainY, nb_epoch=3, batch_size=batch_size, verbose=1)
    # model.fit(trainX, trainY, nb_epoch=100, batch_size=batch_size, verbose=2)
    return model


def create_stateful_lstm(trainX, trainY, look_back, batch_size=1):
    """Create stateful LSTM network."""
    model = Sequential()
    # model.add(LSTM(128, batch_input_shape=(batch_size, look_back, 1), stateful=True, return_sequences=True))
    # model.add(Dropout(0.3))
    # model.add(LSTM(128, batch_input_shape=(batch_size, look_back, 1), stateful=True, return_sequences=True))
    # model.add(Dropout(0.3))
    model.add(LSTM(128, batch_input_shape=(batch_size, look_back, 1), stateful=True))
    model.add(Dropout(0.3))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    for i in range(6):
        model.fit(trainX, trainY, nb_epoch=1, batch_size=batch_size, verbose=1, shuffle=False)
        model.reset_states()
    return model


def create_stacked_lstm(trainX, trainY, look_back, batch_size=1):
    """Create stacked LSTM network."""
    # create and fit the LSTM network
    model = Sequential()
    model.add(LSTM(512, batch_input_shape=(batch_size, look_back, 1), stateful=True, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(128, batch_input_shape=(batch_size, look_back, 1), stateful=True, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(256, batch_input_shape=(batch_size, look_back, 1), stateful=True, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(128, batch_input_shape=(batch_size, look_back, 1), stateful=True, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(64, batch_input_shape=(batch_size, look_back, 1), stateful=True))
    model.add(Dropout(0.3))
    model.add(Dense(1))
    model.add(Activation("linear"))
    model.compile(loss='mean_squared_error', optimizer='adam')
    for i in range(6):
        model.fit(trainX, trainY, nb_epoch=1, batch_size=batch_size, verbose=1, shuffle=False)
        model.reset_states()
    return model


def create_rnn(trainX, trainY, look_back, batch_size=1):
    """Create stacked LSTM network."""
    # create and fit the LSTM network
    model = Sequential()
    model.add(Dense(output_dim=8, input_dim=40, activation="relu"))
    model.add(Dropout(0.3))
    for i in range(2):
        model.add(Dense(output_dim=8, activation="relu"))
        model.add(Dropout(0.3))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adagrad')
    model.fit(trainX, trainY, nb_epoch=10, batch_size=128, verbose=0)
    return model
