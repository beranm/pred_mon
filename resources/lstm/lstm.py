"""LSTM Network prediction."""
import sys
import pandas as pd
import numpy
import matplotlib.pyplot as plt
from optparse import OptionParser
from modules.DataParser import DataParser
from modules.PandaParser import PandaParser
# from lstm.lstmTrain import lstmTrain
from modules.Graphy import Graphy
from peewee import *
from modules.Database import *
import yaml
import LstmTrain
from modules.FileRenames import normalizeName
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
import os
import time
import modules.dataGetter as dataGetter
import warnings
import numpy as np
from numpy import newaxis
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler
from math import sqrt
from sklearn.metrics import mean_squared_error
import logging
from pyzabbix import ZabbixAPI
from datetime import datetime
from keras import backend as K

# look_back = 8
look_back = 60
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Hide messy TensorFlow warnings
warnings.filterwarnings("ignore")  # Hide messy Numpy warnings

def smooth(y, box_pts):
    box = np.ones(box_pts) / box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth


parser = DataParser()
pprser = PandaParser()

hostname = 'linode-mioweb1.smartcluster.net'
service = 'Accepted connections per min'
stream = logging.StreamHandler(sys.stdout)
stream.setLevel(logging.DEBUG)
log = logging.getLogger('pyzabbix')
log.addHandler(stream)
log.setLevel(logging.DEBUG)
ZABBIX_SERVER = 'https://zabbix.lukapo.cz/'
zapi = ZabbixAPI(ZABBIX_SERVER)
zapi.login('martin.beranek', 'letadlo$')
item_id = '52466'
# Create a time range
time_till = time.mktime(datetime.now().timetuple())
time_from = time_till - 60 * 60 * 24 * 3  # 4 hours
# Query item's history (integer) data
history = zapi.history.get(itemids=[item_id],
                           time_from=time_from,
                           time_till=time_till,
                           output='extend'
                           # limit='5000',
                           )
# If nothing was found, try getting it from history (float) data
if not len(history):
    history = zapi.history.get(itemids=[item_id],
                               time_from=time_from,
                               time_till=time_till,
                               output='extend',
                               history=0,
                               )
sjoin = []
for i in history:
    tmp = pprser.generateTmp(int(i['value']),
                             i['clock'],
                             service)
    sjoin.append(tmp)
recpd = pd.concat(sjoin)
"""
hostname = "tst"
service = "sin"
i = 0
c = 0
sjoin = []
while i < 40:
    tmp = pprser.generateTmp(int(np.sin(i) * 100),
                             c,
                             service)
    i = i + 0.1
    c = c + 1
    sjoin.append(tmp)
recpd = pd.concat(sjoin)
"""

print(recpd.head())

recpd['Moving average smoothing'] = smooth(recpd[service].values.tolist(), 240)

recpd.plot(figsize=(15, 6))
plt.savefig(
    "./lstm/graphs/" +
    normalizeName(
        "all_" + hostname + "!" + service + ".png"
    )
)
plt.close()

recpd[service] = recpd['Moving average smoothing']

for column in recpd.columns:
    inputData = recpd[column]
    dataset = inputData.values
    batch_size = 1
    # rescale as bellow
    # print scaler.inverse_transform(dataset)
    # dataset = np.cos(np.arange(1000)*(20*np.pi/1000))[0:200][:, None]
    dataset = dataset[:, None]
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataset = scaler.fit_transform(dataset)
    train, test = pprser.splitTrainTest(dataset, ration=0.5)
    train_size, test_size = len(train), len(test)
    trainX, trainY = LstmTrain.create_dataset(
        train, look_back)
    testX, testY = LstmTrain.create_dataset(
        test, look_back)
    trainX = np.reshape(
        trainX, (trainX.shape[0], trainX.shape[1], 1))
    testX = np.reshape(
        testX, (testX.shape[0], testX.shape[1], 1))
    print trainX.shape
    print trainX
    # ==============================================================
    # Stateful LSTM stacked
    # ==============================================================
    """
    K.clear_session()
    print "Stacked stateful LSTM"
    model = LstmTrain.create_stacked_lstm(
        trainX, trainY, look_back, batch_size)
    print model.summary()
    trainScore = model.evaluate(trainX, trainY, batch_size=batch_size, verbose=0)
    print('Train Score: ', trainScore)
    testScore = model.evaluate(testX[:252], testY[:252], batch_size=batch_size, verbose=0)
    print('Test Score: ', testScore)
    look_ahead = len(testX)
    # print look_ahead
    trainPredict = [np.vstack([trainX[-1][1:], trainY[-1]])]
    predictions = np.zeros((len(testX), 1))
    for i in range(look_ahead):
        prediction = model.predict(np.array([trainPredict[-1]]), batch_size=batch_size)
        predictions[i] = prediction
        trainPredict.append(np.vstack([trainPredict[-1][1:], prediction]))
    plt.figure(figsize=(12, 5))
    # print len(predictions)
    # print len(dataset[train_size:(train_size+look_ahead)])
    plt.plot(np.arange(look_ahead), scaler.inverse_transform(predictions) ,'r', label="Prediction")
    plt.plot(np.arange(look_ahead), scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]), label="Real data")
    plt.legend()
    # plt.show()
    plt.savefig(
        "./lstm/graphs/" +
        normalizeName(
            "stacked_lstm_" + hostname + "!" + service + ".png"
            )
        )
    rmse = sqrt(mean_squared_error(
        scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]),
        scaler.inverse_transform(predictions)))
    print rmse
    print "=============================================================="
    """
    # ==============================================================
    # Stateful LSTM
    # ==============================================================
    K.clear_session()
    print "Stateful LSTM"
    model = LstmTrain.create_stateful_lstm(
        trainX, trainY, look_back, batch_size)
    print model.summary()
    trainScore = model.evaluate(trainX, trainY, batch_size=batch_size, verbose=0)
    print('Train Score: ', trainScore)
    testScore = model.evaluate(testX[:252], testY[:252], batch_size=batch_size, verbose=0)
    print('Test Score: ', testScore)
    look_ahead = len(testX)
    # print look_ahead
    trainPredict = [np.vstack([trainX[-1][1:], trainY[-1]])]
    predictions = np.zeros((len(testX), 1))
    for i in range(look_ahead):
        prediction = model.predict(np.array([trainPredict[-1]]), batch_size=batch_size)
        predictions[i] = prediction
        trainPredict.append(np.vstack([trainPredict[-1][1:], prediction]))
    plt.figure(figsize=(12, 5))
    # print len(predictions)
    # print len(dataset[train_size:(train_size+look_ahead)])
    plt.plot(np.arange(look_ahead), scaler.inverse_transform(predictions) ,'r', label="Prediction")
    plt.plot(np.arange(look_ahead), scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]), label="Real data")
    plt.legend()
    # plt.show()
    plt.savefig(
        "./lstm/graphs/" +
        normalizeName(
            "staful_lstm_" + hostname + "!" + service + ".png"
            )
        )
    rmse = sqrt(mean_squared_error(
        scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]),
        scaler.inverse_transform(predictions)))
    print rmse
    print "=============================================================="
    # ==============================================================
    # Basic LSTM
    # ==============================================================
    K.clear_session()
    print "Basic LSTM"
    model = LstmTrain.create_small_lstm(
        trainX, trainY, batch_size)
    print model.summary()
    trainScore = model.evaluate(trainX, trainY, batch_size=batch_size, verbose=0)
    print('Train Score: ', trainScore)
    testScore = model.evaluate(testX[:252], testY[:252], batch_size=batch_size, verbose=0)
    print('Test Score: ', testScore)
    look_ahead = len(testX)
    # print look_ahead
    trainPredict = [np.vstack([trainX[-1][1:], trainY[-1]])]
    predictions = np.zeros((len(testX), 1))
    for i in range(look_ahead):
        prediction = model.predict(np.array([trainPredict[-1]]), batch_size=batch_size)
        predictions[i] = prediction
        trainPredict.append(np.vstack([trainPredict[-1][1:], prediction]))
    plt.figure(figsize=(12, 5))
    # print len(predictions)
    # print len(dataset[train_size:(train_size+look_ahead)])
    plt.plot(np.arange(look_ahead), scaler.inverse_transform(predictions) ,'r', label="Prediction")
    plt.plot(np.arange(look_ahead), scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]), label="Real data")
    plt.legend()
    # plt.show()
    plt.savefig(
        "./lstm/graphs/" +
        normalizeName(
            "basic_lstm_" + hostname + "!" + service + ".png"
            )
        )
    rmse = sqrt(mean_squared_error(
        scaler.inverse_transform(dataset[train_size:(train_size+look_ahead)]),
        scaler.inverse_transform(predictions)))
    print rmse
    print "=============================================================="


