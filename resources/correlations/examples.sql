SELECT CORR(t1.result, t1.result)
FROM record as t1
WHERE hostname = 'printer.nemor.cz' and service = 'Available memory';




with table_mean as
    (
        select avg(char_length(subject_line)) as mean_subject_length,
            avg(report_summary_open_rate) as mean_open_rate
        from mailchimp.campaigns
    ),

    table_corrected as
    (
        select char_length(subject_line) - mean_subject_length as mean_subject_length_corrected, 
                report_summary_open_rate - mean_open_rate as mean_open_rate_corrected
        from table_mean, mailchimp.campaigns
    ),      

select sum(mean_subject_length_corrected * mean_open_rate_corrected) / sqrt(sum(mean_subject_length_corrected * mean_subject_length_corrected) * sum(mean_open_rate_corrected * mean_open_rate_corrected)) as r
from table_corrected;



SELECT CORR(cast(t1.result as float), cast(t1.result as float))
FROM record as t1
WHERE hostname = 'printer.nemor.cz' and service = 'Available memory';




WITH subst1 AS (
	SELECT result as float
	FROM record
	WHERE hostname = 'printer.nemor.cz' and service = 'Available memory'
),
subst2 AS (
	SELECT result as float
	FROM record
	WHERE hostname = 'nemo64.nemor.cz' and service = 'Available memory'
)
SELECT CORR(cast(subst1.result as float), cast(subst2.result as float)) FROM subst1, subst2;


WITH subst1 AS (
	SELECT cast(result as float) as res1
	FROM record
	WHERE hostname = 'printer.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
),
subst2 AS (
	SELECT cast(result as float) as res2
	FROM record
	WHERE hostname = 'nemo64.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
)
SELECT corr(subst1.res1, subst2.res2) from subst1, subst2;


WITH subst1 AS (
 SELECT cast(result as float) as res1
 FROM record
 WHERE hostname = 'printer.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
),
subst2 AS (
 SELECT cast(result as float) as res2
 FROM record
 WHERE hostname = 'nemo64.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
)
SELECT corr(subst1.res1, subst2.res2) from subst1, subst2;






WITH subst1 AS (
 SELECT cast(result as float) as res1
 FROM record
 WHERE hostname = 'printer.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
),
subst2 AS (
 SELECT cast(result as float) as res2
 FROM record
 WHERE hostname = 'nemo64.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
)

SELECT corr(subst1.res1, subst2.res2) from 
(
 SELECT cast(result as float) as res1
 FROM record
 WHERE hostname = 'printer.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
) as subst1,
(
 SELECT cast(result as float) as res2
 FROM record
 WHERE hostname = 'nemo64.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
) as subst2;

