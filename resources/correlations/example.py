import psycopg2
import numpy
# Try to connect

try:
    conn = psycopg2.connect("dbname='predmon' user='predmon' password='predmon'")
except:
    print "I am unable to connect to the database."

cur = conn.cursor()
cur.execute("""
 SELECT cast(result as float) as res1
 FROM record
 WHERE hostname = 'printer.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
""")

rows = cur.fetchall()
one_field = []
for row in rows:
    one_field.append(row[0])

cur.execute("""
 SELECT cast(result as float) as res2
 FROM record
 WHERE hostname = 'nemo64.nemor.cz' and service = 'Available memory' and time_execution > 400556032 ORDER BY time_execution DESC
""")

rows = cur.fetchall()
second_field = []
for row in rows:
    second_field.append(row[0])

lno = len(one_field)
lns = len(second_field)
if lno < lns:
    second_field = second_field[:lno]
else:
    one_field = one_field[:lns]


print (numpy.corrcoef(one_field, second_field)[0, 1])
