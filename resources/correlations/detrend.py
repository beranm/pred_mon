from scipy.signal import detrend
from matplotlib import pyplot as plt
import random
import numpy as np
from scipy.optimize import curve_fit
import timeit
from sklearn.metrics import mean_squared_error
import math
import statistics
import matplotlib as mpl 
import matplotlib.style
mpl.style.use('classic')

matplotlib.rc('xtick', labelsize=8) 
matplotlib.rc('ytick', labelsize=8) 



def detrend_fd(lst):
    if len(lst) == 1:
        return lst
    i = 1
    new_lst = []
    new_lst.append(lst[0])
    while i < len(lst):
        new_lst.append(lst[i] - lst[i - 1])
        i = i + 1
    return new_lst


def detrend_lr(lst):
    if len(lst) == 1:
        return lst
    i = 1
    new_lst = []
    new_lst.append(lst[0])
    while i < len(lst):
        new_lst.append(lst[i] / lst[i - 1])
        i = i + 1
    return new_lst


def detrend_avg(lst):
    z = np.polyfit(list(range(1, len(lst) + 1)), lst, 1)
    p = np.poly1d(z)
    new_lst = []
    for i in range(1, len(lst) + 1):
        new_lst.append(lst[i - 1] - p(i))
    return new_lst


trend_data = [random.randrange(1, 6) + i for i in range(1, 1000000)]


# t = np.linspace(0, 5, 100)
# trend_data = t + np.random.normal(size=100)


def test1():
    trend_data = [random.randrange(1, 6) + i for i in range(1, 10000000)]
    detrend_fd(trend_data)


def test2():
    trend_data = [random.randrange(1, 6) + i for i in range(1, 10000000)]
    detrend_lr(trend_data)


def test3():
    trend_data = [random.randrange(1, 6) + i for i in range(1, 10000000)]
    detrend(trend_data)


def test4():
    trend_data = [random.randrange(1, 6) + i for i in range(1, 10000000)]
    detrend_avg(trend_data)


def test5():
    trend_data1 = [random.randrange(1, 6) + i for i in range(1, 10000000)]
    trend_data2 = [random.randrange(1, 6) + i for i in range(1, 10000000)]
    print(np.corrcoef(trend_data1, trend_data2))


"""
print(
    timeit.timeit("test5()", number=1, setup="from __main__ import test5")
)


print(timeit.timeit("test1()", number=1, setup="from __main__ import test1"))
print(timeit.timeit("test2()", number=1, setup="from __main__ import test2"))
print(timeit.timeit("test3()", number=1, setup="from __main__ import test3"))
print(
    timeit.timeit("test4()", number=1, setup="from __main__ import test4")
)
"""

trend_data = [random.randrange(1, 6) + i for i in range(1, 100)]
plt.figure(figsize=(5, 4))
plt.plot(range(1, 100), trend_data, label="Input data")
plt.plot(range(1, 100), detrend_fd(trend_data), label="First differences")
plt.plot(range(1, 100), detrend_lr(trend_data), label="Link relatives")
plt.plot(range(1, 100), detrend(trend_data), label="Signal detrend")
plt.plot(range(1, 100), detrend_avg(trend_data), label="Line fit")

plt.legend(loc='best', fontsize = 'x-small')
plt.savefig("./corrs.png")

"""
print (mean_squared_error(detrend_fd(trend_data), detrend_fd(trend_data)))
print (mean_squared_error(detrend_fd(trend_data), detrend_lr(trend_data)))
print (mean_squared_error(detrend_fd(trend_data), detrend(trend_data)))

print()

print (mean_squared_error(detrend_lr(trend_data), detrend_fd(trend_data)))
print (mean_squared_error(detrend_lr(trend_data), detrend(trend_data)))

print()

print (mean_squared_error(detrend(trend_data), detrend_fd(trend_data)))

"""
print()

print (mean_squared_error(detrend_fd(trend_data), trend_data))
print (mean_squared_error(detrend_lr(trend_data), trend_data))
print (mean_squared_error(detrend(trend_data), trend_data))

print()
print (statistics.stdev(trend_data))
print (statistics.stdev(detrend_fd(trend_data)))
print (statistics.stdev(detrend_lr(trend_data)))
print (statistics.stdev(detrend(trend_data)))
