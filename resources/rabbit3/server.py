from amqplib import client_0_8 as amqp

# connect to server
lConnection = amqp.Connection(host="localhost:5672", virtual_host="/", insist=False)
lChannel = lConnection.channel()

lChannel.queue_declare(queue="myClientQueue", durable=True, exclusive=False, auto_delete=False)

lChannel.exchange_declare(exchange="myExchange", type="direct", durable=True, auto_delete=False)
lChannel.queue_bind(queue="myClientQueue", exchange="myExchange")




# Create a message
lMessage = amqp.Message("Test message!")

# Send message as persistant.  This means it will survive a reboot.
lMessage.properties["delivery_mode"] = 2

# publish the message on the exchange
lChannel.basic_publish(lMessage, exchange="myExchange")

# Close connection 
lChannel.close()
lConnection.close()
