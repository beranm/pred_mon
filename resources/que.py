#!/usr/bin/env python
from amqplib import client_0_8 as amqp
import time


conn = amqp.Connection(
    host="192.168.122.208:5672 ",
    userid="guest",
    password="guest",
    virtual_host="/",
    insist=False)
chan = conn.channel()

# Setup que

chan.queue_declare(
    queue="work",
    durable=True,
    exclusive=False,
    auto_delete=False)

chan.exchange_declare(
    exchange="sorting_room",
    type="direct",
    durable=True,
    auto_delete=False,)

chan.queue_bind(
    queue="work",
    exchange="sorting_room",
    routing_key="domain")

# Send messages
send = amqp.Message("Test message!")
send.properties["delivery_mode"] = 2
chan.basic_publish(
    send,
    exchange="sorting_room",
    routing_key="domain")


# Get data runns and lissen for the loop
def recv_callback(msg):
    print('Received: ' + msg.body + ' from channel #' + str(msg.channel.channel_id))


chan.basic_consume(
    queue='work',
    no_ack=True,
    callback=recv_callback,
    consumer_tag="testtag")

while True:
    chan.wait()
chan.basic_cancel("testtag")
