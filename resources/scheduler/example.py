import sys
from time import sleep
from apscheduler.schedulers.background import BackgroundScheduler
sched = BackgroundScheduler()
sched.start()        # start the scheduler
# define the function that is to be executed
# it will be executed in a thread by the scheduler
def my_job(text):
    print (text)


def main():
    # from now on, execute my_job every minute
    job = sched.add_job(my_job, 'interval', minutes=1, args=['minuter'])
    # start at start_date (my_job is called) and then execute my_job every minute
    # job = sched.add_interval_job(my_job, minutes=1, start_date='2013-08-06 00:09:12', args=['text'])
    # job = sched.add_date_job(my_job, '2013-08-05 23:47:05', ['text'])
    while True:
        sleep(1)
        sys.stdout.write('.')
        sys.stdout.flush()

if __name__ == "__main__":
    main()
