while True:
            db = PostgresqlDatabase(
                database_credentials['name'],
                user=database_credentials['user'],
                password=database_credentials['password'])
            database_proxy.initialize(db)
            db.connect()
            badItems = getBadItems(zapi)
            try:
                hosts = zapi.hostinterface.get(output=["dns", "ip", "useip"], selectHosts=["host"], filter={"main": 1, "type": 1})
            except requests.exceptions.ConnectionError:
                print "Connection error, leaving for next iteration"
                hosts = []

            for h in hosts:
                hostname = h['hosts'][0]['host']
                if fnmatch.fnmatch(hostname, hostnames):
                    print h['hosts'][0]
                    try:
                        list_item = zapi.item.get(output="extend", hostids=[h['hosts'][0]['hostid']])
                    except requests.exceptions.ConnectionError:
                        print "Failed connection, continue"
                        continue
                    messages = []
                    for item in list_item:
                        if item['lastclock'] != "0":
                            # print item['delay']
                            new_rec = Record(
                                service=parseName(item['name'], item['key_']),
                                hostname=hostname,
                                state=(
                                    1 if item['itemid'] in badItems
                                    else 0
                                ),
                                time_execution=item['lastclock'],
                                result=str(item['lastvalue']),
                                delay=item['delay']
                                )
                            try:
                                new_rec.save()
                                toqueue = {
                                        'hostname': new_rec.hostname,
                                        'service': parseName(item['name'], item['key_']),
                                        'state': new_rec.state,
                                        'time_execution': new_rec.time_execution,
                                        'result': new_rec.result}
                                messages.append(toqueue)
                                channel.basic_publish(
                                    exchange='',
                                    routing_key='predict',
                                    body=json.dumps(toqueue))
                                channel.basic_publish(
                                    exchange='',
                                    routing_key='collect',
                                    body=json.dumps(toqueue))
                            except peewee.IntegrityError:
                                print "Error..."
                                print item['state']
                                print parseName(item['name'], item['key_'])
                                print hostname
                                print item['lastclock']
                                print str(item['lastvalue'])
                                print "=============="
                    # print item
                    # exit(1)
            db.close()
            time.sleep(basic_configurations['collect_delay'])
