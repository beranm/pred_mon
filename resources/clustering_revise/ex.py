from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
from numpy import linspace
from scipy import signal
from scipy.signal import argrelextrema
import numpy as np
import matplotlib as mpl 
import matplotlib.style
mpl.style.use('classic')


data = [1509113588, 1509113677.252376, 1509113679.252376, 1509213677.252376, 1509213675.252376]
# data = [1, 2, 3, 6, 7, 8, 19, 20, 21]
# data = signal.detrend(data)

print(data)

# 'data' is a 1D array that contains the initial numbers 37231 to 56661
xmin = min(data)
xmax = max(data)

# get evenly distributed numbers for X axis.
x = linspace(xmin, xmax, 1000)   # get 1000 points on x axis
nPoints = len(x)

# get actual kernel density.
density = gaussian_kde(data)
y = density(x)

# print the output data
# for i in range(nPoints):
#     print "%s   %s" % (x[i], y[i])

print(x)
print(density(x))

mi, ma = argrelextrema(density(x), np.less), argrelextrema(density(x), np.greater)

print("mi")
print(x[mi])
print("ma")
print(x[ma])

plt.plot(x, density(x))
plt.show()

