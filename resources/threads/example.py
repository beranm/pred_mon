import threading as t

def hello(inputs):
    print inputs
    print "hello, world"


a = t.Timer(30.0, hello, {"a":"b"})
a.start()
b = t.Timer(5.0, hello, {"a":"a"})
b.start()

